# Start from the MySQL image
FROM mysql:5.7

# Copy the SQL dump file to the appropriate location
COPY ./mysql-provisioning/schema.sql /docker-entrypoint-initdb.d/schema.sql