GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';
-- UPDATE mysql.user SET host='%' WHERE user='root';

CREATE DATABASE IF NOT EXISTS autokuca;
CREATE DATABASE IF NOT EXISTS grafanainfo;

-- MySQL dump 10.13  Distrib 5.7.43, for Linux (x86_64)
--
-- Host: localhost    Database: grafanainfo
-- ------------------------------------------------------
-- Server version	5.7.43

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alert`
--
USE grafanainfo;
DROP TABLE IF EXISTS `alert`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert` (
                         `id` bigint(20) NOT NULL AUTO_INCREMENT,
                         `version` bigint(20) NOT NULL,
                         `dashboard_id` bigint(20) NOT NULL,
                         `panel_id` bigint(20) NOT NULL,
                         `org_id` bigint(20) NOT NULL,
                         `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
                         `state` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `settings` mediumtext COLLATE utf8mb4_unicode_ci,
                         `frequency` bigint(20) NOT NULL,
                         `handler` bigint(20) NOT NULL,
                         `severity` text COLLATE utf8mb4_unicode_ci NOT NULL,
                         `silenced` tinyint(1) NOT NULL,
                         `execution_error` text COLLATE utf8mb4_unicode_ci NOT NULL,
                         `eval_data` text COLLATE utf8mb4_unicode_ci,
                         `eval_date` datetime DEFAULT NULL,
                         `new_state_date` datetime NOT NULL,
                         `state_changes` int(11) NOT NULL,
                         `created` datetime NOT NULL,
                         `updated` datetime NOT NULL,
                         `for` bigint(20) DEFAULT NULL,
                         PRIMARY KEY (`id`),
                         KEY `IDX_alert_org_id_id` (`org_id`,`id`),
                         KEY `IDX_alert_state` (`state`),
                         KEY `IDX_alert_dashboard_id` (`dashboard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert`
--

LOCK TABLES `alert` WRITE;
/*!40000 ALTER TABLE `alert` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_configuration`
--

DROP TABLE IF EXISTS `alert_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_configuration` (
                                       `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                       `alertmanager_configuration` mediumtext COLLATE utf8mb4_unicode_ci,
                                       `configuration_version` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
                                       `created_at` int(11) NOT NULL,
                                       `default` tinyint(1) NOT NULL DEFAULT '0',
                                       `org_id` bigint(20) NOT NULL DEFAULT '0',
                                       `configuration_hash` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'not-yet-calculated',
                                       PRIMARY KEY (`id`),
                                       UNIQUE KEY `UQE_alert_configuration_org_id` (`org_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_configuration`
--

LOCK TABLES `alert_configuration` WRITE;
/*!40000 ALTER TABLE `alert_configuration` DISABLE KEYS */;
INSERT INTO `alert_configuration` VALUES (1,'{\"template_files\":{},\"alertmanager_config\":{\"route\":{\"receiver\":\"grafana-default-email\",\"group_by\":[\"grafana_folder\",\"alertname\"],\"routes\":[{\"receiver\":\"Vlasnik Auto Kuce\",\"object_matchers\":[[\"kontakt\",\"=\",\"vlasnik\"]]}]},\"templates\":null,\"receivers\":[{\"name\":\"grafana-default-email\"},{\"name\":\"Vlasnik Auto Kuce\",\"grafana_managed_receiver_configs\":[{\"uid\":\"b975e35d-f0f5-41dc-88cc-cdea02b6188f\",\"name\":\"Vlasnik Auto Kuce\",\"type\":\"email\",\"disableResolveMessage\":false,\"settings\":{\"addresses\":\"husejin.hodzic@fet.ba\",\"singleEmail\":false},\"secureSettings\":null}]}]}}','v1',1695934944,0,1,'9ee91802554da65c5b04a3fc50cd3b5e');
/*!40000 ALTER TABLE `alert_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_configuration_history`
--

DROP TABLE IF EXISTS `alert_configuration_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_configuration_history` (
                                               `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                               `org_id` bigint(20) NOT NULL DEFAULT '0',
                                               `alertmanager_configuration` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
                                               `configuration_hash` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'not-yet-calculated',
                                               `configuration_version` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
                                               `created_at` int(11) NOT NULL,
                                               `default` tinyint(1) NOT NULL DEFAULT '0',
                                               `last_applied` int(11) NOT NULL DEFAULT '0',
                                               PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_configuration_history`
--

LOCK TABLES `alert_configuration_history` WRITE;
/*!40000 ALTER TABLE `alert_configuration_history` DISABLE KEYS */;
INSERT INTO `alert_configuration_history` VALUES (1,1,'{\n  \"alertmanager_config\": {\n    \"route\": {\n      \"receiver\": \"grafana-default-email\",\n      \"group_by\": [\"grafana_folder\", \"alertname\"]\n    },\n    \"receivers\": [{\n      \"name\": \"grafana-default-email\",\n      \"grafana_managed_receiver_configs\": [{\n        \"uid\": \"\",\n        \"name\": \"email receiver\",\n        \"type\": \"email\",\n        \"isDefault\": true,\n        \"settings\": {\n          \"addresses\": \"<example@email.com>\"\n        }\n      }]\n    }]\n  }\n}\n','e0528a75784033ae7b15c40851d89484','v1',1695933955,1,1695934017),(2,1,'{\"template_files\":{},\"alertmanager_config\":{\"route\":{\"receiver\":\"grafana-default-email\",\"group_by\":[\"grafana_folder\",\"alertname\"]},\"templates\":null,\"receivers\":[{\"name\":\"grafana-default-email\",\"grafana_managed_receiver_configs\":[{\"uid\":\"c85c3417-30cb-4ec2-9449-9ac02939bb5e\",\"name\":\"email receiver\",\"type\":\"email\",\"disableResolveMessage\":false,\"settings\":{\"addresses\":\"\\u003cexample@email.com\\u003e\"},\"secureSettings\":null}]},{\"name\":\"test\",\"grafana_managed_receiver_configs\":[{\"uid\":\"be4df445-53c1-4111-ac90-6ff6d34ef787\",\"name\":\"test\",\"type\":\"email\",\"disableResolveMessage\":false,\"settings\":{\"addresses\":\"test@gmail.com\",\"singleEmail\":false},\"secureSettings\":{}}]}]}}','6a502c4edbc9369bba9b1b158e26fba4','v1',1695934017,0,1695934064),(3,1,'{\"template_files\":{},\"alertmanager_config\":{\"route\":{\"receiver\":\"grafana-default-email\",\"group_by\":[\"grafana_folder\",\"alertname\"]},\"templates\":null,\"receivers\":[{\"name\":\"grafana-default-email\",\"grafana_managed_receiver_configs\":[{\"uid\":\"c85c3417-30cb-4ec2-9449-9ac02939bb5e\",\"name\":\"email receiver\",\"type\":\"email\",\"disableResolveMessage\":false,\"settings\":{\"addresses\":\"\\u003cexample@email.com\\u003e\"},\"secureSettings\":null}]}]}}','c84fb02db1a530523926a742aff0778f','v1',1695934102,0,1695934479),(4,1,'{\"template_files\":{},\"alertmanager_config\":{\"route\":{\"receiver\":\"grafana-default-email\",\"group_by\":[\"grafana_folder\",\"alertname\"]},\"templates\":null,\"receivers\":[{\"name\":\"grafana-default-email\",\"grafana_managed_receiver_configs\":[{\"uid\":\"c85c3417-30cb-4ec2-9449-9ac02939bb5e\",\"name\":\"email receiver\",\"type\":\"email\",\"disableResolveMessage\":false,\"settings\":{\"addresses\":\"\\u003cexample@email.com\\u003e\"},\"secureSettings\":null}]},{\"name\":\"Vlasnik Auto Kuce\",\"grafana_managed_receiver_configs\":[{\"uid\":\"b975e35d-f0f5-41dc-88cc-cdea02b6188f\",\"name\":\"Vlasnik Auto Kuce\",\"type\":\"email\",\"disableResolveMessage\":false,\"settings\":{\"addresses\":\"husejin.hodzic@fet.ba\",\"singleEmail\":false},\"secureSettings\":{}}]}]}}','5124cd03db04686a10bd5ffb99ed82af','v1',1695934890,0,1695934900),(5,1,'{\"template_files\":{},\"alertmanager_config\":{\"route\":{\"receiver\":\"grafana-default-email\",\"group_by\":[\"grafana_folder\",\"alertname\"]},\"templates\":null,\"receivers\":[{\"name\":\"grafana-default-email\"},{\"name\":\"Vlasnik Auto Kuce\",\"grafana_managed_receiver_configs\":[{\"uid\":\"b975e35d-f0f5-41dc-88cc-cdea02b6188f\",\"name\":\"Vlasnik Auto Kuce\",\"type\":\"email\",\"disableResolveMessage\":false,\"settings\":{\"addresses\":\"husejin.hodzic@fet.ba\",\"singleEmail\":false},\"secureSettings\":null}]}]}}','f3e592c1deda6cd0223d05a4d713b790','v1',1695934920,0,1695934920),(6,1,'{\"template_files\":{},\"alertmanager_config\":{\"route\":{\"receiver\":\"grafana-default-email\",\"group_by\":[\"grafana_folder\",\"alertname\"],\"routes\":[{\"receiver\":\"Vlasnik Auto Kuce\",\"object_matchers\":[[\"kontakt\",\"=\",\"vlasnik\"]]}]},\"templates\":null,\"receivers\":[{\"name\":\"grafana-default-email\"},{\"name\":\"Vlasnik Auto Kuce\",\"grafana_managed_receiver_configs\":[{\"uid\":\"b975e35d-f0f5-41dc-88cc-cdea02b6188f\",\"name\":\"Vlasnik Auto Kuce\",\"type\":\"email\",\"disableResolveMessage\":false,\"settings\":{\"addresses\":\"husejin.hodzic@fet.ba\",\"singleEmail\":false},\"secureSettings\":null}]}]}}','9ee91802554da65c5b04a3fc50cd3b5e','v1',1695934944,0,1695994849);
/*!40000 ALTER TABLE `alert_configuration_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_image`
--

DROP TABLE IF EXISTS `alert_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_image` (
                               `id` bigint(20) NOT NULL AUTO_INCREMENT,
                               `token` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `path` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `url` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `created_at` datetime NOT NULL,
                               `expires_at` datetime NOT NULL,
                               PRIMARY KEY (`id`),
                               UNIQUE KEY `UQE_alert_image_token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_image`
--

LOCK TABLES `alert_image` WRITE;
/*!40000 ALTER TABLE `alert_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_instance`
--

DROP TABLE IF EXISTS `alert_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_instance` (
                                  `rule_org_id` bigint(20) NOT NULL,
                                  `rule_uid` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
                                  `labels` text COLLATE utf8mb4_unicode_ci NOT NULL,
                                  `labels_hash` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                                  `current_state` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                                  `current_state_since` bigint(20) NOT NULL,
                                  `last_eval_time` bigint(20) NOT NULL,
                                  `current_state_end` bigint(20) NOT NULL DEFAULT '0',
                                  `current_reason` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                  PRIMARY KEY (`rule_org_id`,`rule_uid`,`labels_hash`),
                                  KEY `IDX_alert_instance_rule_org_id_rule_uid_current_state` (`rule_org_id`,`rule_uid`,`current_state`),
                                  KEY `IDX_alert_instance_rule_org_id_current_state` (`rule_org_id`,`current_state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_instance`
--

LOCK TABLES `alert_instance` WRITE;
/*!40000 ALTER TABLE `alert_instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_notification`
--

DROP TABLE IF EXISTS `alert_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_notification` (
                                      `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                      `org_id` bigint(20) NOT NULL,
                                      `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `settings` text COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `created` datetime NOT NULL,
                                      `updated` datetime NOT NULL,
                                      `is_default` tinyint(1) NOT NULL DEFAULT '0',
                                      `frequency` bigint(20) DEFAULT NULL,
                                      `send_reminder` tinyint(1) DEFAULT '0',
                                      `disable_resolve_message` tinyint(1) NOT NULL DEFAULT '0',
                                      `uid` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `secure_settings` text COLLATE utf8mb4_unicode_ci,
                                      PRIMARY KEY (`id`),
                                      UNIQUE KEY `UQE_alert_notification_org_id_uid` (`org_id`,`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_notification`
--

LOCK TABLES `alert_notification` WRITE;
/*!40000 ALTER TABLE `alert_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_notification_state`
--

DROP TABLE IF EXISTS `alert_notification_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_notification_state` (
                                            `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                            `org_id` bigint(20) NOT NULL,
                                            `alert_id` bigint(20) NOT NULL,
                                            `notifier_id` bigint(20) NOT NULL,
                                            `state` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
                                            `version` bigint(20) NOT NULL,
                                            `updated_at` bigint(20) NOT NULL,
                                            `alert_rule_state_updated_version` bigint(20) NOT NULL,
                                            PRIMARY KEY (`id`),
                                            UNIQUE KEY `UQE_alert_notification_state_org_id_alert_id_notifier_id` (`org_id`,`alert_id`,`notifier_id`),
                                            KEY `IDX_alert_notification_state_alert_id` (`alert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_notification_state`
--

LOCK TABLES `alert_notification_state` WRITE;
/*!40000 ALTER TABLE `alert_notification_state` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_notification_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_rule`
--

DROP TABLE IF EXISTS `alert_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_rule` (
                              `id` bigint(20) NOT NULL AUTO_INCREMENT,
                              `org_id` bigint(20) NOT NULL,
                              `title` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `condition` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `data` mediumtext COLLATE utf8mb4_unicode_ci,
                              `updated` datetime NOT NULL,
                              `interval_seconds` bigint(20) NOT NULL DEFAULT '60',
                              `version` int(11) NOT NULL DEFAULT '0',
                              `uid` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
                              `namespace_uid` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `rule_group` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `no_data_state` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NoData',
                              `exec_err_state` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Alerting',
                              `for` bigint(20) NOT NULL DEFAULT '0',
                              `annotations` text COLLATE utf8mb4_unicode_ci,
                              `labels` text COLLATE utf8mb4_unicode_ci,
                              `dashboard_uid` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                              `panel_id` bigint(20) DEFAULT NULL,
                              `rule_group_idx` int(11) NOT NULL DEFAULT '1',
                              `is_paused` tinyint(1) NOT NULL DEFAULT '0',
                              PRIMARY KEY (`id`),
                              UNIQUE KEY `UQE_alert_rule_org_id_uid` (`org_id`,`uid`),
                              UNIQUE KEY `UQE_alert_rule_org_id_namespace_uid_title` (`org_id`,`namespace_uid`,`title`),
                              KEY `IDX_alert_rule_org_id_namespace_uid_rule_group` (`org_id`,`namespace_uid`,`rule_group`),
                              KEY `IDX_alert_rule_org_id_dashboard_uid_panel_id` (`org_id`,`dashboard_uid`,`panel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_rule`
--

LOCK TABLES `alert_rule` WRITE;
/*!40000 ALTER TABLE `alert_rule` DISABLE KEYS */;
INSERT INTO `alert_rule` VALUES (1,1,'Prevelik broj vozila na servisu','Kriterij','[{\"refId\":\"Podaci iz baze podataka\",\"queryType\":\"\",\"relativeTimeRange\":{\"from\":600,\"to\":0},\"datasourceUid\":\"P8462547643F503B1\",\"model\":{\"dataset\":\"autokuca\",\"editorMode\":\"code\",\"format\":\"table\",\"intervalMs\":1000,\"maxDataPoints\":43200,\"rawQuery\":true,\"rawSql\":\"SELECT\\n  COUNT(*)\\nFROM\\n  automobili\\nWHERE\\n  na_servisu = true\",\"refId\":\"Podaci iz baze podataka\",\"sql\":{\"columns\":[{\"parameters\":[],\"type\":\"function\"}],\"groupBy\":[{\"property\":{\"type\":\"string\"},\"type\":\"groupBy\"}],\"limit\":50}}},{\"refId\":\"Kriterij\",\"queryType\":\"\",\"relativeTimeRange\":{\"from\":600,\"to\":0},\"datasourceUid\":\"__expr__\",\"model\":{\"conditions\":[{\"evaluator\":{\"params\":[10],\"type\":\"gt\"},\"operator\":{\"type\":\"and\"},\"query\":{\"params\":[\"C\"]},\"reducer\":{\"params\":[],\"type\":\"last\"},\"type\":\"query\"}],\"datasource\":{\"type\":\"__expr__\",\"uid\":\"__expr__\"},\"expression\":\"Podaci iz baze podataka\",\"intervalMs\":1000,\"maxDataPoints\":43200,\"refId\":\"Kriterij\",\"type\":\"threshold\"}}]','2023-09-28 21:36:40',86400,1,'c09ec6f5-01c0-41a3-9d76-b82ef4619d6d','b38e7302-eee9-49b1-abae-2d561374371c','Dnevna evaluacija','NoData','Error',0,'{\"summary\":\"Ovaj alert ce nas obavjestiti ukoliko u auto kuci vise od 10 vozila je na servisiranju.\"}','{\"kategorija\":\"vozila\",\"kontakt\":\"vlasnik\"}',NULL,NULL,1,0);
/*!40000 ALTER TABLE `alert_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_rule_tag`
--

DROP TABLE IF EXISTS `alert_rule_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_rule_tag` (
                                  `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                  `alert_id` bigint(20) NOT NULL,
                                  `tag_id` bigint(20) NOT NULL,
                                  PRIMARY KEY (`id`),
                                  UNIQUE KEY `UQE_alert_rule_tag_alert_id_tag_id` (`alert_id`,`tag_id`),
                                  KEY `IDX_alert_rule_tag_alert_id` (`alert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_rule_tag`
--

LOCK TABLES `alert_rule_tag` WRITE;
/*!40000 ALTER TABLE `alert_rule_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_rule_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_rule_version`
--

DROP TABLE IF EXISTS `alert_rule_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_rule_version` (
                                      `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                      `rule_org_id` bigint(20) NOT NULL,
                                      `rule_uid` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
                                      `rule_namespace_uid` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `rule_group` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `parent_version` int(11) NOT NULL,
                                      `restored_from` int(11) NOT NULL,
                                      `version` int(11) NOT NULL,
                                      `created` datetime NOT NULL,
                                      `title` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `condition` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `data` mediumtext COLLATE utf8mb4_unicode_ci,
                                      `interval_seconds` bigint(20) NOT NULL,
                                      `no_data_state` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NoData',
                                      `exec_err_state` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Alerting',
                                      `for` bigint(20) NOT NULL DEFAULT '0',
                                      `annotations` text COLLATE utf8mb4_unicode_ci,
                                      `labels` text COLLATE utf8mb4_unicode_ci,
                                      `rule_group_idx` int(11) NOT NULL DEFAULT '1',
                                      `is_paused` tinyint(1) NOT NULL DEFAULT '0',
                                      PRIMARY KEY (`id`),
                                      UNIQUE KEY `UQE_alert_rule_version_rule_org_id_rule_uid_version` (`rule_org_id`,`rule_uid`,`version`),
                                      KEY `IDX_alert_rule_version_rule_org_id_rule_namespace_uid_rule_group` (`rule_org_id`,`rule_namespace_uid`,`rule_group`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_rule_version`
--

LOCK TABLES `alert_rule_version` WRITE;
/*!40000 ALTER TABLE `alert_rule_version` DISABLE KEYS */;
INSERT INTO `alert_rule_version` VALUES (1,1,'c09ec6f5-01c0-41a3-9d76-b82ef4619d6d','b38e7302-eee9-49b1-abae-2d561374371c','Dnevna evaluacija',0,0,1,'2023-09-28 21:36:40','Prevelik broj vozila na servisu','Kriterij','[{\"refId\":\"Podaci iz baze podataka\",\"queryType\":\"\",\"relativeTimeRange\":{\"from\":600,\"to\":0},\"datasourceUid\":\"P8462547643F503B1\",\"model\":{\"dataset\":\"autokuca\",\"editorMode\":\"code\",\"format\":\"table\",\"intervalMs\":1000,\"maxDataPoints\":43200,\"rawQuery\":true,\"rawSql\":\"SELECT\\n  COUNT(*)\\nFROM\\n  automobili\\nWHERE\\n  na_servisu = true\",\"refId\":\"Podaci iz baze podataka\",\"sql\":{\"columns\":[{\"parameters\":[],\"type\":\"function\"}],\"groupBy\":[{\"property\":{\"type\":\"string\"},\"type\":\"groupBy\"}],\"limit\":50}}},{\"refId\":\"Kriterij\",\"queryType\":\"\",\"relativeTimeRange\":{\"from\":600,\"to\":0},\"datasourceUid\":\"__expr__\",\"model\":{\"conditions\":[{\"evaluator\":{\"params\":[10],\"type\":\"gt\"},\"operator\":{\"type\":\"and\"},\"query\":{\"params\":[\"C\"]},\"reducer\":{\"params\":[],\"type\":\"last\"},\"type\":\"query\"}],\"datasource\":{\"type\":\"__expr__\",\"uid\":\"__expr__\"},\"expression\":\"Podaci iz baze podataka\",\"intervalMs\":1000,\"maxDataPoints\":43200,\"refId\":\"Kriterij\",\"type\":\"threshold\"}}]',86400,'NoData','Error',0,'{\"summary\":\"Ovaj alert ce nas obavjestiti ukoliko u auto kuci vise od 10 vozila je na servisiranju.\"}','{\"kategorija\":\"vozila\",\"kontakt\":\"vlasnik\"}',0,0);
/*!40000 ALTER TABLE `alert_rule_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `annotation`
--

DROP TABLE IF EXISTS `annotation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annotation` (
                              `id` bigint(20) NOT NULL AUTO_INCREMENT,
                              `org_id` bigint(20) NOT NULL,
                              `alert_id` bigint(20) DEFAULT NULL,
                              `user_id` bigint(20) DEFAULT NULL,
                              `dashboard_id` bigint(20) DEFAULT NULL,
                              `panel_id` bigint(20) DEFAULT NULL,
                              `category_id` bigint(20) DEFAULT NULL,
                              `type` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
                              `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
                              `metric` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                              `prev_state` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `new_state` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
                              `epoch` bigint(20) NOT NULL,
                              `region_id` bigint(20) DEFAULT '0',
                              `tags` varchar(4096) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                              `created` bigint(20) DEFAULT '0',
                              `updated` bigint(20) DEFAULT '0',
                              `epoch_end` bigint(20) NOT NULL DEFAULT '0',
                              PRIMARY KEY (`id`),
                              KEY `IDX_annotation_org_id_alert_id` (`org_id`,`alert_id`),
                              KEY `IDX_annotation_org_id_type` (`org_id`,`type`),
                              KEY `IDX_annotation_org_id_created` (`org_id`,`created`),
                              KEY `IDX_annotation_org_id_updated` (`org_id`,`updated`),
                              KEY `IDX_annotation_org_id_dashboard_id_epoch_end_epoch` (`org_id`,`dashboard_id`,`epoch_end`,`epoch`),
                              KEY `IDX_annotation_org_id_epoch_end_epoch` (`org_id`,`epoch_end`,`epoch`),
                              KEY `IDX_annotation_alert_id` (`alert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `annotation`
--

LOCK TABLES `annotation` WRITE;
/*!40000 ALTER TABLE `annotation` DISABLE KEYS */;
/*!40000 ALTER TABLE `annotation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `annotation_tag`
--

DROP TABLE IF EXISTS `annotation_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annotation_tag` (
                                  `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                  `annotation_id` bigint(20) NOT NULL,
                                  `tag_id` bigint(20) NOT NULL,
                                  PRIMARY KEY (`id`),
                                  UNIQUE KEY `UQE_annotation_tag_annotation_id_tag_id` (`annotation_id`,`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `annotation_tag`
--

LOCK TABLES `annotation_tag` WRITE;
/*!40000 ALTER TABLE `annotation_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `annotation_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `api_key`
--

DROP TABLE IF EXISTS `api_key`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_key` (
                           `id` bigint(20) NOT NULL AUTO_INCREMENT,
                           `org_id` bigint(20) NOT NULL,
                           `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                           `key` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                           `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                           `created` datetime NOT NULL,
                           `updated` datetime NOT NULL,
                           `expires` bigint(20) DEFAULT NULL,
                           `service_account_id` bigint(20) DEFAULT NULL,
                           `last_used_at` datetime DEFAULT NULL,
                           `is_revoked` tinyint(1) DEFAULT '0',
                           PRIMARY KEY (`id`),
                           UNIQUE KEY `UQE_api_key_key` (`key`),
                           UNIQUE KEY `UQE_api_key_org_id_name` (`org_id`,`name`),
                           KEY `IDX_api_key_org_id` (`org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_key`
--

LOCK TABLES `api_key` WRITE;
/*!40000 ALTER TABLE `api_key` DISABLE KEYS */;
/*!40000 ALTER TABLE `api_key` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `builtin_role`
--

DROP TABLE IF EXISTS `builtin_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `builtin_role` (
                                `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                `role` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                                `role_id` bigint(20) NOT NULL,
                                `created` datetime NOT NULL,
                                `updated` datetime NOT NULL,
                                `org_id` bigint(20) NOT NULL DEFAULT '0',
                                PRIMARY KEY (`id`),
                                UNIQUE KEY `UQE_builtin_role_org_id_role_id_role` (`org_id`,`role_id`,`role`),
                                KEY `IDX_builtin_role_role_id` (`role_id`),
                                KEY `IDX_builtin_role_role` (`role`),
                                KEY `IDX_builtin_role_org_id` (`org_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `builtin_role`
--

LOCK TABLES `builtin_role` WRITE;
/*!40000 ALTER TABLE `builtin_role` DISABLE KEYS */;
INSERT INTO `builtin_role` VALUES (1,'Editor',1,'2023-09-28 20:45:57','2023-09-28 20:45:57',1),(2,'Viewer',2,'2023-09-28 20:45:57','2023-09-28 20:45:57',1);
/*!40000 ALTER TABLE `builtin_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_data`
--

DROP TABLE IF EXISTS `cache_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cache_data` (
                              `cache_key` varchar(168) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `data` blob NOT NULL,
                              `expires` int(255) NOT NULL,
                              `created_at` int(255) NOT NULL,
                              PRIMARY KEY (`cache_key`),
                              UNIQUE KEY `UQE_cache_data_cache_key` (`cache_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_data`
--

LOCK TABLES `cache_data` WRITE;
/*!40000 ALTER TABLE `cache_data` DISABLE KEYS */;
INSERT INTO `cache_data` VALUES ('authed-session:0584777ebedc0a3dfbb4006f7e53bca0',0x7B226B696E64223A226175746865642D73657373696F6E222C226970223A223139322E3136382E3234302E31222C22757365725F6167656E74223A224D6F7A696C6C612F352E3020285831313B204C696E7578207838365F363429204170706C655765624B69742F3533372E333620284B48544D4C2C206C696B65204765636B6F29204368726F6D652F3131342E302E302E30205361666172692F3533372E3336222C226C6173745F7365656E223A22323032332D30392D32395431333A34323A33352E38363038363736325A227D,2592000,1695994955),('authed-session:c3848bb90233b3dac68ae6c0c105608e',0x7B226B696E64223A226175746865642D73657373696F6E222C226970223A223137322E32312E302E31222C22757365725F6167656E74223A224D6F7A696C6C612F352E3020285831313B204C696E7578207838365F363429204170706C655765624B69742F3533372E333620284B48544D4C2C206C696B65204765636B6F29204368726F6D652F3131342E302E302E30205361666172692F3533372E3336222C226C6173745F7365656E223A22323032332D30392D32385432323A31303A35322E35373936393630315A227D,2592000,1695939052);
/*!40000 ALTER TABLE `cache_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `correlation`
--

DROP TABLE IF EXISTS `correlation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `correlation` (
                               `uid` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `source_uid` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `target_uid` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `label` text COLLATE utf8mb4_unicode_ci NOT NULL,
                               `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
                               `config` text COLLATE utf8mb4_unicode_ci,
                               PRIMARY KEY (`uid`,`source_uid`),
                               KEY `IDX_correlation_uid` (`uid`),
                               KEY `IDX_correlation_source_uid` (`source_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `correlation`
--

LOCK TABLES `correlation` WRITE;
/*!40000 ALTER TABLE `correlation` DISABLE KEYS */;
/*!40000 ALTER TABLE `correlation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard`
--

DROP TABLE IF EXISTS `dashboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard` (
                             `id` bigint(20) NOT NULL AUTO_INCREMENT,
                             `version` int(11) NOT NULL,
                             `slug` varchar(189) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `title` varchar(189) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
                             `org_id` bigint(20) NOT NULL,
                             `created` datetime NOT NULL,
                             `updated` datetime NOT NULL,
                             `updated_by` int(11) DEFAULT NULL,
                             `created_by` int(11) DEFAULT NULL,
                             `gnet_id` bigint(20) DEFAULT NULL,
                             `plugin_id` varchar(189) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `folder_id` bigint(20) NOT NULL DEFAULT '0',
                             `is_folder` tinyint(1) NOT NULL DEFAULT '0',
                             `has_acl` tinyint(1) NOT NULL DEFAULT '0',
                             `uid` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `is_public` tinyint(1) NOT NULL DEFAULT '0',
                             PRIMARY KEY (`id`),
                             UNIQUE KEY `UQE_dashboard_org_id_folder_id_title` (`org_id`,`folder_id`,`title`),
                             UNIQUE KEY `UQE_dashboard_org_id_uid` (`org_id`,`uid`),
                             KEY `IDX_dashboard_org_id` (`org_id`),
                             KEY `IDX_dashboard_gnet_id` (`gnet_id`),
                             KEY `IDX_dashboard_org_id_plugin_id` (`org_id`,`plugin_id`),
                             KEY `IDX_dashboard_title` (`title`),
                             KEY `IDX_dashboard_is_folder` (`is_folder`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard`
--

LOCK TABLES `dashboard` WRITE;
/*!40000 ALTER TABLE `dashboard` DISABLE KEYS */;
INSERT INTO `dashboard` VALUES (2,2,'demo-dashboard-auto-kuca','Demo dashboard (Auto kuca)','{\"annotations\":{\"list\":[{\"builtIn\":1,\"datasource\":{\"type\":\"grafana\",\"uid\":\"-- Grafana --\"},\"enable\":true,\"hide\":true,\"iconColor\":\"rgba(0, 211, 255, 1)\",\"name\":\"Annotations \\u0026 Alerts\",\"type\":\"dashboard\"}]},\"editable\":true,\"fiscalYearStartMonth\":0,\"graphTooltip\":0,\"id\":2,\"links\":[],\"liveNow\":false,\"panels\":[{\"datasource\":{\"type\":\"testdata\",\"uid\":\"P814D78962B0F8AC2\"},\"fieldConfig\":{\"defaults\":{\"color\":{\"mode\":\"thresholds\"},\"mappings\":[],\"thresholds\":{\"mode\":\"absolute\",\"steps\":[{\"color\":\"green\",\"value\":null},{\"color\":\"red\",\"value\":80}]}},\"overrides\":[]},\"gridPos\":{\"h\":8,\"w\":12,\"x\":0,\"y\":0},\"id\":3,\"options\":{\"displayMode\":\"gradient\",\"minVizHeight\":10,\"minVizWidth\":0,\"orientation\":\"auto\",\"reduceOptions\":{\"calcs\":[\"lastNotNull\"],\"fields\":\"\",\"values\":false},\"showUnfilled\":true,\"valueMode\":\"color\"},\"pluginVersion\":\"10.1.2\",\"targets\":[{\"csvWave\":[{\"name\":\"Volkswagen\",\"timeStep\":60,\"valuesCSV\":\"10,65,22,14\"}],\"datasource\":{\"type\":\"testdata\",\"uid\":\"P814D78962B0F8AC2\"},\"refId\":\"Skoda\",\"scenarioId\":\"predictable_csv_wave\"}],\"title\":\"Mjesecna prodaja automobila\",\"transformations\":[{\"id\":\"calculateField\",\"options\":{\"alias\":\"BMW\",\"mode\":\"reduceRow\",\"reduce\":{\"reducer\":\"sum\"}}},{\"id\":\"calculateField\",\"options\":{\"alias\":\"Mercedes-Benz\",\"mode\":\"reduceRow\",\"reduce\":{\"reducer\":\"sum\"}}},{\"id\":\"calculateField\",\"options\":{\"alias\":\"Audi\",\"mode\":\"reduceRow\",\"reduce\":{\"reducer\":\"sum\"}}}],\"type\":\"bargauge\"},{\"datasource\":{\"type\":\"mysql\",\"uid\":\"P8462547643F503B1\"},\"fieldConfig\":{\"defaults\":{\"color\":{\"mode\":\"thresholds\"},\"mappings\":[],\"thresholds\":{\"mode\":\"absolute\",\"steps\":[{\"color\":\"green\",\"value\":null},{\"color\":\"red\",\"value\":80}]}},\"overrides\":[]},\"gridPos\":{\"h\":8,\"w\":12,\"x\":12,\"y\":0},\"id\":2,\"maxDataPoints\":1,\"options\":{\"colorMode\":\"value\",\"graphMode\":\"area\",\"justifyMode\":\"auto\",\"orientation\":\"auto\",\"reduceOptions\":{\"calcs\":[\"lastNotNull\"],\"fields\":\"\",\"values\":false},\"textMode\":\"auto\"},\"pluginVersion\":\"10.1.2\",\"targets\":[{\"dataset\":\"autokuca\",\"datasource\":{\"type\":\"mysql\",\"uid\":\"P8462547643F503B1\"},\"editorMode\":\"code\",\"format\":\"table\",\"rawQuery\":true,\"rawSql\":\"SELECT COUNT(*) FROM autokuca.automobili WHERE na_servisu=0\",\"refId\":\"A\",\"sql\":{\"columns\":[{\"parameters\":[],\"type\":\"function\"}],\"groupBy\":[{\"property\":{\"type\":\"string\"},\"type\":\"groupBy\"}],\"limit\":50}}],\"title\":\"Broj dostupnih vozila\",\"type\":\"stat\"},{\"datasource\":{\"type\":\"testdata\",\"uid\":\"P814D78962B0F8AC2\"},\"description\":\"\",\"fieldConfig\":{\"defaults\":{\"color\":{\"mode\":\"palette-classic\"},\"custom\":{\"axisCenteredZero\":false,\"axisColorMode\":\"text\",\"axisLabel\":\"\",\"axisPlacement\":\"auto\",\"barAlignment\":0,\"drawStyle\":\"line\",\"fillOpacity\":0,\"gradientMode\":\"none\",\"hideFrom\":{\"legend\":false,\"tooltip\":false,\"viz\":false},\"insertNulls\":false,\"lineInterpolation\":\"linear\",\"lineWidth\":1,\"pointSize\":5,\"scaleDistribution\":{\"type\":\"linear\"},\"showPoints\":\"auto\",\"spanNulls\":false,\"stacking\":{\"group\":\"A\",\"mode\":\"none\"},\"thresholdsStyle\":{\"mode\":\"off\"}},\"mappings\":[],\"thresholds\":{\"mode\":\"absolute\",\"steps\":[{\"color\":\"green\",\"value\":null},{\"color\":\"red\",\"value\":80}]}},\"overrides\":[]},\"gridPos\":{\"h\":8,\"w\":12,\"x\":0,\"y\":8},\"id\":1,\"maxDataPoints\":200,\"options\":{\"candleStyle\":\"candles\",\"colorStrategy\":\"open-close\",\"colors\":{\"down\":\"red\",\"up\":\"green\"},\"includeAllFields\":false,\"legend\":{\"calcs\":[],\"displayMode\":\"list\",\"placement\":\"bottom\",\"showLegend\":true},\"mode\":\"candles\"},\"pluginVersion\":\"9.5.1\",\"targets\":[{\"datasource\":{\"type\":\"testdata\",\"uid\":\"P814D78962B0F8AC2\"},\"refId\":\"A\",\"scenarioId\":\"random_walk\",\"spread\":300300}],\"title\":\"Mjesecna zarada\",\"transparent\":true,\"type\":\"candlestick\"}],\"refresh\":\"\",\"schemaVersion\":38,\"style\":\"dark\",\"tags\":[],\"templating\":{\"list\":[]},\"time\":{\"from\":\"now-30d\",\"to\":\"now\"},\"timepicker\":{},\"timezone\":\"\",\"title\":\"Demo dashboard (Auto kuca)\",\"uid\":\"f2945aef-5b5a-4e47-90dc-de604270e1fa\",\"version\":2,\"weekStart\":\"\"}',1,'2023-09-28 21:05:42','2023-09-29 23:34:01',1,1,0,'',3,0,0,'f2945aef-5b5a-4e47-90dc-de604270e1fa',0),(3,1,'stanje-auto-kuce','Stanje Auto Kuce','{\"schemaVersion\":17,\"title\":\"Stanje Auto Kuce\",\"uid\":\"b38e7302-eee9-49b1-abae-2d561374371c\",\"version\":1}',1,'2023-09-28 21:31:49','2023-09-28 21:31:49',1,1,0,'',0,1,0,'b38e7302-eee9-49b1-abae-2d561374371c',0),(4,2,'informacije-o-aplikaciji','Informacije o aplikaciji','{\"annotations\":{\"list\":[{\"builtIn\":1,\"datasource\":{\"type\":\"datasource\",\"uid\":\"grafana\"},\"enable\":true,\"hide\":true,\"iconColor\":\"rgba(0, 211, 255, 1)\",\"limit\":100,\"name\":\"Annotations \\u0026 Alerts\",\"showIn\":0,\"type\":\"dashboard\"},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"enable\":true,\"expr\":\"resets(process_uptime_seconds{application=\\\"$application\\\", instance=\\\"$instance\\\"}[1m]) \\u003e 0\",\"iconColor\":\"rgba(255, 96, 96, 1)\",\"name\":\"Restart Detection\",\"showIn\":0,\"step\":\"1m\",\"tagKeys\":\"restart-tag\",\"textFormat\":\"uptime reset\",\"titleFormat\":\"Restart\"}]},\"description\":\"Dashboard for Micrometer instrumented applications (Java, Spring Boot, Micronaut)\",\"editable\":true,\"fiscalYearStartMonth\":0,\"gnetId\":4701,\"graphTooltip\":1,\"id\":4,\"links\":[],\"liveNow\":false,\"panels\":[{\"collapsed\":false,\"datasource\":{\"type\":\"datasource\",\"uid\":\"grafana\"},\"gridPos\":{\"h\":1,\"w\":24,\"x\":0,\"y\":0},\"id\":139,\"panels\":[],\"targets\":[{\"datasource\":{\"type\":\"datasource\",\"uid\":\"grafana\"},\"refId\":\"A\"}],\"title\":\"Quick Facts\",\"type\":\"row\"},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"fieldConfig\":{\"defaults\":{\"color\":{\"mode\":\"thresholds\"},\"decimals\":1,\"mappings\":[{\"options\":{\"match\":\"null\",\"result\":{\"text\":\"N/A\"}},\"type\":\"special\"}],\"thresholds\":{\"mode\":\"absolute\",\"steps\":[{\"color\":\"green\",\"value\":null},{\"color\":\"red\",\"value\":80}]},\"unit\":\"s\"},\"overrides\":[]},\"gridPos\":{\"h\":3,\"w\":6,\"x\":0,\"y\":1},\"id\":63,\"links\":[],\"maxDataPoints\":100,\"options\":{\"colorMode\":\"value\",\"graphMode\":\"none\",\"justifyMode\":\"auto\",\"orientation\":\"horizontal\",\"reduceOptions\":{\"calcs\":[\"lastNotNull\"],\"fields\":\"\",\"values\":false},\"textMode\":\"auto\"},\"pluginVersion\":\"10.1.4\",\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"process_uptime_seconds{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"\",\"metric\":\"\",\"refId\":\"A\",\"step\":14400}],\"title\":\"Uptime\",\"type\":\"stat\"},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"fieldConfig\":{\"defaults\":{\"color\":{\"mode\":\"thresholds\"},\"mappings\":[{\"options\":{\"match\":\"null\",\"result\":{\"text\":\"N/A\"}},\"type\":\"special\"}],\"thresholds\":{\"mode\":\"absolute\",\"steps\":[{\"color\":\"green\",\"value\":null},{\"color\":\"red\",\"value\":80}]},\"unit\":\"dateTimeAsIso\"},\"overrides\":[]},\"gridPos\":{\"h\":3,\"w\":6,\"x\":6,\"y\":1},\"id\":92,\"links\":[],\"maxDataPoints\":100,\"options\":{\"colorMode\":\"value\",\"graphMode\":\"none\",\"justifyMode\":\"auto\",\"orientation\":\"horizontal\",\"reduceOptions\":{\"calcs\":[\"lastNotNull\"],\"fields\":\"\",\"values\":false},\"textMode\":\"auto\"},\"pluginVersion\":\"10.1.4\",\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"process_start_time_seconds{application=\\\"$application\\\", instance=\\\"$instance\\\"}*1000\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"\",\"metric\":\"\",\"refId\":\"A\",\"step\":14400}],\"title\":\"Start time\",\"type\":\"stat\"},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"fieldConfig\":{\"defaults\":{\"color\":{\"mode\":\"thresholds\"},\"decimals\":2,\"mappings\":[{\"options\":{\"match\":\"null\",\"result\":{\"text\":\"N/A\"}},\"type\":\"special\"}],\"thresholds\":{\"mode\":\"absolute\",\"steps\":[{\"color\":\"rgba(50, 172, 45, 0.97)\",\"value\":null},{\"color\":\"rgba(237, 129, 40, 0.89)\",\"value\":70},{\"color\":\"rgba(245, 54, 54, 0.9)\",\"value\":90}]},\"unit\":\"percent\"},\"overrides\":[]},\"gridPos\":{\"h\":3,\"w\":6,\"x\":12,\"y\":1},\"id\":65,\"links\":[],\"maxDataPoints\":100,\"options\":{\"colorMode\":\"value\",\"graphMode\":\"none\",\"justifyMode\":\"auto\",\"orientation\":\"horizontal\",\"reduceOptions\":{\"calcs\":[\"lastNotNull\"],\"fields\":\"\",\"values\":false},\"textMode\":\"auto\"},\"pluginVersion\":\"10.1.4\",\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"sum(jvm_memory_used_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\", area=\\\"heap\\\"})*100/sum(jvm_memory_max_bytes{application=\\\"$application\\\",instance=\\\"$instance\\\", area=\\\"heap\\\"})\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"\",\"refId\":\"A\",\"step\":14400}],\"title\":\"Heap used\",\"type\":\"stat\"},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"fieldConfig\":{\"defaults\":{\"color\":{\"mode\":\"thresholds\"},\"decimals\":2,\"mappings\":[{\"options\":{\"match\":\"null\",\"result\":{\"text\":\"N/A\"}},\"type\":\"special\"},{\"options\":{\"from\":-1e+32,\"result\":{\"text\":\"N/A\"},\"to\":0},\"type\":\"range\"}],\"thresholds\":{\"mode\":\"absolute\",\"steps\":[{\"color\":\"rgba(50, 172, 45, 0.97)\",\"value\":null},{\"color\":\"rgba(237, 129, 40, 0.89)\",\"value\":70},{\"color\":\"rgba(245, 54, 54, 0.9)\",\"value\":90}]},\"unit\":\"percent\"},\"overrides\":[]},\"gridPos\":{\"h\":3,\"w\":6,\"x\":18,\"y\":1},\"id\":75,\"links\":[],\"maxDataPoints\":100,\"options\":{\"colorMode\":\"value\",\"graphMode\":\"none\",\"justifyMode\":\"auto\",\"orientation\":\"horizontal\",\"reduceOptions\":{\"calcs\":[\"lastNotNull\"],\"fields\":\"\",\"values\":false},\"textMode\":\"auto\"},\"pluginVersion\":\"10.1.4\",\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"sum(jvm_memory_used_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\", area=\\\"nonheap\\\"})*100/sum(jvm_memory_max_bytes{application=\\\"$application\\\",instance=\\\"$instance\\\", area=\\\"nonheap\\\"})\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"\",\"refId\":\"A\",\"step\":14400}],\"title\":\"Non-Heap used\",\"type\":\"stat\"},{\"collapsed\":false,\"datasource\":{\"type\":\"datasource\",\"uid\":\"grafana\"},\"gridPos\":{\"h\":1,\"w\":24,\"x\":0,\"y\":4},\"id\":140,\"panels\":[],\"targets\":[{\"datasource\":{\"type\":\"datasource\",\"uid\":\"grafana\"},\"refId\":\"A\"}],\"title\":\"I/O Overview\",\"type\":\"row\"},{\"aliasColors\":{},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"fill\":1,\"fillGradient\":0,\"gridPos\":{\"h\":7,\"w\":6,\"x\":0,\"y\":5},\"hiddenSeries\":false,\"id\":111,\"legend\":{\"avg\":false,\"current\":true,\"max\":false,\"min\":false,\"show\":true,\"total\":false,\"values\":true},\"lines\":true,\"linewidth\":1,\"links\":[],\"nullPointMode\":\"null\",\"options\":{\"alertThreshold\":true},\"percentage\":false,\"pluginVersion\":\"10.1.4\",\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"sum(rate(http_server_requests_seconds_count{application=\\\"$application\\\", instance=\\\"$instance\\\"}[1m]))\",\"format\":\"time_series\",\"intervalFactor\":1,\"legendFormat\":\"HTTP\",\"refId\":\"A\"}],\"thresholds\":[],\"timeRegions\":[],\"title\":\"Rate\",\"tooltip\":{\"shared\":true,\"sort\":0,\"value_type\":\"individual\"},\"type\":\"graph\",\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"yaxes\":[{\"format\":\"ops\",\"logBase\":1,\"min\":\"0\",\"show\":true},{\"format\":\"short\",\"logBase\":1,\"show\":true}],\"yaxis\":{\"align\":false}},{\"aliasColors\":{\"HTTP\":\"#890f02\",\"HTTP - 5xx\":\"#bf1b00\"},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"fill\":1,\"fillGradient\":0,\"gridPos\":{\"h\":7,\"w\":6,\"x\":6,\"y\":5},\"hiddenSeries\":false,\"id\":112,\"legend\":{\"avg\":false,\"current\":true,\"max\":false,\"min\":false,\"show\":true,\"total\":false,\"values\":true},\"lines\":true,\"linewidth\":1,\"links\":[],\"nullPointMode\":\"null\",\"options\":{\"alertThreshold\":true},\"percentage\":false,\"pluginVersion\":\"10.1.4\",\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"sum(rate(http_server_requests_seconds_count{application=\\\"$application\\\", instance=\\\"$instance\\\", status=~\\\"5..\\\"}[1m]))\",\"format\":\"time_series\",\"intervalFactor\":1,\"legendFormat\":\"HTTP - 5xx\",\"refId\":\"A\"}],\"thresholds\":[],\"timeRegions\":[],\"title\":\"Errors\",\"tooltip\":{\"shared\":true,\"sort\":0,\"value_type\":\"individual\"},\"type\":\"graph\",\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"yaxes\":[{\"format\":\"ops\",\"logBase\":1,\"min\":\"0\",\"show\":true},{\"format\":\"short\",\"logBase\":1,\"show\":true}],\"yaxis\":{\"align\":false}},{\"aliasColors\":{},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"fill\":1,\"fillGradient\":0,\"gridPos\":{\"h\":7,\"w\":6,\"x\":12,\"y\":5},\"hiddenSeries\":false,\"id\":113,\"legend\":{\"avg\":false,\"current\":true,\"max\":false,\"min\":false,\"show\":true,\"total\":false,\"values\":true},\"lines\":true,\"linewidth\":1,\"links\":[],\"nullPointMode\":\"null\",\"options\":{\"alertThreshold\":true},\"percentage\":false,\"pluginVersion\":\"10.1.4\",\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"sum(rate(http_server_requests_seconds_sum{application=\\\"$application\\\", instance=\\\"$instance\\\", status!~\\\"5..\\\"}[1m]))/sum(rate(http_server_requests_seconds_count{application=\\\"$application\\\", instance=\\\"$instance\\\", status!~\\\"5..\\\"}[1m]))\",\"format\":\"time_series\",\"hide\":false,\"intervalFactor\":1,\"legendFormat\":\"HTTP - AVG\",\"refId\":\"A\"},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"max(http_server_requests_seconds_max{application=\\\"$application\\\", instance=\\\"$instance\\\", status!~\\\"5..\\\"})\",\"format\":\"time_series\",\"hide\":false,\"intervalFactor\":1,\"legendFormat\":\"HTTP - MAX\",\"refId\":\"B\"}],\"thresholds\":[],\"timeRegions\":[],\"title\":\"Duration\",\"tooltip\":{\"shared\":true,\"sort\":0,\"value_type\":\"individual\"},\"type\":\"graph\",\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"yaxes\":[{\"format\":\"s\",\"logBase\":1,\"min\":\"0\",\"show\":true},{\"format\":\"short\",\"logBase\":1,\"show\":true}],\"yaxis\":{\"align\":false}},{\"aliasColors\":{},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"description\":\"\",\"fill\":1,\"fillGradient\":0,\"gridPos\":{\"h\":7,\"w\":6,\"x\":18,\"y\":5},\"hiddenSeries\":false,\"id\":119,\"legend\":{\"alignAsTable\":false,\"avg\":false,\"current\":true,\"max\":false,\"min\":false,\"show\":true,\"total\":false,\"values\":true},\"lines\":true,\"linewidth\":1,\"links\":[],\"nullPointMode\":\"null\",\"options\":{\"alertThreshold\":true},\"percentage\":false,\"pluginVersion\":\"10.1.4\",\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"tomcat_threads_busy_threads{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"hide\":false,\"intervalFactor\":2,\"legendFormat\":\"TOMCAT - BSY\",\"refId\":\"A\"},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"tomcat_threads_current_threads{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"hide\":false,\"intervalFactor\":2,\"legendFormat\":\"TOMCAT - CUR\",\"refId\":\"B\"},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"tomcat_threads_config_max_threads{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"hide\":false,\"intervalFactor\":2,\"legendFormat\":\"TOMCAT - MAX\",\"refId\":\"C\"},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"jetty_threads_busy{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"hide\":false,\"intervalFactor\":2,\"legendFormat\":\"JETTY - BSY\",\"refId\":\"D\"},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"jetty_threads_current{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"hide\":false,\"intervalFactor\":2,\"legendFormat\":\"JETTY - CUR\",\"refId\":\"E\"},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"jetty_threads_config_max{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"hide\":false,\"intervalFactor\":2,\"legendFormat\":\"JETTY - MAX\",\"refId\":\"F\"}],\"thresholds\":[],\"timeRegions\":[],\"title\":\"Utilisation\",\"tooltip\":{\"shared\":true,\"sort\":0,\"value_type\":\"individual\"},\"type\":\"graph\",\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"yaxes\":[{\"format\":\"short\",\"logBase\":1,\"min\":\"0\",\"show\":true},{\"format\":\"short\",\"logBase\":1,\"show\":true}],\"yaxis\":{\"align\":false}},{\"collapsed\":false,\"datasource\":{\"type\":\"datasource\",\"uid\":\"grafana\"},\"gridPos\":{\"h\":1,\"w\":24,\"x\":0,\"y\":12},\"id\":141,\"panels\":[],\"targets\":[{\"datasource\":{\"type\":\"datasource\",\"uid\":\"grafana\"},\"refId\":\"A\"}],\"title\":\"JVM Memory\",\"type\":\"row\"},{\"aliasColors\":{},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"editable\":true,\"error\":false,\"fill\":1,\"fillGradient\":0,\"grid\":{\"leftLogBase\":1,\"rightLogBase\":1},\"gridPos\":{\"h\":7,\"w\":6,\"x\":0,\"y\":13},\"hiddenSeries\":false,\"id\":24,\"legend\":{\"avg\":false,\"current\":true,\"max\":true,\"min\":false,\"show\":true,\"total\":false,\"values\":true},\"lines\":true,\"linewidth\":1,\"links\":[],\"nullPointMode\":\"null\",\"options\":{\"alertThreshold\":true},\"percentage\":false,\"pluginVersion\":\"10.1.4\",\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"sum(jvm_memory_used_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\", area=\\\"heap\\\"})\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"used\",\"metric\":\"\",\"refId\":\"A\",\"step\":2400},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"sum(jvm_memory_committed_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\", area=\\\"heap\\\"})\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"committed\",\"refId\":\"B\",\"step\":2400},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"sum(jvm_memory_max_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\", area=\\\"heap\\\"})\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"max\",\"refId\":\"C\",\"step\":2400}],\"thresholds\":[],\"timeRegions\":[],\"title\":\"JVM Heap\",\"tooltip\":{\"msResolution\":false,\"shared\":true,\"sort\":0,\"value_type\":\"cumulative\"},\"type\":\"graph\",\"x-axis\":true,\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"y-axis\":true,\"y_formats\":[\"mbytes\",\"short\"],\"yaxes\":[{\"format\":\"bytes\",\"logBase\":1,\"min\":0,\"show\":true},{\"format\":\"short\",\"logBase\":1,\"show\":true}],\"yaxis\":{\"align\":false}},{\"aliasColors\":{},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"editable\":true,\"error\":false,\"fill\":1,\"fillGradient\":0,\"grid\":{\"leftLogBase\":1,\"rightLogBase\":1},\"gridPos\":{\"h\":7,\"w\":6,\"x\":6,\"y\":13},\"hiddenSeries\":false,\"id\":25,\"legend\":{\"avg\":false,\"current\":true,\"max\":true,\"min\":false,\"show\":true,\"total\":false,\"values\":true},\"lines\":true,\"linewidth\":1,\"links\":[],\"nullPointMode\":\"null\",\"options\":{\"alertThreshold\":true},\"percentage\":false,\"pluginVersion\":\"10.1.4\",\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"sum(jvm_memory_used_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\", area=\\\"nonheap\\\"})\",\"format\":\"time_series\",\"interval\":\"\",\"intervalFactor\":2,\"legendFormat\":\"used\",\"metric\":\"\",\"refId\":\"A\",\"step\":2400},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"sum(jvm_memory_committed_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\", area=\\\"nonheap\\\"})\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"committed\",\"refId\":\"B\",\"step\":2400},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"sum(jvm_memory_max_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\", area=\\\"nonheap\\\"})\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"max\",\"refId\":\"C\",\"step\":2400}],\"thresholds\":[],\"timeRegions\":[],\"title\":\"JVM Non-Heap\",\"tooltip\":{\"msResolution\":false,\"shared\":true,\"sort\":0,\"value_type\":\"cumulative\"},\"type\":\"graph\",\"x-axis\":true,\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"y-axis\":true,\"y_formats\":[\"mbytes\",\"short\"],\"yaxes\":[{\"format\":\"bytes\",\"logBase\":1,\"min\":0,\"show\":true},{\"format\":\"short\",\"logBase\":1,\"show\":true}],\"yaxis\":{\"align\":false}},{\"aliasColors\":{},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"editable\":true,\"error\":false,\"fill\":1,\"fillGradient\":0,\"grid\":{\"leftLogBase\":1,\"rightLogBase\":1},\"gridPos\":{\"h\":7,\"w\":6,\"x\":12,\"y\":13},\"hiddenSeries\":false,\"id\":26,\"legend\":{\"alignAsTable\":false,\"avg\":false,\"current\":true,\"max\":true,\"min\":false,\"show\":true,\"total\":false,\"values\":true},\"lines\":true,\"linewidth\":1,\"links\":[],\"nullPointMode\":\"null\",\"options\":{\"alertThreshold\":true},\"percentage\":false,\"pluginVersion\":\"10.1.4\",\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"sum(jvm_memory_used_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\"})\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"used\",\"metric\":\"\",\"refId\":\"A\",\"step\":2400},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"sum(jvm_memory_committed_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\"})\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"committed\",\"refId\":\"B\",\"step\":2400},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"sum(jvm_memory_max_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\"})\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"max\",\"refId\":\"C\",\"step\":2400}],\"thresholds\":[],\"timeRegions\":[],\"title\":\"JVM Total\",\"tooltip\":{\"msResolution\":false,\"shared\":true,\"sort\":0,\"value_type\":\"cumulative\"},\"type\":\"graph\",\"x-axis\":true,\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"y-axis\":true,\"y_formats\":[\"mbytes\",\"short\"],\"yaxes\":[{\"format\":\"bytes\",\"label\":\"\",\"logBase\":1,\"min\":0,\"show\":true},{\"format\":\"short\",\"logBase\":1,\"show\":true}],\"yaxis\":{\"align\":false}},{\"aliasColors\":{},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"editable\":true,\"error\":false,\"fill\":1,\"fillGradient\":0,\"grid\":{\"leftLogBase\":1,\"rightLogBase\":1},\"gridPos\":{\"h\":7,\"w\":6,\"x\":18,\"y\":13},\"hiddenSeries\":false,\"id\":86,\"legend\":{\"avg\":false,\"current\":true,\"max\":true,\"min\":false,\"show\":true,\"total\":false,\"values\":true},\"lines\":true,\"linewidth\":1,\"links\":[],\"nullPointMode\":\"null\",\"options\":{\"alertThreshold\":true},\"percentage\":false,\"pluginVersion\":\"10.1.4\",\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"process_memory_vss_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"hide\":true,\"intervalFactor\":2,\"legendFormat\":\"vss\",\"metric\":\"\",\"refId\":\"A\",\"step\":2400},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"process_memory_rss_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"rss\",\"refId\":\"B\"},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"process_memory_swap_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"swap\",\"refId\":\"C\"},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"process_memory_rss_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\"} + process_memory_swap_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"total\",\"refId\":\"D\"}],\"thresholds\":[],\"timeRegions\":[],\"title\":\"JVM Process Memory\",\"tooltip\":{\"msResolution\":false,\"shared\":true,\"sort\":0,\"value_type\":\"cumulative\"},\"type\":\"graph\",\"x-axis\":true,\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"y-axis\":true,\"y_formats\":[\"mbytes\",\"short\"],\"yaxes\":[{\"format\":\"bytes\",\"label\":\"\",\"logBase\":1,\"min\":\"0\",\"show\":true},{\"format\":\"short\",\"logBase\":1,\"show\":true}],\"yaxis\":{\"align\":false}},{\"collapsed\":false,\"datasource\":{\"type\":\"datasource\",\"uid\":\"grafana\"},\"gridPos\":{\"h\":1,\"w\":24,\"x\":0,\"y\":20},\"id\":142,\"panels\":[],\"targets\":[{\"datasource\":{\"type\":\"datasource\",\"uid\":\"grafana\"},\"refId\":\"A\"}],\"title\":\"JVM Misc\",\"type\":\"row\"},{\"aliasColors\":{},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"editable\":true,\"error\":false,\"fill\":1,\"fillGradient\":0,\"grid\":{\"leftLogBase\":1,\"rightLogBase\":1},\"gridPos\":{\"h\":7,\"w\":6,\"x\":0,\"y\":21},\"hiddenSeries\":false,\"id\":106,\"legend\":{\"avg\":false,\"current\":true,\"max\":true,\"min\":false,\"show\":true,\"total\":false,\"values\":true},\"lines\":true,\"linewidth\":1,\"links\":[],\"nullPointMode\":\"null\",\"options\":{\"alertThreshold\":true},\"percentage\":false,\"pluginVersion\":\"10.1.4\",\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"system_cpu_usage{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"hide\":false,\"intervalFactor\":1,\"legendFormat\":\"system\",\"metric\":\"\",\"refId\":\"A\",\"step\":2400},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"process_cpu_usage{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"hide\":false,\"intervalFactor\":1,\"legendFormat\":\"process\",\"refId\":\"B\"},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"avg_over_time(process_cpu_usage{application=\\\"$application\\\", instance=\\\"$instance\\\"}[15m])\",\"format\":\"time_series\",\"hide\":false,\"intervalFactor\":1,\"legendFormat\":\"process-15m\",\"refId\":\"C\"}],\"thresholds\":[],\"timeRegions\":[],\"title\":\"CPU Usage\",\"tooltip\":{\"msResolution\":false,\"shared\":true,\"sort\":0,\"value_type\":\"cumulative\"},\"type\":\"graph\",\"x-axis\":true,\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"y-axis\":true,\"y_formats\":[\"short\",\"short\"],\"yaxes\":[{\"decimals\":1,\"format\":\"percentunit\",\"label\":\"\",\"logBase\":1,\"max\":\"1\",\"min\":0,\"show\":true},{\"format\":\"short\",\"logBase\":1,\"show\":true}],\"yaxis\":{\"align\":false}},{\"aliasColors\":{},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"editable\":true,\"error\":false,\"fill\":1,\"fillGradient\":0,\"grid\":{\"leftLogBase\":1,\"rightLogBase\":1},\"gridPos\":{\"h\":7,\"w\":6,\"x\":6,\"y\":21},\"hiddenSeries\":false,\"id\":93,\"legend\":{\"avg\":false,\"current\":true,\"max\":true,\"min\":false,\"show\":true,\"total\":false,\"values\":true},\"lines\":true,\"linewidth\":1,\"links\":[],\"nullPointMode\":\"null\",\"options\":{\"alertThreshold\":true},\"percentage\":false,\"pluginVersion\":\"10.1.4\",\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"system_load_average_1m{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"system-1m\",\"metric\":\"\",\"refId\":\"A\",\"step\":2400},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"system_cpu_count{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"cpus\",\"refId\":\"B\"}],\"thresholds\":[],\"timeRegions\":[],\"title\":\"Load\",\"tooltip\":{\"msResolution\":false,\"shared\":true,\"sort\":0,\"value_type\":\"cumulative\"},\"type\":\"graph\",\"x-axis\":true,\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"y-axis\":true,\"y_formats\":[\"short\",\"short\"],\"yaxes\":[{\"decimals\":1,\"format\":\"short\",\"label\":\"\",\"logBase\":1,\"min\":0,\"show\":true},{\"format\":\"short\",\"logBase\":1,\"show\":true}],\"yaxis\":{\"align\":false}},{\"aliasColors\":{},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"editable\":true,\"error\":false,\"fill\":1,\"fillGradient\":0,\"grid\":{\"leftLogBase\":1,\"rightLogBase\":1},\"gridPos\":{\"h\":7,\"w\":6,\"x\":12,\"y\":21},\"hiddenSeries\":false,\"id\":32,\"legend\":{\"avg\":false,\"current\":true,\"max\":true,\"min\":false,\"show\":true,\"total\":false,\"values\":true},\"lines\":true,\"linewidth\":1,\"links\":[],\"nullPointMode\":\"null\",\"options\":{\"alertThreshold\":true},\"percentage\":false,\"pluginVersion\":\"10.1.4\",\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"jvm_threads_live_threads{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"live\",\"metric\":\"\",\"refId\":\"A\",\"step\":2400},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"jvm_threads_daemon_threads{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"daemon\",\"metric\":\"\",\"refId\":\"B\",\"step\":2400},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"jvm_threads_peak_threads{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"peak\",\"refId\":\"C\",\"step\":2400},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"process_threads{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"interval\":\"\",\"intervalFactor\":2,\"legendFormat\":\"process\",\"refId\":\"D\",\"step\":2400}],\"thresholds\":[],\"timeRegions\":[],\"title\":\"Threads\",\"tooltip\":{\"msResolution\":false,\"shared\":true,\"sort\":0,\"value_type\":\"cumulative\"},\"type\":\"graph\",\"x-axis\":true,\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"y-axis\":true,\"y_formats\":[\"short\",\"short\"],\"yaxes\":[{\"decimals\":0,\"format\":\"short\",\"logBase\":1,\"min\":0,\"show\":true},{\"format\":\"short\",\"logBase\":1,\"show\":true}],\"yaxis\":{\"align\":false}},{\"aliasColors\":{\"blocked\":\"#bf1b00\",\"new\":\"#fce2de\",\"runnable\":\"#7eb26d\",\"terminated\":\"#511749\",\"timed-waiting\":\"#c15c17\",\"waiting\":\"#eab839\"},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"fill\":1,\"fillGradient\":0,\"gridPos\":{\"h\":7,\"w\":6,\"x\":18,\"y\":21},\"hiddenSeries\":false,\"id\":124,\"legend\":{\"alignAsTable\":false,\"avg\":false,\"current\":true,\"max\":true,\"min\":false,\"rightSide\":false,\"show\":true,\"total\":false,\"values\":true},\"lines\":true,\"linewidth\":1,\"links\":[],\"nullPointMode\":\"null\",\"options\":{\"alertThreshold\":true},\"percentage\":false,\"pluginVersion\":\"10.1.4\",\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"jvm_threads_states_threads{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"{{state}}\",\"refId\":\"A\"}],\"thresholds\":[],\"timeRegions\":[],\"title\":\"Thread States\",\"tooltip\":{\"shared\":true,\"sort\":0,\"value_type\":\"individual\"},\"type\":\"graph\",\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"yaxes\":[{\"format\":\"short\",\"logBase\":1,\"show\":true},{\"format\":\"short\",\"logBase\":1,\"show\":true}],\"yaxis\":{\"align\":false}},{\"aliasColors\":{},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"description\":\"The percent of time spent on Garbage Collection over all CPUs assigned to the JVM process.\",\"fill\":1,\"gridPos\":{\"h\":7,\"w\":6,\"x\":0,\"y\":28},\"id\":138,\"legend\":{\"avg\":false,\"current\":true,\"max\":true,\"min\":false,\"show\":true,\"total\":false,\"values\":true},\"lines\":true,\"linewidth\":1,\"links\":[],\"nullPointMode\":\"null\",\"percentage\":false,\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"sum(rate(jvm_gc_pause_seconds_sum{application=\\\"$application\\\", instance=\\\"$instance\\\"}[1m])) by (application, instance) / on(application, instance) system_cpu_count\",\"format\":\"time_series\",\"intervalFactor\":1,\"legendFormat\":\"CPU time spent on GC\",\"refId\":\"A\"}],\"thresholds\":[],\"title\":\"GC Pressure\",\"tooltip\":{\"shared\":true,\"sort\":0,\"value_type\":\"individual\"},\"type\":\"graph\",\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"yaxes\":[{\"decimals\":1,\"format\":\"percentunit\",\"logBase\":1,\"max\":\"1\",\"min\":\"0\",\"show\":true},{\"format\":\"short\",\"logBase\":1,\"show\":true}]},{\"aliasColors\":{\"debug\":\"#1F78C1\",\"error\":\"#BF1B00\",\"info\":\"#508642\",\"trace\":\"#6ED0E0\",\"warn\":\"#EAB839\"},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"editable\":true,\"error\":false,\"fill\":1,\"grid\":{\"leftLogBase\":1,\"rightLogBase\":1},\"gridPos\":{\"h\":7,\"w\":12,\"x\":6,\"y\":28},\"height\":\"\",\"id\":91,\"legend\":{\"alignAsTable\":false,\"avg\":false,\"current\":true,\"hideEmpty\":false,\"hideZero\":false,\"max\":true,\"min\":false,\"rightSide\":false,\"show\":true,\"total\":false,\"values\":true},\"lines\":true,\"linewidth\":1,\"links\":[],\"nullPointMode\":\"null\",\"percentage\":true,\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"seriesOverrides\":[{\"alias\":\"error\",\"yaxis\":1},{\"alias\":\"warn\",\"yaxis\":1}],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"increase(logback_events_total{application=\\\"$application\\\", instance=\\\"$instance\\\"}[1m])\",\"format\":\"time_series\",\"interval\":\"\",\"intervalFactor\":2,\"legendFormat\":\"{{level}}\",\"metric\":\"\",\"refId\":\"A\",\"step\":1200}],\"thresholds\":[],\"title\":\"Log Events\",\"tooltip\":{\"msResolution\":false,\"shared\":true,\"sort\":0,\"value_type\":\"individual\"},\"type\":\"graph\",\"x-axis\":true,\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"y-axis\":true,\"y_formats\":[\"short\",\"short\"],\"yaxes\":[{\"decimals\":0,\"format\":\"opm\",\"logBase\":1,\"min\":\"0\",\"show\":true},{\"format\":\"short\",\"logBase\":1,\"show\":true}]},{\"aliasColors\":{},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"editable\":true,\"error\":false,\"fill\":1,\"grid\":{\"leftLogBase\":1,\"rightLogBase\":1},\"gridPos\":{\"h\":7,\"w\":6,\"x\":18,\"y\":28},\"id\":61,\"legend\":{\"avg\":false,\"current\":true,\"max\":true,\"min\":false,\"show\":true,\"total\":false,\"values\":true},\"lines\":true,\"linewidth\":1,\"links\":[],\"nullPointMode\":\"null\",\"percentage\":false,\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"process_files_open_files{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"hide\":false,\"intervalFactor\":2,\"legendFormat\":\"open\",\"metric\":\"\",\"refId\":\"A\",\"step\":2400},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"process_files_max_files{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"hide\":false,\"intervalFactor\":2,\"legendFormat\":\"max\",\"metric\":\"\",\"refId\":\"B\",\"step\":2400}],\"thresholds\":[],\"title\":\"File Descriptors\",\"tooltip\":{\"msResolution\":false,\"shared\":true,\"sort\":0,\"value_type\":\"cumulative\"},\"type\":\"graph\",\"x-axis\":true,\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"y-axis\":true,\"y_formats\":[\"short\",\"short\"],\"yaxes\":[{\"decimals\":0,\"format\":\"short\",\"logBase\":10,\"min\":0,\"show\":true},{\"format\":\"short\",\"logBase\":1,\"show\":true}]},{\"collapsed\":false,\"datasource\":{\"type\":\"datasource\",\"uid\":\"grafana\"},\"gridPos\":{\"h\":1,\"w\":24,\"x\":0,\"y\":35},\"id\":143,\"panels\":[],\"repeat\":\"persistence_counts\",\"targets\":[{\"datasource\":{\"type\":\"datasource\",\"uid\":\"grafana\"},\"refId\":\"A\"}],\"title\":\"JVM Memory Pools (Heap)\",\"type\":\"row\"},{\"aliasColors\":{},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"editable\":true,\"error\":false,\"fill\":1,\"grid\":{\"leftLogBase\":1,\"rightLogBase\":1},\"gridPos\":{\"h\":7,\"w\":8,\"x\":0,\"y\":36},\"id\":3,\"legend\":{\"alignAsTable\":false,\"avg\":false,\"current\":true,\"max\":true,\"min\":false,\"rightSide\":false,\"show\":true,\"total\":false,\"values\":true},\"lines\":true,\"linewidth\":1,\"links\":[],\"maxPerRow\":3,\"nullPointMode\":\"null\",\"percentage\":false,\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"repeat\":\"jvm_memory_pool_heap\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"jvm_memory_used_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\", id=~\\\"$jvm_memory_pool_heap\\\"}\",\"format\":\"time_series\",\"hide\":false,\"interval\":\"\",\"intervalFactor\":2,\"legendFormat\":\"used\",\"metric\":\"\",\"refId\":\"A\",\"step\":1800},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"jvm_memory_committed_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\", id=~\\\"$jvm_memory_pool_heap\\\"}\",\"format\":\"time_series\",\"hide\":false,\"interval\":\"\",\"intervalFactor\":2,\"legendFormat\":\"commited\",\"metric\":\"\",\"refId\":\"B\",\"step\":1800},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"jvm_memory_max_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\", id=~\\\"$jvm_memory_pool_heap\\\"}\",\"format\":\"time_series\",\"hide\":false,\"interval\":\"\",\"intervalFactor\":2,\"legendFormat\":\"max\",\"metric\":\"\",\"refId\":\"C\",\"step\":1800}],\"thresholds\":[],\"title\":\"$jvm_memory_pool_heap\",\"tooltip\":{\"msResolution\":false,\"shared\":true,\"sort\":0,\"value_type\":\"cumulative\"},\"type\":\"graph\",\"x-axis\":true,\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"y-axis\":true,\"y_formats\":[\"mbytes\",\"short\"],\"yaxes\":[{\"format\":\"bytes\",\"logBase\":1,\"min\":0,\"show\":true},{\"format\":\"short\",\"logBase\":1,\"show\":true}]},{\"collapsed\":false,\"datasource\":{\"type\":\"datasource\",\"uid\":\"grafana\"},\"gridPos\":{\"h\":1,\"w\":24,\"x\":0,\"y\":43},\"id\":144,\"panels\":[],\"targets\":[{\"datasource\":{\"type\":\"datasource\",\"uid\":\"grafana\"},\"refId\":\"A\"}],\"title\":\"JVM Memory Pools (Non-Heap)\",\"type\":\"row\"},{\"aliasColors\":{},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"editable\":true,\"error\":false,\"fill\":1,\"grid\":{\"leftLogBase\":1,\"rightLogBase\":1},\"gridPos\":{\"h\":7,\"w\":8,\"x\":0,\"y\":44},\"id\":78,\"legend\":{\"alignAsTable\":false,\"avg\":false,\"current\":true,\"max\":true,\"min\":false,\"rightSide\":false,\"show\":true,\"total\":false,\"values\":true},\"lines\":true,\"linewidth\":1,\"links\":[],\"maxPerRow\":3,\"nullPointMode\":\"null\",\"percentage\":false,\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"repeat\":\"jvm_memory_pool_nonheap\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"jvm_memory_used_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\", id=~\\\"$jvm_memory_pool_nonheap\\\"}\",\"format\":\"time_series\",\"hide\":false,\"interval\":\"\",\"intervalFactor\":2,\"legendFormat\":\"used\",\"metric\":\"\",\"refId\":\"A\",\"step\":1800},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"jvm_memory_committed_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\", id=~\\\"$jvm_memory_pool_nonheap\\\"}\",\"format\":\"time_series\",\"hide\":false,\"interval\":\"\",\"intervalFactor\":2,\"legendFormat\":\"commited\",\"metric\":\"\",\"refId\":\"B\",\"step\":1800},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"jvm_memory_max_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\", id=~\\\"$jvm_memory_pool_nonheap\\\"}\",\"format\":\"time_series\",\"hide\":false,\"interval\":\"\",\"intervalFactor\":2,\"legendFormat\":\"max\",\"metric\":\"\",\"refId\":\"C\",\"step\":1800}],\"thresholds\":[],\"title\":\"$jvm_memory_pool_nonheap\",\"tooltip\":{\"msResolution\":false,\"shared\":true,\"sort\":0,\"value_type\":\"cumulative\"},\"type\":\"graph\",\"x-axis\":true,\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"y-axis\":true,\"y_formats\":[\"mbytes\",\"short\"],\"yaxes\":[{\"format\":\"bytes\",\"logBase\":1,\"min\":0,\"show\":true},{\"format\":\"short\",\"logBase\":1,\"show\":true}]},{\"collapsed\":false,\"datasource\":{\"type\":\"datasource\",\"uid\":\"grafana\"},\"gridPos\":{\"h\":1,\"w\":24,\"x\":0,\"y\":58},\"id\":145,\"panels\":[],\"targets\":[{\"datasource\":{\"type\":\"datasource\",\"uid\":\"grafana\"},\"refId\":\"A\"}],\"title\":\"Garbage Collection\",\"type\":\"row\"},{\"aliasColors\":{},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"fill\":1,\"gridPos\":{\"h\":7,\"w\":8,\"x\":0,\"y\":59},\"id\":98,\"legend\":{\"avg\":false,\"current\":false,\"max\":false,\"min\":false,\"show\":true,\"total\":false,\"values\":false},\"lines\":true,\"linewidth\":1,\"links\":[],\"nullPointMode\":\"null\",\"percentage\":false,\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"rate(jvm_gc_pause_seconds_count{application=\\\"$application\\\", instance=\\\"$instance\\\"}[1m])\",\"format\":\"time_series\",\"hide\":false,\"intervalFactor\":1,\"legendFormat\":\"{{action}} ({{cause}})\",\"refId\":\"A\"}],\"thresholds\":[],\"title\":\"Collections\",\"tooltip\":{\"shared\":true,\"sort\":0,\"value_type\":\"individual\"},\"type\":\"graph\",\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"yaxes\":[{\"format\":\"ops\",\"logBase\":1,\"min\":\"0\",\"show\":true},{\"format\":\"short\",\"label\":\"\",\"logBase\":1,\"show\":true}]},{\"aliasColors\":{},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"fill\":1,\"gridPos\":{\"h\":7,\"w\":8,\"x\":8,\"y\":59},\"id\":101,\"legend\":{\"avg\":false,\"current\":false,\"max\":false,\"min\":false,\"show\":true,\"total\":false,\"values\":false},\"lines\":true,\"linewidth\":1,\"links\":[],\"nullPointMode\":\"null\",\"percentage\":false,\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"rate(jvm_gc_pause_seconds_sum{application=\\\"$application\\\", instance=\\\"$instance\\\"}[1m])/rate(jvm_gc_pause_seconds_count{application=\\\"$application\\\", instance=\\\"$instance\\\"}[1m])\",\"format\":\"time_series\",\"hide\":false,\"instant\":false,\"intervalFactor\":1,\"legendFormat\":\"avg {{action}} ({{cause}})\",\"refId\":\"A\"},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"jvm_gc_pause_seconds_max{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"hide\":false,\"instant\":false,\"intervalFactor\":1,\"legendFormat\":\"max {{action}} ({{cause}})\",\"refId\":\"B\"}],\"thresholds\":[],\"title\":\"Pause Durations\",\"tooltip\":{\"shared\":true,\"sort\":0,\"value_type\":\"individual\"},\"type\":\"graph\",\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"yaxes\":[{\"format\":\"s\",\"logBase\":1,\"min\":\"0\",\"show\":true},{\"format\":\"short\",\"label\":\"\",\"logBase\":1,\"show\":true}]},{\"aliasColors\":{},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"fill\":1,\"gridPos\":{\"h\":7,\"w\":8,\"x\":16,\"y\":59},\"id\":99,\"legend\":{\"avg\":false,\"current\":false,\"max\":false,\"min\":false,\"show\":true,\"total\":false,\"values\":false},\"lines\":true,\"linewidth\":1,\"links\":[],\"nullPointMode\":\"null\",\"percentage\":false,\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"rate(jvm_gc_memory_allocated_bytes_total{application=\\\"$application\\\", instance=\\\"$instance\\\"}[1m])\",\"format\":\"time_series\",\"interval\":\"\",\"intervalFactor\":1,\"legendFormat\":\"allocated\",\"refId\":\"A\"},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"rate(jvm_gc_memory_promoted_bytes_total{application=\\\"$application\\\", instance=\\\"$instance\\\"}[1m])\",\"format\":\"time_series\",\"interval\":\"\",\"intervalFactor\":1,\"legendFormat\":\"promoted\",\"refId\":\"B\"}],\"thresholds\":[],\"title\":\"Allocated/Promoted\",\"tooltip\":{\"shared\":true,\"sort\":0,\"value_type\":\"individual\"},\"type\":\"graph\",\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"yaxes\":[{\"format\":\"Bps\",\"logBase\":1,\"min\":\"0\",\"show\":true},{\"format\":\"short\",\"logBase\":1,\"show\":true}]},{\"collapsed\":false,\"datasource\":{\"type\":\"datasource\",\"uid\":\"grafana\"},\"gridPos\":{\"h\":1,\"w\":24,\"x\":0,\"y\":66},\"id\":146,\"panels\":[],\"targets\":[{\"datasource\":{\"type\":\"datasource\",\"uid\":\"grafana\"},\"refId\":\"A\"}],\"title\":\"Classloading\",\"type\":\"row\"},{\"aliasColors\":{},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"editable\":true,\"error\":false,\"fill\":1,\"grid\":{\"leftLogBase\":1,\"rightLogBase\":1},\"gridPos\":{\"h\":7,\"w\":12,\"x\":0,\"y\":67},\"id\":37,\"legend\":{\"avg\":false,\"current\":false,\"max\":false,\"min\":false,\"show\":true,\"total\":false,\"values\":false},\"lines\":true,\"linewidth\":1,\"links\":[],\"nullPointMode\":\"null\",\"percentage\":false,\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"jvm_classes_loaded_classes{application=\\\"$application\\\", instance=\\\"$instance\\\"}\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"loaded\",\"metric\":\"\",\"refId\":\"A\",\"step\":1200}],\"thresholds\":[],\"title\":\"Classes loaded\",\"tooltip\":{\"msResolution\":false,\"shared\":true,\"sort\":0,\"value_type\":\"cumulative\"},\"type\":\"graph\",\"x-axis\":true,\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"y-axis\":true,\"y_formats\":[\"short\",\"short\"],\"yaxes\":[{\"format\":\"short\",\"logBase\":1,\"min\":0,\"show\":true},{\"format\":\"short\",\"logBase\":1,\"show\":true}]},{\"aliasColors\":{},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"editable\":true,\"error\":false,\"fill\":1,\"grid\":{\"leftLogBase\":1,\"rightLogBase\":1},\"gridPos\":{\"h\":7,\"w\":12,\"x\":12,\"y\":67},\"id\":38,\"legend\":{\"avg\":false,\"current\":false,\"max\":false,\"min\":false,\"show\":true,\"total\":false,\"values\":false},\"lines\":true,\"linewidth\":1,\"links\":[],\"nullPointMode\":\"null\",\"percentage\":false,\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"seriesOverrides\":[],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"delta(jvm_classes_loaded_classes{application=\\\"$application\\\",instance=\\\"$instance\\\"}[1m])\",\"format\":\"time_series\",\"hide\":false,\"interval\":\"\",\"intervalFactor\":1,\"legendFormat\":\"delta-1m\",\"metric\":\"\",\"refId\":\"A\",\"step\":1200}],\"thresholds\":[],\"title\":\"Class delta\",\"tooltip\":{\"msResolution\":false,\"shared\":true,\"sort\":0,\"value_type\":\"cumulative\"},\"type\":\"graph\",\"x-axis\":true,\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"y-axis\":true,\"y_formats\":[\"ops\",\"short\"],\"yaxes\":[{\"format\":\"short\",\"label\":\"\",\"logBase\":1,\"show\":true},{\"format\":\"short\",\"logBase\":1,\"show\":true}]},{\"collapsed\":false,\"datasource\":{\"type\":\"datasource\",\"uid\":\"grafana\"},\"gridPos\":{\"h\":1,\"w\":24,\"x\":0,\"y\":74},\"id\":147,\"panels\":[],\"targets\":[{\"datasource\":{\"type\":\"datasource\",\"uid\":\"grafana\"},\"refId\":\"A\"}],\"title\":\"Buffer Pools\",\"type\":\"row\"},{\"aliasColors\":{},\"bars\":false,\"dashLength\":10,\"dashes\":false,\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"fill\":1,\"gridPos\":{\"h\":7,\"w\":8,\"x\":0,\"y\":75},\"id\":131,\"legend\":{\"avg\":false,\"current\":false,\"max\":false,\"min\":false,\"show\":true,\"total\":false,\"values\":false},\"lines\":true,\"linewidth\":1,\"links\":[],\"maxPerRow\":3,\"nullPointMode\":\"null\",\"percentage\":false,\"pointradius\":5,\"points\":false,\"renderer\":\"flot\",\"repeat\":\"jvm_buffer_pool\",\"seriesOverrides\":[{\"alias\":\"count\",\"yaxis\":2},{\"alias\":\"buffers\",\"yaxis\":2}],\"spaceLength\":10,\"stack\":false,\"steppedLine\":false,\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"jvm_buffer_memory_used_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\", id=~\\\"$jvm_buffer_pool\\\"}\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"used\",\"refId\":\"A\"},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"jvm_buffer_total_capacity_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\", id=~\\\"$jvm_buffer_pool\\\"}\",\"format\":\"time_series\",\"intervalFactor\":2,\"legendFormat\":\"capacity\",\"refId\":\"B\"},{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"expr\":\"jvm_buffer_count_buffers{application=\\\"$application\\\", instance=\\\"$instance\\\", id=~\\\"$jvm_buffer_pool\\\"}\",\"format\":\"time_series\",\"hide\":false,\"intervalFactor\":2,\"legendFormat\":\"buffers\",\"refId\":\"C\"}],\"thresholds\":[],\"title\":\"$jvm_buffer_pool\",\"tooltip\":{\"shared\":true,\"sort\":0,\"value_type\":\"individual\"},\"type\":\"graph\",\"xaxis\":{\"mode\":\"time\",\"show\":true,\"values\":[]},\"yaxes\":[{\"format\":\"decbytes\",\"logBase\":1,\"min\":\"0\",\"show\":true},{\"decimals\":0,\"format\":\"short\",\"label\":\"\",\"logBase\":1,\"min\":\"0\",\"show\":true}]}],\"refresh\":\"30s\",\"schemaVersion\":38,\"style\":\"dark\",\"tags\":[],\"templating\":{\"list\":[{\"current\":{},\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"definition\":\"\",\"hide\":0,\"includeAll\":false,\"label\":\"Application\",\"multi\":false,\"name\":\"application\",\"options\":[],\"query\":\"label_values(application)\",\"refresh\":2,\"regex\":\"\",\"skipUrlSync\":false,\"sort\":0,\"tagValuesQuery\":\"\",\"tagsQuery\":\"\",\"type\":\"query\",\"useTags\":false},{\"allFormat\":\"glob\",\"current\":{},\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"definition\":\"\",\"hide\":0,\"includeAll\":false,\"label\":\"Instance\",\"multi\":false,\"multiFormat\":\"glob\",\"name\":\"instance\",\"options\":[],\"query\":\"label_values(jvm_memory_used_bytes{application=\\\"$application\\\"}, instance)\",\"refresh\":2,\"regex\":\"\",\"skipUrlSync\":false,\"sort\":0,\"tagValuesQuery\":\"\",\"tagsQuery\":\"\",\"type\":\"query\",\"useTags\":false},{\"allFormat\":\"glob\",\"current\":{},\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"definition\":\"\",\"hide\":2,\"includeAll\":true,\"label\":\"JVM Memory Pools Heap\",\"multi\":false,\"multiFormat\":\"glob\",\"name\":\"jvm_memory_pool_heap\",\"options\":[],\"query\":\"label_values(jvm_memory_used_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\", area=\\\"heap\\\"},id)\",\"refresh\":1,\"regex\":\"\",\"skipUrlSync\":false,\"sort\":1,\"tagValuesQuery\":\"\",\"tagsQuery\":\"\",\"type\":\"query\",\"useTags\":false},{\"allFormat\":\"glob\",\"current\":{},\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"definition\":\"\",\"hide\":2,\"includeAll\":true,\"label\":\"JVM Memory Pools Non-Heap\",\"multi\":false,\"multiFormat\":\"glob\",\"name\":\"jvm_memory_pool_nonheap\",\"options\":[],\"query\":\"label_values(jvm_memory_used_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\", area=\\\"nonheap\\\"},id)\",\"refresh\":1,\"regex\":\"\",\"skipUrlSync\":false,\"sort\":2,\"tagValuesQuery\":\"\",\"tagsQuery\":\"\",\"type\":\"query\",\"useTags\":false},{\"allFormat\":\"glob\",\"current\":{},\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"definition\":\"\",\"hide\":2,\"includeAll\":true,\"label\":\"JVM Buffer Pools\",\"multi\":false,\"multiFormat\":\"glob\",\"name\":\"jvm_buffer_pool\",\"options\":[],\"query\":\"label_values(jvm_buffer_memory_used_bytes{application=\\\"$application\\\", instance=\\\"$instance\\\"},id)\",\"refresh\":1,\"regex\":\"\",\"skipUrlSync\":false,\"sort\":1,\"tagValuesQuery\":\"\",\"tagsQuery\":\"\",\"type\":\"query\",\"useTags\":false}]},\"time\":{\"from\":\"now-24h\",\"to\":\"now\"},\"timepicker\":{\"now\":true,\"refresh_intervals\":[\"5s\",\"10s\",\"30s\",\"1m\",\"5m\",\"15m\",\"30m\",\"1h\",\"2h\",\"1d\"],\"time_options\":[\"5m\",\"15m\",\"1h\",\"6h\",\"12h\",\"24h\",\"2d\",\"7d\",\"30d\"]},\"timezone\":\"browser\",\"title\":\"Informacije o aplikaciji\",\"uid\":\"adb554c0-d86a-489d-8863-7fe8ba79e25c\",\"version\":2,\"weekStart\":\"\"}',1,'2023-09-30 08:54:23','2023-09-30 08:58:31',1,1,4701,'',0,0,0,'adb554c0-d86a-489d-8863-7fe8ba79e25c',0),(5,1,'demo-prometheus','Demo prometheus','{\"annotations\":{\"list\":[{\"builtIn\":1,\"datasource\":{\"type\":\"grafana\",\"uid\":\"-- Grafana --\"},\"enable\":true,\"hide\":true,\"iconColor\":\"rgba(0, 211, 255, 1)\",\"name\":\"Annotations \\u0026 Alerts\",\"type\":\"dashboard\"}]},\"editable\":true,\"fiscalYearStartMonth\":0,\"graphTooltip\":0,\"id\":null,\"links\":[],\"liveNow\":false,\"panels\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"fieldConfig\":{\"defaults\":{\"color\":{\"mode\":\"palette-classic\"},\"custom\":{\"axisCenteredZero\":false,\"axisColorMode\":\"text\",\"axisLabel\":\"\",\"axisPlacement\":\"auto\",\"barAlignment\":0,\"drawStyle\":\"line\",\"fillOpacity\":0,\"gradientMode\":\"none\",\"hideFrom\":{\"legend\":false,\"tooltip\":false,\"viz\":false},\"insertNulls\":false,\"lineInterpolation\":\"linear\",\"lineWidth\":1,\"pointSize\":5,\"scaleDistribution\":{\"type\":\"linear\"},\"showPoints\":\"auto\",\"spanNulls\":false,\"stacking\":{\"group\":\"A\",\"mode\":\"none\"},\"thresholdsStyle\":{\"mode\":\"off\"}},\"mappings\":[],\"thresholds\":{\"mode\":\"absolute\",\"steps\":[{\"color\":\"green\",\"value\":null},{\"color\":\"red\",\"value\":80}]}},\"overrides\":[{\"__systemRef\":\"hideSeriesFrom\",\"matcher\":{\"id\":\"byNames\",\"options\":{\"mode\":\"exclude\",\"names\":[\"endpoint_broj_automobila_count_manual_total {__name__=\\\"endpoint_broj_automobila_count_manual_total\\\", instance=\\\"cardealershipgrafana-quarkus_app-1:8080\\\", job=\\\"autokuca\\\"}\"],\"prefix\":\"All except:\",\"readOnly\":true}},\"properties\":[{\"id\":\"custom.hideFrom\",\"value\":{\"legend\":false,\"tooltip\":false,\"viz\":true}}]}]},\"gridPos\":{\"h\":8,\"w\":12,\"x\":0,\"y\":0},\"id\":1,\"options\":{\"legend\":{\"calcs\":[],\"displayMode\":\"list\",\"placement\":\"bottom\",\"showLegend\":true},\"tooltip\":{\"mode\":\"single\",\"sort\":\"none\"}},\"targets\":[{\"datasource\":{\"type\":\"prometheus\",\"uid\":\"dcfe3162-5356-4701-b7e0-da45f9a46ea9\"},\"disableTextWrap\":false,\"editorMode\":\"builder\",\"expr\":\"endpoint_broj_automobila_count_manual_total\",\"fullMetaSearch\":false,\"includeNullMetadata\":true,\"instant\":true,\"key\":\"Q-d75fbee3-11fe-4883-a5e0-6d721688bfa5-0\",\"legendFormat\":\"{{label_name}}\",\"range\":true,\"refId\":\"A\",\"useBackend\":false}],\"title\":\"Broj poziva na \\\"autokuca/broj_automobila\\\"\",\"type\":\"timeseries\"}],\"refresh\":\"\",\"schemaVersion\":38,\"style\":\"dark\",\"tags\":[],\"templating\":{\"list\":[]},\"time\":{\"from\":\"now-15m\",\"to\":\"now\"},\"timepicker\":{},\"timezone\":\"\",\"title\":\"Demo prometheus\",\"uid\":\"cfd4cbe7-4e50-49eb-8098-439f1703d513\",\"version\":1,\"weekStart\":\"\"}',1,'2023-10-01 17:38:26','2023-10-01 17:38:26',1,1,0,'',0,0,0,'cfd4cbe7-4e50-49eb-8098-439f1703d513',0);
/*!40000 ALTER TABLE `dashboard` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `dashboard_acl`
--

DROP TABLE IF EXISTS `dashboard_acl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_acl` (
                                 `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                 `org_id` bigint(20) NOT NULL,
                                 `dashboard_id` bigint(20) NOT NULL,
                                 `user_id` bigint(20) DEFAULT NULL,
                                 `team_id` bigint(20) DEFAULT NULL,
                                 `permission` smallint(6) NOT NULL DEFAULT '4',
                                 `role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 `created` datetime NOT NULL,
                                 `updated` datetime NOT NULL,
                                 PRIMARY KEY (`id`),
                                 UNIQUE KEY `UQE_dashboard_acl_dashboard_id_user_id` (`dashboard_id`,`user_id`),
                                 UNIQUE KEY `UQE_dashboard_acl_dashboard_id_team_id` (`dashboard_id`,`team_id`),
                                 KEY `IDX_dashboard_acl_dashboard_id` (`dashboard_id`),
                                 KEY `IDX_dashboard_acl_user_id` (`user_id`),
                                 KEY `IDX_dashboard_acl_team_id` (`team_id`),
                                 KEY `IDX_dashboard_acl_org_id_role` (`org_id`,`role`),
                                 KEY `IDX_dashboard_acl_permission` (`permission`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_acl`
--

LOCK TABLES `dashboard_acl` WRITE;
/*!40000 ALTER TABLE `dashboard_acl` DISABLE KEYS */;
INSERT INTO `dashboard_acl` VALUES (1,-1,-1,NULL,NULL,1,'Viewer','2017-06-20 00:00:00','2017-06-20 00:00:00'),(2,-1,-1,NULL,NULL,2,'Editor','2017-06-20 00:00:00','2017-06-20 00:00:00');
/*!40000 ALTER TABLE `dashboard_acl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_provisioning`
--

DROP TABLE IF EXISTS `dashboard_provisioning`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_provisioning` (
                                          `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                          `dashboard_id` bigint(20) DEFAULT NULL,
                                          `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
                                          `external_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
                                          `updated` int(11) NOT NULL DEFAULT '0',
                                          `check_sum` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                          PRIMARY KEY (`id`),
                                          KEY `IDX_dashboard_provisioning_dashboard_id` (`dashboard_id`),
                                          KEY `IDX_dashboard_provisioning_dashboard_id_name` (`dashboard_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_provisioning`
--

LOCK TABLES `dashboard_provisioning` WRITE;
/*!40000 ALTER TABLE `dashboard_provisioning` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard_provisioning` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_public`
--

DROP TABLE IF EXISTS `dashboard_public`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_public` (
                                    `uid` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
                                    `dashboard_uid` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
                                    `org_id` bigint(20) NOT NULL,
                                    `time_settings` text COLLATE utf8mb4_unicode_ci,
                                    `template_variables` mediumtext COLLATE utf8mb4_unicode_ci,
                                    `access_token` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
                                    `created_by` int(11) NOT NULL,
                                    `updated_by` int(11) DEFAULT NULL,
                                    `created_at` datetime NOT NULL,
                                    `updated_at` datetime DEFAULT NULL,
                                    `is_enabled` tinyint(1) NOT NULL DEFAULT '0',
                                    `annotations_enabled` tinyint(1) NOT NULL DEFAULT '0',
                                    `time_selection_enabled` tinyint(1) NOT NULL DEFAULT '0',
                                    `share` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'public',
                                    PRIMARY KEY (`uid`),
                                    UNIQUE KEY `UQE_dashboard_public_config_uid` (`uid`),
                                    UNIQUE KEY `UQE_dashboard_public_config_access_token` (`access_token`),
                                    KEY `IDX_dashboard_public_config_org_id_dashboard_uid` (`org_id`,`dashboard_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_public`
--

LOCK TABLES `dashboard_public` WRITE;
/*!40000 ALTER TABLE `dashboard_public` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard_public` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_snapshot`
--

DROP TABLE IF EXISTS `dashboard_snapshot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_snapshot` (
                                      `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                      `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `key` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `delete_key` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `org_id` bigint(20) NOT NULL,
                                      `user_id` bigint(20) NOT NULL,
                                      `external` tinyint(1) NOT NULL,
                                      `external_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `dashboard` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `expires` datetime NOT NULL,
                                      `created` datetime NOT NULL,
                                      `updated` datetime NOT NULL,
                                      `external_delete_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `dashboard_encrypted` mediumblob,
                                      PRIMARY KEY (`id`),
                                      UNIQUE KEY `UQE_dashboard_snapshot_key` (`key`),
                                      UNIQUE KEY `UQE_dashboard_snapshot_delete_key` (`delete_key`),
                                      KEY `IDX_dashboard_snapshot_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_snapshot`
--

LOCK TABLES `dashboard_snapshot` WRITE;
/*!40000 ALTER TABLE `dashboard_snapshot` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard_snapshot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_tag`
--

DROP TABLE IF EXISTS `dashboard_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_tag` (
                                 `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                 `dashboard_id` bigint(20) NOT NULL,
                                 `term` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 PRIMARY KEY (`id`),
                                 KEY `IDX_dashboard_tag_dashboard_id` (`dashboard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_tag`
--

LOCK TABLES `dashboard_tag` WRITE;
/*!40000 ALTER TABLE `dashboard_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_version`
--

DROP TABLE IF EXISTS `dashboard_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_version` (
                                     `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                     `dashboard_id` bigint(20) NOT NULL,
                                     `parent_version` int(11) NOT NULL,
                                     `restored_from` int(11) NOT NULL,
                                     `version` int(11) NOT NULL,
                                     `created` datetime NOT NULL,
                                     `created_by` bigint(20) NOT NULL,
                                     `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
                                     `data` mediumtext COLLATE utf8mb4_unicode_ci,
                                     PRIMARY KEY (`id`),
                                     UNIQUE KEY `UQE_dashboard_version_dashboard_id_version` (`dashboard_id`,`version`),
                                     KEY `IDX_dashboard_version_dashboard_id` (`dashboard_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_version`
--

LOCK TABLES `dashboard_version` WRITE;
/*!40000 ALTER TABLE `dashboard_version` DISABLE KEYS */;
INSERT INTO `dashboard_version` VALUES (2,2,1,0,1,'2023-09-28 21:05:42',1,'','{\"annotations\":{\"list\":[{\"builtIn\":1,\"datasource\":{\"type\":\"grafana\",\"uid\":\"-- Grafana --\"},\"enable\":true,\"hide\":true,\"iconColor\":\"rgba(0, 211, 255, 1)\",\"name\":\"Annotations \\u0026 Alerts\",\"type\":\"dashboard\"}]},\"editable\":true,\"fiscalYearStartMonth\":0,\"graphTooltip\":0,\"id\":null,\"links\":[],\"liveNow\":false,\"panels\":[{\"datasource\":{\"type\":\"testdata\",\"uid\":\"P814D78962B0F8AC2\"},\"fieldConfig\":{\"defaults\":{\"color\":{\"mode\":\"thresholds\"},\"mappings\":[],\"thresholds\":{\"mode\":\"absolute\",\"steps\":[{\"color\":\"green\",\"value\":null},{\"color\":\"red\",\"value\":80}]}},\"overrides\":[]},\"gridPos\":{\"h\":8,\"w\":12,\"x\":0,\"y\":0},\"id\":3,\"options\":{\"displayMode\":\"gradient\",\"minVizHeight\":10,\"minVizWidth\":0,\"orientation\":\"auto\",\"reduceOptions\":{\"calcs\":[\"lastNotNull\"],\"fields\":\"\",\"values\":false},\"showUnfilled\":true,\"valueMode\":\"color\"},\"pluginVersion\":\"10.1.2\",\"targets\":[{\"csvWave\":[{\"name\":\"Volkswagen\",\"timeStep\":60,\"valuesCSV\":\"10,65,22,14\"}],\"datasource\":{\"type\":\"testdata\",\"uid\":\"P814D78962B0F8AC2\"},\"refId\":\"Skoda\",\"scenarioId\":\"predictable_csv_wave\"}],\"title\":\"Mjesecna prodaja automobila\",\"transformations\":[{\"id\":\"calculateField\",\"options\":{\"alias\":\"BMW\",\"mode\":\"reduceRow\",\"reduce\":{\"reducer\":\"sum\"}}},{\"id\":\"calculateField\",\"options\":{\"alias\":\"Mercedes-Benz\",\"mode\":\"reduceRow\",\"reduce\":{\"reducer\":\"sum\"}}},{\"id\":\"calculateField\",\"options\":{\"alias\":\"Audi\",\"mode\":\"reduceRow\",\"reduce\":{\"reducer\":\"sum\"}}}],\"type\":\"bargauge\"},{\"datasource\":{\"type\":\"mysql\",\"uid\":\"P8462547643F503B1\"},\"fieldConfig\":{\"defaults\":{\"color\":{\"mode\":\"thresholds\"},\"mappings\":[],\"thresholds\":{\"mode\":\"absolute\",\"steps\":[{\"color\":\"green\",\"value\":null},{\"color\":\"red\",\"value\":80}]}},\"overrides\":[]},\"gridPos\":{\"h\":8,\"w\":12,\"x\":12,\"y\":0},\"id\":2,\"maxDataPoints\":1,\"options\":{\"colorMode\":\"value\",\"graphMode\":\"area\",\"justifyMode\":\"auto\",\"orientation\":\"auto\",\"reduceOptions\":{\"calcs\":[\"lastNotNull\"],\"fields\":\"\",\"values\":false},\"textMode\":\"auto\"},\"pluginVersion\":\"10.1.2\",\"targets\":[{\"dataset\":\"autokuca\",\"datasource\":{\"type\":\"mysql\",\"uid\":\"P8462547643F503B1\"},\"editorMode\":\"code\",\"format\":\"table\",\"rawQuery\":true,\"rawSql\":\"SELECT COUNT(*) FROM autokuca.automobili WHERE na_servisu=0\",\"refId\":\"A\",\"sql\":{\"columns\":[{\"parameters\":[],\"type\":\"function\"}],\"groupBy\":[{\"property\":{\"type\":\"string\"},\"type\":\"groupBy\"}],\"limit\":50}}],\"title\":\"Broj dostupnih vozila\",\"type\":\"stat\"},{\"datasource\":{\"type\":\"testdata\",\"uid\":\"P814D78962B0F8AC2\"},\"description\":\"\",\"fieldConfig\":{\"defaults\":{\"color\":{\"mode\":\"palette-classic\"},\"custom\":{\"axisCenteredZero\":false,\"axisColorMode\":\"text\",\"axisLabel\":\"\",\"axisPlacement\":\"auto\",\"barAlignment\":0,\"drawStyle\":\"line\",\"fillOpacity\":0,\"gradientMode\":\"none\",\"hideFrom\":{\"legend\":false,\"tooltip\":false,\"viz\":false},\"insertNulls\":false,\"lineInterpolation\":\"linear\",\"lineWidth\":1,\"pointSize\":5,\"scaleDistribution\":{\"type\":\"linear\"},\"showPoints\":\"auto\",\"spanNulls\":false,\"stacking\":{\"group\":\"A\",\"mode\":\"none\"},\"thresholdsStyle\":{\"mode\":\"off\"}},\"mappings\":[],\"thresholds\":{\"mode\":\"absolute\",\"steps\":[{\"color\":\"green\",\"value\":null},{\"color\":\"red\",\"value\":80}]}},\"overrides\":[]},\"gridPos\":{\"h\":8,\"w\":12,\"x\":0,\"y\":8},\"id\":1,\"maxDataPoints\":200,\"options\":{\"candleStyle\":\"candles\",\"colorStrategy\":\"open-close\",\"colors\":{\"down\":\"red\",\"up\":\"green\"},\"includeAllFields\":false,\"legend\":{\"calcs\":[],\"displayMode\":\"list\",\"placement\":\"bottom\",\"showLegend\":true},\"mode\":\"candles\"},\"pluginVersion\":\"9.5.1\",\"targets\":[{\"datasource\":{\"type\":\"testdata\",\"uid\":\"P814D78962B0F8AC2\"},\"refId\":\"A\",\"scenarioId\":\"random_walk\",\"spread\":300300}],\"title\":\"Mjesecna zarada\",\"transparent\":true,\"type\":\"candlestick\"}],\"refresh\":\"\",\"schemaVersion\":38,\"style\":\"dark\",\"tags\":[],\"templating\":{\"list\":[]},\"time\":{\"from\":\"now-30d\",\"to\":\"now\"},\"timepicker\":{},\"timezone\":\"\",\"title\":\"Demo dashboard (Auto kuca)\",\"uid\":\"f2945aef-5b5a-4e47-90dc-de604270e1fa\",\"version\":1,\"weekStart\":\"\"}'),(3,3,0,0,1,'2023-09-28 21:31:49',1,'','{\"schemaVersion\":17,\"title\":\"Stanje Auto Kuce\",\"uid\":\"b38e7302-eee9-49b1-abae-2d561374371c\",\"version\":1}');
/*!40000 ALTER TABLE `dashboard_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_keys`
--

DROP TABLE IF EXISTS `data_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_keys` (
                             `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `active` tinyint(1) NOT NULL,
                             `scope` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `provider` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `encrypted_data` blob NOT NULL,
                             `created` datetime NOT NULL,
                             `updated` datetime NOT NULL,
                             `label` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_keys`
--

LOCK TABLES `data_keys` WRITE;
/*!40000 ALTER TABLE `data_keys` DISABLE KEYS */;
INSERT INTO `data_keys` VALUES ('a76a1367-327a-450f-b62f-1e2f0196baca',1,'root','secretKey.v1',0x2A5957567A4C574E6D59672A4F6C776845503171EFBFBD28EFBFBD6067EFBFBDEFBFBDEFBFBDC2A8EFBFBDDEACEFBFBD2B63EFBFBDEFBFBDEFBFBD273FEFBFBD23CD8FEFBFBD,'2023-09-28 20:45:57','2023-09-28 20:45:57','2023-09-28/root@secretKey.v1'),('af9f8c31-d836-45ee-98c2-cccdad8d36a9',1,'root','secretKey.v1',0x2A5957567A4C574E6D59672A587349386467645ADAA3854AE5661F4F8AAD23E4C9269B845068B7F682D4434EE924C8511947B245,'2023-09-29 13:45:12','2023-09-29 13:45:12','2023-09-29/root@secretKey.v1');
/*!40000 ALTER TABLE `data_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_source`
--

DROP TABLE IF EXISTS `data_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_source` (
                               `id` bigint(20) NOT NULL AUTO_INCREMENT,
                               `org_id` bigint(20) NOT NULL,
                               `version` int(11) NOT NULL,
                               `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `access` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `database` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `basic_auth` tinyint(1) NOT NULL,
                               `basic_auth_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `basic_auth_password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `is_default` tinyint(1) NOT NULL,
                               `json_data` text COLLATE utf8mb4_unicode_ci,
                               `created` datetime NOT NULL,
                               `updated` datetime NOT NULL,
                               `with_credentials` tinyint(1) NOT NULL DEFAULT '0',
                               `secure_json_data` text COLLATE utf8mb4_unicode_ci,
                               `read_only` tinyint(1) DEFAULT NULL,
                               `uid` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
                               PRIMARY KEY (`id`),
                               UNIQUE KEY `UQE_data_source_org_id_name` (`org_id`,`name`),
                               UNIQUE KEY `UQE_data_source_org_id_uid` (`org_id`,`uid`),
                               KEY `IDX_data_source_org_id` (`org_id`),
                               KEY `IDX_data_source_org_id_is_default` (`org_id`,`is_default`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_source`
--

LOCK TABLES `data_source` WRITE;
/*!40000 ALTER TABLE `data_source` DISABLE KEYS */;
INSERT INTO `data_source` VALUES (1,1,1,'testdata','TestData','proxy','','','','',0,'','',0,'{}','2023-09-28 20:45:57','2023-09-28 20:47:45',0,'{}',1,'P814D78962B0F8AC2'),(2,1,2,'mysql','Auto Kuca','proxy','cardealershipgrafana-mysql-1:3306','','root','',0,'','',0,'{\"connMaxLifetime\":14400,\"database\":\"autokuca\",\"maxIdleConns\":100,\"maxIdleConnsAuto\":true,\"maxOpenConns\":100}','2023-09-29 13:45:12','2023-09-29 13:45:23',0,'{\"password\":\"I1lXWTVaamhqTXpFdFpEZ3pOaTAwTldWbExUazRZekl0WTJOalpHRmtPR1F6Tm1FNSMqWVdWekxXTm1ZZypXN2JCTElnWT7bMvITrJvD1mGUREgj8hyEeIjlnk8l4zh3IJk=\"}',0,'P8462547643F503B1'),(4,1,2,'prometheus','AutoKuca-Prometheus','proxy','http://cardealershipgrafana-prometheus-1:9090','','','',0,'','',0,'{\"httpMethod\":\"POST\"}','2023-09-30 08:52:27','2023-09-30 08:52:42',0,'{}',0,'dcfe3162-5356-4701-b7e0-da45f9a46ea9');
/*!40000 ALTER TABLE `data_source` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `entity_event`
--

DROP TABLE IF EXISTS `entity_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entity_event` (
                                `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                `entity_id` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
                                `event_type` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
                                `created` bigint(20) NOT NULL,
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entity_event`
--

LOCK TABLES `entity_event` WRITE;
/*!40000 ALTER TABLE `entity_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `entity_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file`
--

DROP TABLE IF EXISTS `file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file` (
                        `path` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `path_hash` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `parent_folder_path_hash` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `contents` mediumblob,
                        `etag` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `cache_control` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `content_disposition` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `updated` datetime NOT NULL,
                        `created` datetime NOT NULL,
                        `size` bigint(20) NOT NULL,
                        `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                        UNIQUE KEY `UQE_file_path_hash` (`path_hash`),
                        KEY `IDX_file_parent_folder_path_hash` (`parent_folder_path_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file`
--

LOCK TABLES `file` WRITE;
/*!40000 ALTER TABLE `file` DISABLE KEYS */;
/*!40000 ALTER TABLE `file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_meta`
--

DROP TABLE IF EXISTS `file_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_meta` (
                             `path_hash` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `value` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
                             UNIQUE KEY `UQE_file_meta_path_hash_key` (`path_hash`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_meta`
--

LOCK TABLES `file_meta` WRITE;
/*!40000 ALTER TABLE `file_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folder`
--

DROP TABLE IF EXISTS `folder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folder` (
                          `id` bigint(20) NOT NULL AUTO_INCREMENT,
                          `uid` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
                          `org_id` bigint(20) NOT NULL,
                          `title` varchar(189) COLLATE utf8mb4_unicode_ci NOT NULL,
                          `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                          `parent_uid` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                          `created` datetime NOT NULL,
                          `updated` datetime NOT NULL,
                          PRIMARY KEY (`id`),
                          UNIQUE KEY `UQE_folder_uid_org_id` (`uid`,`org_id`),
                          UNIQUE KEY `UQE_folder_title_parent_uid` (`title`,`parent_uid`),
                          KEY `IDX_folder_parent_uid_org_id` (`parent_uid`,`org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folder`
--

LOCK TABLES `folder` WRITE;
/*!40000 ALTER TABLE `folder` DISABLE KEYS */;
/*!40000 ALTER TABLE `folder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kv_store`
--

DROP TABLE IF EXISTS `kv_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kv_store` (
                            `id` bigint(20) NOT NULL AUTO_INCREMENT,
                            `org_id` bigint(20) NOT NULL,
                            `namespace` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                            `key` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                            `value` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
                            `created` datetime NOT NULL,
                            `updated` datetime NOT NULL,
                            PRIMARY KEY (`id`),
                            UNIQUE KEY `UQE_kv_store_org_id_namespace_key` (`org_id`,`namespace`,`key`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kv_store`
--

LOCK TABLES `kv_store` WRITE;
/*!40000 ALTER TABLE `kv_store` DISABLE KEYS */;
INSERT INTO `kv_store` VALUES (1,0,'datasource','secretMigrationStatus','compatible','2023-09-28 20:45:57','2023-09-28 20:45:57'),(2,0,'plugin.publickeys','key-7e4d0c6a708866e7','-----BEGIN PGP PUBLIC KEY BLOCK-----\r\nVersion: OpenPGP.js v4.10.1\r\nComment: https://openpgpjs.org\r\n\r\nxpMEXpTXXxMFK4EEACMEIwQBiOUQhvGbDLvndE0fEXaR0908wXzPGFpf0P0Z\r\nHJ06tsq+0higIYHp7WTNJVEZtcwoYLcPRGaa9OQqbUU63BEyZdgAkPTz3RFd\r\n5+TkDWZizDcaVFhzbDd500yTwexrpIrdInwC/jrgs7Zy/15h8KA59XXUkdmT\r\nYB6TR+OA9RKME+dCJozNGUdyYWZhbmEgPGVuZ0BncmFmYW5hLmNvbT7CvAQQ\r\nEwoAIAUCXpTXXwYLCQcIAwIEFQgKAgQWAgEAAhkBAhsDAh4BAAoJEH5NDGpw\r\niGbnaWoCCQGQ3SQnCkRWrG6XrMkXOKfDTX2ow9fuoErN46BeKmLM4f1EkDZQ\r\nTpq3SE8+My8B5BIH3SOcBeKzi3S57JHGBdFA+wIJAYWMrJNIvw8GeXne+oUo\r\nNzzACdvfqXAZEp/HFMQhCKfEoWGJE8d2YmwY2+3GufVRTI5lQnZOHLE8L/Vc\r\n1S5MXESjzpcEXpTXXxIFK4EEACMEIwQBtHX/SD5Qm3v4V92qpaIZQgtTX0sT\r\ncFPjYWAHqsQ1iENrYN/vg1wU3ADlYATvydOQYvkTyT/tbDvx2Fse8PL84MQA\r\nYKKQ6AJ3gLVvmeouZdU03YoV4MYaT8KbnJUkZQZkqdz2riOlySNI9CG3oYmv\r\nomjUAtzgAgnCcurfGLZkkMxlmY8DAQoJwqQEGBMKAAkFAl6U118CGwwACgkQ\r\nfk0ManCIZuc0jAIJAVw2xdLr4ZQqPUhubrUyFcqlWoW8dQoQagwO8s8ubmby\r\nKuLA9FWJkfuuRQr+O9gHkDVCez3aism7zmJBqIOi38aNAgjJ3bo6leSS2jR/\r\nx5NqiKVi83tiXDPncDQYPymOnMhW0l7CVA7wj75HrFvvlRI/4MArlbsZ2tBn\r\nN1c5v9v/4h6qeA==\r\n=DNbR\r\n-----END PGP PUBLIC KEY BLOCK-----\r\n','2023-09-28 20:45:57','2023-09-28 20:45:57'),(3,0,'plugin.publickeys','last_updated','2023-09-28T20:45:57Z','2023-09-28 20:45:57','2023-09-28 20:45:57'),(4,1,'alertmanager','silences','','2023-09-28 20:47:37','2023-09-28 20:47:37'),(5,1,'alertmanager','notifications','','2023-09-28 20:47:37','2023-09-28 20:47:37');
/*!40000 ALTER TABLE `kv_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library_element`
--

DROP TABLE IF EXISTS `library_element`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library_element` (
                                   `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                   `org_id` bigint(20) NOT NULL,
                                   `folder_id` bigint(20) NOT NULL,
                                   `uid` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `kind` bigint(20) NOT NULL,
                                   `type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `description` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `model` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `created` datetime NOT NULL,
                                   `created_by` bigint(20) NOT NULL,
                                   `updated` datetime NOT NULL,
                                   `updated_by` bigint(20) NOT NULL,
                                   `version` bigint(20) NOT NULL,
                                   PRIMARY KEY (`id`),
                                   UNIQUE KEY `UQE_library_element_org_id_folder_id_name_kind` (`org_id`,`folder_id`,`name`,`kind`),
                                   UNIQUE KEY `UQE_library_element_org_id_uid` (`org_id`,`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library_element`
--

LOCK TABLES `library_element` WRITE;
/*!40000 ALTER TABLE `library_element` DISABLE KEYS */;
/*!40000 ALTER TABLE `library_element` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library_element_connection`
--

DROP TABLE IF EXISTS `library_element_connection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library_element_connection` (
                                              `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                              `element_id` bigint(20) NOT NULL,
                                              `kind` bigint(20) NOT NULL,
                                              `connection_id` bigint(20) NOT NULL,
                                              `created` datetime NOT NULL,
                                              `created_by` bigint(20) NOT NULL,
                                              PRIMARY KEY (`id`),
                                              UNIQUE KEY `UQE_library_element_connection_element_id_kind_connection_id` (`element_id`,`kind`,`connection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library_element_connection`
--

LOCK TABLES `library_element_connection` WRITE;
/*!40000 ALTER TABLE `library_element_connection` DISABLE KEYS */;
/*!40000 ALTER TABLE `library_element_connection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_attempt`
--

DROP TABLE IF EXISTS `login_attempt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_attempt` (
                                 `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                 `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `ip_address` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `created` int(11) NOT NULL DEFAULT '0',
                                 PRIMARY KEY (`id`),
                                 KEY `IDX_login_attempt_username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_attempt`
--

LOCK TABLES `login_attempt` WRITE;
/*!40000 ALTER TABLE `login_attempt` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_attempt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_log`
--

DROP TABLE IF EXISTS `migration_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_log` (
                                 `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                 `migration_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `sql` text COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `success` tinyint(1) NOT NULL,
                                 `error` text COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `timestamp` datetime NOT NULL,
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=497 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_log`
--

LOCK TABLES `migration_log` WRITE;
/*!40000 ALTER TABLE `migration_log` DISABLE KEYS */;
INSERT INTO `migration_log` VALUES (1,'create migration_log table','CREATE TABLE IF NOT EXISTS `migration_log` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `migration_id` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `sql` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `success` TINYINT(1) NOT NULL\n, `error` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `timestamp` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:52'),(2,'create user table','CREATE TABLE IF NOT EXISTS `user` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `version` INT NOT NULL\n, `login` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `email` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `password` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `salt` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `rands` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `company` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `account_id` BIGINT(20) NOT NULL\n, `is_admin` TINYINT(1) NOT NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:52'),(3,'add unique index user.login','CREATE UNIQUE INDEX `UQE_user_login` ON `user` (`login`);',1,'','2023-09-28 20:45:52'),(4,'add unique index user.email','CREATE UNIQUE INDEX `UQE_user_email` ON `user` (`email`);',1,'','2023-09-28 20:45:52'),(5,'drop index UQE_user_login - v1','DROP INDEX `UQE_user_login` ON `user`',1,'','2023-09-28 20:45:52'),(6,'drop index UQE_user_email - v1','DROP INDEX `UQE_user_email` ON `user`',1,'','2023-09-28 20:45:52'),(7,'Rename table user to user_v1 - v1','ALTER TABLE `user` RENAME TO `user_v1`',1,'','2023-09-28 20:45:52'),(8,'create user table v2','CREATE TABLE IF NOT EXISTS `user` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `version` INT NOT NULL\n, `login` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `email` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `password` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `salt` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `rands` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `company` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `org_id` BIGINT(20) NOT NULL\n, `is_admin` TINYINT(1) NOT NULL\n, `email_verified` TINYINT(1) NULL\n, `theme` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:52'),(9,'create index UQE_user_login - v2','CREATE UNIQUE INDEX `UQE_user_login` ON `user` (`login`);',1,'','2023-09-28 20:45:52'),(10,'create index UQE_user_email - v2','CREATE UNIQUE INDEX `UQE_user_email` ON `user` (`email`);',1,'','2023-09-28 20:45:52'),(11,'copy data_source v1 to v2','INSERT INTO `user` (`email`\n, `name`\n, `password`\n, `rands`\n, `company`\n, `org_id`\n, `id`\n, `version`\n, `is_admin`\n, `created`\n, `updated`\n, `login`\n, `salt`) SELECT `email`\n, `name`\n, `password`\n, `rands`\n, `company`\n, `account_id`\n, `id`\n, `version`\n, `is_admin`\n, `created`\n, `updated`\n, `login`\n, `salt` FROM `user_v1`',1,'','2023-09-28 20:45:52'),(12,'Drop old table user_v1','DROP TABLE IF EXISTS `user_v1`',1,'','2023-09-28 20:45:52'),(13,'Add column help_flags1 to user table','alter table `user` ADD COLUMN `help_flags1` BIGINT(20) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:52'),(14,'Update user table charset','ALTER TABLE `user` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, MODIFY `login` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `email` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `password` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `salt` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `rands` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `company` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `theme` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ;',1,'','2023-09-28 20:45:52'),(15,'Add last_seen_at column to user','alter table `user` ADD COLUMN `last_seen_at` DATETIME NULL ',1,'','2023-09-28 20:45:52'),(16,'Add missing user data','code migration',1,'','2023-09-28 20:45:52'),(17,'Add is_disabled column to user','alter table `user` ADD COLUMN `is_disabled` TINYINT(1) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:52'),(18,'Add index user.login/user.email','CREATE INDEX `IDX_user_login_email` ON `user` (`login`,`email`);',1,'','2023-09-28 20:45:52'),(19,'Add is_service_account column to user','alter table `user` ADD COLUMN `is_service_account` TINYINT(1) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:52'),(20,'Update is_service_account column to nullable','ALTER TABLE user MODIFY is_service_account BOOLEAN DEFAULT 0;',1,'','2023-09-28 20:45:52'),(21,'create temp user table v1-7','CREATE TABLE IF NOT EXISTS `temp_user` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `version` INT NOT NULL\n, `email` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `role` VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `code` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `status` VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `invited_by_user_id` BIGINT(20) NULL\n, `email_sent` TINYINT(1) NOT NULL\n, `email_sent_on` DATETIME NULL\n, `remote_addr` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:52'),(22,'create index IDX_temp_user_email - v1-7','CREATE INDEX `IDX_temp_user_email` ON `temp_user` (`email`);',1,'','2023-09-28 20:45:52'),(23,'create index IDX_temp_user_org_id - v1-7','CREATE INDEX `IDX_temp_user_org_id` ON `temp_user` (`org_id`);',1,'','2023-09-28 20:45:52'),(24,'create index IDX_temp_user_code - v1-7','CREATE INDEX `IDX_temp_user_code` ON `temp_user` (`code`);',1,'','2023-09-28 20:45:52'),(25,'create index IDX_temp_user_status - v1-7','CREATE INDEX `IDX_temp_user_status` ON `temp_user` (`status`);',1,'','2023-09-28 20:45:52'),(26,'Update temp_user table charset','ALTER TABLE `temp_user` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, MODIFY `email` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `role` VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `code` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `status` VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `remote_addr` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ;',1,'','2023-09-28 20:45:52'),(27,'drop index IDX_temp_user_email - v1','DROP INDEX `IDX_temp_user_email` ON `temp_user`',1,'','2023-09-28 20:45:52'),(28,'drop index IDX_temp_user_org_id - v1','DROP INDEX `IDX_temp_user_org_id` ON `temp_user`',1,'','2023-09-28 20:45:52'),(29,'drop index IDX_temp_user_code - v1','DROP INDEX `IDX_temp_user_code` ON `temp_user`',1,'','2023-09-28 20:45:52'),(30,'drop index IDX_temp_user_status - v1','DROP INDEX `IDX_temp_user_status` ON `temp_user`',1,'','2023-09-28 20:45:52'),(31,'Rename table temp_user to temp_user_tmp_qwerty - v1','ALTER TABLE `temp_user` RENAME TO `temp_user_tmp_qwerty`',1,'','2023-09-28 20:45:52'),(32,'create temp_user v2','CREATE TABLE IF NOT EXISTS `temp_user` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `version` INT NOT NULL\n, `email` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `role` VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `code` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `status` VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `invited_by_user_id` BIGINT(20) NULL\n, `email_sent` TINYINT(1) NOT NULL\n, `email_sent_on` DATETIME NULL\n, `remote_addr` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `created` INT NOT NULL DEFAULT 0\n, `updated` INT NOT NULL DEFAULT 0\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:52'),(33,'create index IDX_temp_user_email - v2','CREATE INDEX `IDX_temp_user_email` ON `temp_user` (`email`);',1,'','2023-09-28 20:45:52'),(34,'create index IDX_temp_user_org_id - v2','CREATE INDEX `IDX_temp_user_org_id` ON `temp_user` (`org_id`);',1,'','2023-09-28 20:45:52'),(35,'create index IDX_temp_user_code - v2','CREATE INDEX `IDX_temp_user_code` ON `temp_user` (`code`);',1,'','2023-09-28 20:45:52'),(36,'create index IDX_temp_user_status - v2','CREATE INDEX `IDX_temp_user_status` ON `temp_user` (`status`);',1,'','2023-09-28 20:45:52'),(37,'copy temp_user v1 to v2','INSERT INTO `temp_user` (`email_sent_on`\n, `remote_addr`\n, `role`\n, `code`\n, `invited_by_user_id`\n, `email`\n, `name`\n, `status`\n, `email_sent`\n, `id`\n, `org_id`\n, `version`) SELECT `email_sent_on`\n, `remote_addr`\n, `role`\n, `code`\n, `invited_by_user_id`\n, `email`\n, `name`\n, `status`\n, `email_sent`\n, `id`\n, `org_id`\n, `version` FROM `temp_user_tmp_qwerty`',1,'','2023-09-28 20:45:52'),(38,'drop temp_user_tmp_qwerty','DROP TABLE IF EXISTS `temp_user_tmp_qwerty`',1,'','2023-09-28 20:45:52'),(39,'Set created for temp users that will otherwise prematurely expire','code migration',1,'','2023-09-28 20:45:52'),(40,'create star table','CREATE TABLE IF NOT EXISTS `star` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `user_id` BIGINT(20) NOT NULL\n, `dashboard_id` BIGINT(20) NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:52'),(41,'add unique index star.user_id_dashboard_id','CREATE UNIQUE INDEX `UQE_star_user_id_dashboard_id` ON `star` (`user_id`,`dashboard_id`);',1,'','2023-09-28 20:45:52'),(42,'create org table v1','CREATE TABLE IF NOT EXISTS `org` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `version` INT NOT NULL\n, `name` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `address1` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `address2` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `city` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `state` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `zip_code` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `country` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `billing_email` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:52'),(43,'create index UQE_org_name - v1','CREATE UNIQUE INDEX `UQE_org_name` ON `org` (`name`);',1,'','2023-09-28 20:45:52'),(44,'create org_user table v1','CREATE TABLE IF NOT EXISTS `org_user` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `user_id` BIGINT(20) NOT NULL\n, `role` VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:52'),(45,'create index IDX_org_user_org_id - v1','CREATE INDEX `IDX_org_user_org_id` ON `org_user` (`org_id`);',1,'','2023-09-28 20:45:52'),(46,'create index UQE_org_user_org_id_user_id - v1','CREATE UNIQUE INDEX `UQE_org_user_org_id_user_id` ON `org_user` (`org_id`,`user_id`);',1,'','2023-09-28 20:45:52'),(47,'create index IDX_org_user_user_id - v1','CREATE INDEX `IDX_org_user_user_id` ON `org_user` (`user_id`);',1,'','2023-09-28 20:45:52'),(48,'Update org table charset','ALTER TABLE `org` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, MODIFY `name` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `address1` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `address2` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `city` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `state` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `zip_code` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `country` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `billing_email` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ;',1,'','2023-09-28 20:45:52'),(49,'Update org_user table charset','ALTER TABLE `org_user` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, MODIFY `role` VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL ;',1,'','2023-09-28 20:45:52'),(50,'Migrate all Read Only Viewers to Viewers','UPDATE org_user SET role = \'Viewer\' WHERE role = \'Read Only Editor\'',1,'','2023-09-28 20:45:52'),(51,'create dashboard table','CREATE TABLE IF NOT EXISTS `dashboard` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `version` INT NOT NULL\n, `slug` VARCHAR(189) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `title` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `data` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `account_id` BIGINT(20) NOT NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:52'),(52,'add index dashboard.account_id','CREATE INDEX `IDX_dashboard_account_id` ON `dashboard` (`account_id`);',1,'','2023-09-28 20:45:52'),(53,'add unique index dashboard_account_id_slug','CREATE UNIQUE INDEX `UQE_dashboard_account_id_slug` ON `dashboard` (`account_id`,`slug`);',1,'','2023-09-28 20:45:52'),(54,'create dashboard_tag table','CREATE TABLE IF NOT EXISTS `dashboard_tag` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `dashboard_id` BIGINT(20) NOT NULL\n, `term` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:52'),(55,'add unique index dashboard_tag.dasboard_id_term','CREATE UNIQUE INDEX `UQE_dashboard_tag_dashboard_id_term` ON `dashboard_tag` (`dashboard_id`,`term`);',1,'','2023-09-28 20:45:52'),(56,'drop index UQE_dashboard_tag_dashboard_id_term - v1','DROP INDEX `UQE_dashboard_tag_dashboard_id_term` ON `dashboard_tag`',1,'','2023-09-28 20:45:52'),(57,'Rename table dashboard to dashboard_v1 - v1','ALTER TABLE `dashboard` RENAME TO `dashboard_v1`',1,'','2023-09-28 20:45:52'),(58,'create dashboard v2','CREATE TABLE IF NOT EXISTS `dashboard` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `version` INT NOT NULL\n, `slug` VARCHAR(189) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `title` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `data` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:52'),(59,'create index IDX_dashboard_org_id - v2','CREATE INDEX `IDX_dashboard_org_id` ON `dashboard` (`org_id`);',1,'','2023-09-28 20:45:52'),(60,'create index UQE_dashboard_org_id_slug - v2','CREATE UNIQUE INDEX `UQE_dashboard_org_id_slug` ON `dashboard` (`org_id`,`slug`);',1,'','2023-09-28 20:45:52'),(61,'copy dashboard v1 to v2','INSERT INTO `dashboard` (`created`\n, `updated`\n, `id`\n, `version`\n, `slug`\n, `title`\n, `data`\n, `org_id`) SELECT `created`\n, `updated`\n, `id`\n, `version`\n, `slug`\n, `title`\n, `data`\n, `account_id` FROM `dashboard_v1`',1,'','2023-09-28 20:45:52'),(62,'drop table dashboard_v1','DROP TABLE IF EXISTS `dashboard_v1`',1,'','2023-09-28 20:45:52'),(63,'alter dashboard.data to mediumtext v1','ALTER TABLE dashboard MODIFY data MEDIUMTEXT;',1,'','2023-09-28 20:45:52'),(64,'Add column updated_by in dashboard - v2','alter table `dashboard` ADD COLUMN `updated_by` INT NULL ',1,'','2023-09-28 20:45:52'),(65,'Add column created_by in dashboard - v2','alter table `dashboard` ADD COLUMN `created_by` INT NULL ',1,'','2023-09-28 20:45:52'),(66,'Add column gnetId in dashboard','alter table `dashboard` ADD COLUMN `gnet_id` BIGINT(20) NULL ',1,'','2023-09-28 20:45:52'),(67,'Add index for gnetId in dashboard','CREATE INDEX `IDX_dashboard_gnet_id` ON `dashboard` (`gnet_id`);',1,'','2023-09-28 20:45:52'),(68,'Add column plugin_id in dashboard','alter table `dashboard` ADD COLUMN `plugin_id` VARCHAR(189) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:52'),(69,'Add index for plugin_id in dashboard','CREATE INDEX `IDX_dashboard_org_id_plugin_id` ON `dashboard` (`org_id`,`plugin_id`);',1,'','2023-09-28 20:45:52'),(70,'Add index for dashboard_id in dashboard_tag','CREATE INDEX `IDX_dashboard_tag_dashboard_id` ON `dashboard_tag` (`dashboard_id`);',1,'','2023-09-28 20:45:52'),(71,'Update dashboard table charset','ALTER TABLE `dashboard` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, MODIFY `slug` VARCHAR(189) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `title` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `plugin_id` VARCHAR(189) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `data` MEDIUMTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL ;',1,'','2023-09-28 20:45:52'),(72,'Update dashboard_tag table charset','ALTER TABLE `dashboard_tag` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, MODIFY `term` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL ;',1,'','2023-09-28 20:45:52'),(73,'Add column folder_id in dashboard','alter table `dashboard` ADD COLUMN `folder_id` BIGINT(20) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:52'),(74,'Add column isFolder in dashboard','alter table `dashboard` ADD COLUMN `is_folder` TINYINT(1) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:52'),(75,'Add column has_acl in dashboard','alter table `dashboard` ADD COLUMN `has_acl` TINYINT(1) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:52'),(76,'Add column uid in dashboard','alter table `dashboard` ADD COLUMN `uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:52'),(77,'Update uid column values in dashboard','UPDATE dashboard SET uid=lpad(id,9,\'0\') WHERE uid IS NULL;',1,'','2023-09-28 20:45:52'),(78,'Add unique index dashboard_org_id_uid','CREATE UNIQUE INDEX `UQE_dashboard_org_id_uid` ON `dashboard` (`org_id`,`uid`);',1,'','2023-09-28 20:45:52'),(79,'Remove unique index org_id_slug','DROP INDEX `UQE_dashboard_org_id_slug` ON `dashboard`',1,'','2023-09-28 20:45:52'),(80,'Update dashboard title length','ALTER TABLE `dashboard` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, MODIFY `title` VARCHAR(189) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL ;',1,'','2023-09-28 20:45:52'),(81,'Add unique index for dashboard_org_id_title_folder_id','CREATE UNIQUE INDEX `UQE_dashboard_org_id_folder_id_title` ON `dashboard` (`org_id`,`folder_id`,`title`);',1,'','2023-09-28 20:45:52'),(82,'create dashboard_provisioning','CREATE TABLE IF NOT EXISTS `dashboard_provisioning` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `dashboard_id` BIGINT(20) NULL\n, `name` VARCHAR(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `external_id` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:52'),(83,'Rename table dashboard_provisioning to dashboard_provisioning_tmp_qwerty - v1','ALTER TABLE `dashboard_provisioning` RENAME TO `dashboard_provisioning_tmp_qwerty`',1,'','2023-09-28 20:45:52'),(84,'create dashboard_provisioning v2','CREATE TABLE IF NOT EXISTS `dashboard_provisioning` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `dashboard_id` BIGINT(20) NULL\n, `name` VARCHAR(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `external_id` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `updated` INT NOT NULL DEFAULT 0\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:52'),(85,'create index IDX_dashboard_provisioning_dashboard_id - v2','CREATE INDEX `IDX_dashboard_provisioning_dashboard_id` ON `dashboard_provisioning` (`dashboard_id`);',1,'','2023-09-28 20:45:52'),(86,'create index IDX_dashboard_provisioning_dashboard_id_name - v2','CREATE INDEX `IDX_dashboard_provisioning_dashboard_id_name` ON `dashboard_provisioning` (`dashboard_id`,`name`);',1,'','2023-09-28 20:45:52'),(87,'copy dashboard_provisioning v1 to v2','INSERT INTO `dashboard_provisioning` (`id`\n, `dashboard_id`\n, `name`\n, `external_id`) SELECT `id`\n, `dashboard_id`\n, `name`\n, `external_id` FROM `dashboard_provisioning_tmp_qwerty`',1,'','2023-09-28 20:45:52'),(88,'drop dashboard_provisioning_tmp_qwerty','DROP TABLE IF EXISTS `dashboard_provisioning_tmp_qwerty`',1,'','2023-09-28 20:45:52'),(89,'Add check_sum column','alter table `dashboard_provisioning` ADD COLUMN `check_sum` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:52'),(90,'Add index for dashboard_title','CREATE INDEX `IDX_dashboard_title` ON `dashboard` (`title`);',1,'','2023-09-28 20:45:52'),(91,'delete tags for deleted dashboards','DELETE FROM dashboard_tag WHERE dashboard_id NOT IN (SELECT id FROM dashboard)',1,'','2023-09-28 20:45:52'),(92,'delete stars for deleted dashboards','DELETE FROM star WHERE dashboard_id NOT IN (SELECT id FROM dashboard)',1,'','2023-09-28 20:45:52'),(93,'Add index for dashboard_is_folder','CREATE INDEX `IDX_dashboard_is_folder` ON `dashboard` (`is_folder`);',1,'','2023-09-28 20:45:52'),(94,'Add isPublic for dashboard','alter table `dashboard` ADD COLUMN `is_public` TINYINT(1) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:52'),(95,'create data_source table','CREATE TABLE IF NOT EXISTS `data_source` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `account_id` BIGINT(20) NOT NULL\n, `version` INT NOT NULL\n, `type` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `name` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `access` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `url` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `password` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `user` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `database` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `basic_auth` TINYINT(1) NOT NULL\n, `basic_auth_user` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `basic_auth_password` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `is_default` TINYINT(1) NOT NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:52'),(96,'add index data_source.account_id','CREATE INDEX `IDX_data_source_account_id` ON `data_source` (`account_id`);',1,'','2023-09-28 20:45:52'),(97,'add unique index data_source.account_id_name','CREATE UNIQUE INDEX `UQE_data_source_account_id_name` ON `data_source` (`account_id`,`name`);',1,'','2023-09-28 20:45:52'),(98,'drop index IDX_data_source_account_id - v1','DROP INDEX `IDX_data_source_account_id` ON `data_source`',1,'','2023-09-28 20:45:52'),(99,'drop index UQE_data_source_account_id_name - v1','DROP INDEX `UQE_data_source_account_id_name` ON `data_source`',1,'','2023-09-28 20:45:52'),(100,'Rename table data_source to data_source_v1 - v1','ALTER TABLE `data_source` RENAME TO `data_source_v1`',1,'','2023-09-28 20:45:52'),(101,'create data_source table v2','CREATE TABLE IF NOT EXISTS `data_source` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `version` INT NOT NULL\n, `type` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `name` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `access` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `url` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `password` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `user` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `database` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `basic_auth` TINYINT(1) NOT NULL\n, `basic_auth_user` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `basic_auth_password` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `is_default` TINYINT(1) NOT NULL\n, `json_data` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:52'),(102,'create index IDX_data_source_org_id - v2','CREATE INDEX `IDX_data_source_org_id` ON `data_source` (`org_id`);',1,'','2023-09-28 20:45:52'),(103,'create index UQE_data_source_org_id_name - v2','CREATE UNIQUE INDEX `UQE_data_source_org_id_name` ON `data_source` (`org_id`,`name`);',1,'','2023-09-28 20:45:52'),(104,'Drop old table data_source_v1 #2','DROP TABLE IF EXISTS `data_source_v1`',1,'','2023-09-28 20:45:52'),(105,'Add column with_credentials','alter table `data_source` ADD COLUMN `with_credentials` TINYINT(1) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:52'),(106,'Add secure json data column','alter table `data_source` ADD COLUMN `secure_json_data` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:52'),(107,'Update data_source table charset','ALTER TABLE `data_source` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, MODIFY `type` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `name` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `access` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `url` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `password` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `user` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `database` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `basic_auth_user` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `basic_auth_password` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `json_data` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `secure_json_data` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ;',1,'','2023-09-28 20:45:52'),(108,'Update initial version to 1','UPDATE data_source SET version = 1 WHERE version = 0',1,'','2023-09-28 20:45:52'),(109,'Add read_only data column','alter table `data_source` ADD COLUMN `read_only` TINYINT(1) NULL ',1,'','2023-09-28 20:45:52'),(110,'Migrate logging ds to loki ds','UPDATE data_source SET type = \'loki\' WHERE type = \'logging\'',1,'','2023-09-28 20:45:52'),(111,'Update json_data with nulls','UPDATE data_source SET json_data = \'{}\' WHERE json_data is null',1,'','2023-09-28 20:45:52'),(112,'Add uid column','alter table `data_source` ADD COLUMN `uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:52'),(113,'Update uid value','UPDATE data_source SET uid=lpad(id,9,\'0\');',1,'','2023-09-28 20:45:52'),(114,'Add unique index datasource_org_id_uid','CREATE UNIQUE INDEX `UQE_data_source_org_id_uid` ON `data_source` (`org_id`,`uid`);',1,'','2023-09-28 20:45:52'),(115,'add unique index datasource_org_id_is_default','CREATE INDEX `IDX_data_source_org_id_is_default` ON `data_source` (`org_id`,`is_default`);',1,'','2023-09-28 20:45:52'),(116,'create api_key table','CREATE TABLE IF NOT EXISTS `api_key` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `account_id` BIGINT(20) NOT NULL\n, `name` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `key` VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `role` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:52'),(117,'add index api_key.account_id','CREATE INDEX `IDX_api_key_account_id` ON `api_key` (`account_id`);',1,'','2023-09-28 20:45:52'),(118,'add index api_key.key','CREATE UNIQUE INDEX `UQE_api_key_key` ON `api_key` (`key`);',1,'','2023-09-28 20:45:52'),(119,'add index api_key.account_id_name','CREATE UNIQUE INDEX `UQE_api_key_account_id_name` ON `api_key` (`account_id`,`name`);',1,'','2023-09-28 20:45:52'),(120,'drop index IDX_api_key_account_id - v1','DROP INDEX `IDX_api_key_account_id` ON `api_key`',1,'','2023-09-28 20:45:52'),(121,'drop index UQE_api_key_key - v1','DROP INDEX `UQE_api_key_key` ON `api_key`',1,'','2023-09-28 20:45:52'),(122,'drop index UQE_api_key_account_id_name - v1','DROP INDEX `UQE_api_key_account_id_name` ON `api_key`',1,'','2023-09-28 20:45:52'),(123,'Rename table api_key to api_key_v1 - v1','ALTER TABLE `api_key` RENAME TO `api_key_v1`',1,'','2023-09-28 20:45:52'),(124,'create api_key table v2','CREATE TABLE IF NOT EXISTS `api_key` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `name` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `key` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `role` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:52'),(125,'create index IDX_api_key_org_id - v2','CREATE INDEX `IDX_api_key_org_id` ON `api_key` (`org_id`);',1,'','2023-09-28 20:45:53'),(126,'create index UQE_api_key_key - v2','CREATE UNIQUE INDEX `UQE_api_key_key` ON `api_key` (`key`);',1,'','2023-09-28 20:45:53'),(127,'create index UQE_api_key_org_id_name - v2','CREATE UNIQUE INDEX `UQE_api_key_org_id_name` ON `api_key` (`org_id`,`name`);',1,'','2023-09-28 20:45:53'),(128,'copy api_key v1 to v2','INSERT INTO `api_key` (`updated`\n, `id`\n, `org_id`\n, `name`\n, `key`\n, `role`\n, `created`) SELECT `updated`\n, `id`\n, `account_id`\n, `name`\n, `key`\n, `role`\n, `created` FROM `api_key_v1`',1,'','2023-09-28 20:45:53'),(129,'Drop old table api_key_v1','DROP TABLE IF EXISTS `api_key_v1`',1,'','2023-09-28 20:45:53'),(130,'Update api_key table charset','ALTER TABLE `api_key` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, MODIFY `name` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `key` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `role` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL ;',1,'','2023-09-28 20:45:53'),(131,'Add expires to api_key table','alter table `api_key` ADD COLUMN `expires` BIGINT(20) NULL ',1,'','2023-09-28 20:45:53'),(132,'Add service account foreign key','alter table `api_key` ADD COLUMN `service_account_id` BIGINT(20) NULL ',1,'','2023-09-28 20:45:53'),(133,'set service account foreign key to nil if 0','UPDATE api_key SET service_account_id = NULL WHERE service_account_id = 0;',1,'','2023-09-28 20:45:53'),(134,'Add last_used_at to api_key table','alter table `api_key` ADD COLUMN `last_used_at` DATETIME NULL ',1,'','2023-09-28 20:45:53'),(135,'Add is_revoked column to api_key table','alter table `api_key` ADD COLUMN `is_revoked` TINYINT(1) NULL DEFAULT 0 ',1,'','2023-09-28 20:45:53'),(136,'create dashboard_snapshot table v4','CREATE TABLE IF NOT EXISTS `dashboard_snapshot` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `key` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `dashboard` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `expires` DATETIME NOT NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:53'),(137,'drop table dashboard_snapshot_v4 #1','DROP TABLE IF EXISTS `dashboard_snapshot`',1,'','2023-09-28 20:45:53'),(138,'create dashboard_snapshot table v5 #2','CREATE TABLE IF NOT EXISTS `dashboard_snapshot` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `key` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `delete_key` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `user_id` BIGINT(20) NOT NULL\n, `external` TINYINT(1) NOT NULL\n, `external_url` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `dashboard` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `expires` DATETIME NOT NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:53'),(139,'create index UQE_dashboard_snapshot_key - v5','CREATE UNIQUE INDEX `UQE_dashboard_snapshot_key` ON `dashboard_snapshot` (`key`);',1,'','2023-09-28 20:45:53'),(140,'create index UQE_dashboard_snapshot_delete_key - v5','CREATE UNIQUE INDEX `UQE_dashboard_snapshot_delete_key` ON `dashboard_snapshot` (`delete_key`);',1,'','2023-09-28 20:45:53'),(141,'create index IDX_dashboard_snapshot_user_id - v5','CREATE INDEX `IDX_dashboard_snapshot_user_id` ON `dashboard_snapshot` (`user_id`);',1,'','2023-09-28 20:45:53'),(142,'alter dashboard_snapshot to mediumtext v2','ALTER TABLE dashboard_snapshot MODIFY dashboard MEDIUMTEXT;',1,'','2023-09-28 20:45:53'),(143,'Update dashboard_snapshot table charset','ALTER TABLE `dashboard_snapshot` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, MODIFY `name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `key` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `delete_key` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `external_url` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `dashboard` MEDIUMTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL ;',1,'','2023-09-28 20:45:53'),(144,'Add column external_delete_url to dashboard_snapshots table','alter table `dashboard_snapshot` ADD COLUMN `external_delete_url` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:53'),(145,'Add encrypted dashboard json column','alter table `dashboard_snapshot` ADD COLUMN `dashboard_encrypted` BLOB NULL ',1,'','2023-09-28 20:45:53'),(146,'Change dashboard_encrypted column to MEDIUMBLOB','ALTER TABLE dashboard_snapshot MODIFY dashboard_encrypted MEDIUMBLOB;',1,'','2023-09-28 20:45:53'),(147,'create quota table v1','CREATE TABLE IF NOT EXISTS `quota` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NULL\n, `user_id` BIGINT(20) NULL\n, `target` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `limit` BIGINT(20) NOT NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:53'),(148,'create index UQE_quota_org_id_user_id_target - v1','CREATE UNIQUE INDEX `UQE_quota_org_id_user_id_target` ON `quota` (`org_id`,`user_id`,`target`);',1,'','2023-09-28 20:45:53'),(149,'Update quota table charset','ALTER TABLE `quota` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, MODIFY `target` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL ;',1,'','2023-09-28 20:45:53'),(150,'create plugin_setting table','CREATE TABLE IF NOT EXISTS `plugin_setting` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NULL\n, `plugin_id` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `enabled` TINYINT(1) NOT NULL\n, `pinned` TINYINT(1) NOT NULL\n, `json_data` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `secure_json_data` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:53'),(151,'create index UQE_plugin_setting_org_id_plugin_id - v1','CREATE UNIQUE INDEX `UQE_plugin_setting_org_id_plugin_id` ON `plugin_setting` (`org_id`,`plugin_id`);',1,'','2023-09-28 20:45:53'),(152,'Add column plugin_version to plugin_settings','alter table `plugin_setting` ADD COLUMN `plugin_version` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:53'),(153,'Update plugin_setting table charset','ALTER TABLE `plugin_setting` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, MODIFY `plugin_id` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `json_data` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `secure_json_data` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `plugin_version` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ;',1,'','2023-09-28 20:45:53'),(154,'create session table','CREATE TABLE IF NOT EXISTS `session` (\n`key` CHAR(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci PRIMARY KEY NOT NULL\n, `data` BLOB NOT NULL\n, `expiry` INTEGER(255) NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:53'),(155,'Drop old table playlist table','DROP TABLE IF EXISTS `playlist`',1,'','2023-09-28 20:45:53'),(156,'Drop old table playlist_item table','DROP TABLE IF EXISTS `playlist_item`',1,'','2023-09-28 20:45:53'),(157,'create playlist table v2','CREATE TABLE IF NOT EXISTS `playlist` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `interval` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:53'),(158,'create playlist item table v2','CREATE TABLE IF NOT EXISTS `playlist_item` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `playlist_id` BIGINT(20) NOT NULL\n, `type` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `value` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `title` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `order` INT NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:53'),(159,'Update playlist table charset','ALTER TABLE `playlist` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, MODIFY `name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `interval` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL ;',1,'','2023-09-28 20:45:53'),(160,'Update playlist_item table charset','ALTER TABLE `playlist_item` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, MODIFY `type` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `value` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `title` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL ;',1,'','2023-09-28 20:45:53'),(161,'drop preferences table v2','DROP TABLE IF EXISTS `preferences`',1,'','2023-09-28 20:45:53'),(162,'drop preferences table v3','DROP TABLE IF EXISTS `preferences`',1,'','2023-09-28 20:45:53'),(163,'create preferences table v3','CREATE TABLE IF NOT EXISTS `preferences` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `user_id` BIGINT(20) NOT NULL\n, `version` INT NOT NULL\n, `home_dashboard_id` BIGINT(20) NOT NULL\n, `timezone` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `theme` VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:53'),(164,'Update preferences table charset','ALTER TABLE `preferences` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, MODIFY `timezone` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `theme` VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL ;',1,'','2023-09-28 20:45:53'),(165,'Add column team_id in preferences','alter table `preferences` ADD COLUMN `team_id` BIGINT(20) NULL ',1,'','2023-09-28 20:45:53'),(166,'Update team_id column values in preferences','UPDATE preferences SET team_id=0 WHERE team_id IS NULL;',1,'','2023-09-28 20:45:53'),(167,'Add column week_start in preferences','alter table `preferences` ADD COLUMN `week_start` VARCHAR(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:53'),(168,'Add column preferences.json_data','alter table `preferences` ADD COLUMN `json_data` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:53'),(169,'alter preferences.json_data to mediumtext v1','ALTER TABLE preferences MODIFY json_data MEDIUMTEXT;',1,'','2023-09-28 20:45:53'),(170,'Add preferences index org_id','CREATE INDEX `IDX_preferences_org_id` ON `preferences` (`org_id`);',1,'','2023-09-28 20:45:53'),(171,'Add preferences index user_id','CREATE INDEX `IDX_preferences_user_id` ON `preferences` (`user_id`);',1,'','2023-09-28 20:45:53'),(172,'create alert table v1','CREATE TABLE IF NOT EXISTS `alert` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `version` BIGINT(20) NOT NULL\n, `dashboard_id` BIGINT(20) NOT NULL\n, `panel_id` BIGINT(20) NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `message` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `state` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `settings` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `frequency` BIGINT(20) NOT NULL\n, `handler` BIGINT(20) NOT NULL\n, `severity` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `silenced` TINYINT(1) NOT NULL\n, `execution_error` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `eval_data` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `eval_date` DATETIME NULL\n, `new_state_date` DATETIME NOT NULL\n, `state_changes` INT NOT NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:53'),(173,'add index alert org_id & id ','CREATE INDEX `IDX_alert_org_id_id` ON `alert` (`org_id`,`id`);',1,'','2023-09-28 20:45:53'),(174,'add index alert state','CREATE INDEX `IDX_alert_state` ON `alert` (`state`);',1,'','2023-09-28 20:45:53'),(175,'add index alert dashboard_id','CREATE INDEX `IDX_alert_dashboard_id` ON `alert` (`dashboard_id`);',1,'','2023-09-28 20:45:53'),(176,'Create alert_rule_tag table v1','CREATE TABLE IF NOT EXISTS `alert_rule_tag` (\n`alert_id` BIGINT(20) NOT NULL\n, `tag_id` BIGINT(20) NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:53'),(177,'Add unique index alert_rule_tag.alert_id_tag_id','CREATE UNIQUE INDEX `UQE_alert_rule_tag_alert_id_tag_id` ON `alert_rule_tag` (`alert_id`,`tag_id`);',1,'','2023-09-28 20:45:53'),(178,'drop index UQE_alert_rule_tag_alert_id_tag_id - v1','DROP INDEX `UQE_alert_rule_tag_alert_id_tag_id` ON `alert_rule_tag`',1,'','2023-09-28 20:45:53'),(179,'Rename table alert_rule_tag to alert_rule_tag_v1 - v1','ALTER TABLE `alert_rule_tag` RENAME TO `alert_rule_tag_v1`',1,'','2023-09-28 20:45:53'),(180,'Create alert_rule_tag table v2','CREATE TABLE IF NOT EXISTS `alert_rule_tag` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `alert_id` BIGINT(20) NOT NULL\n, `tag_id` BIGINT(20) NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:53'),(181,'create index UQE_alert_rule_tag_alert_id_tag_id - Add unique index alert_rule_tag.alert_id_tag_id V2','CREATE UNIQUE INDEX `UQE_alert_rule_tag_alert_id_tag_id` ON `alert_rule_tag` (`alert_id`,`tag_id`);',1,'','2023-09-28 20:45:53'),(182,'copy alert_rule_tag v1 to v2','INSERT INTO `alert_rule_tag` (`tag_id`\n, `alert_id`) SELECT `tag_id`\n, `alert_id` FROM `alert_rule_tag_v1`',1,'','2023-09-28 20:45:53'),(183,'drop table alert_rule_tag_v1','DROP TABLE IF EXISTS `alert_rule_tag_v1`',1,'','2023-09-28 20:45:53'),(184,'create alert_notification table v1','CREATE TABLE IF NOT EXISTS `alert_notification` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `name` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `type` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `settings` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:53'),(185,'Add column is_default','alter table `alert_notification` ADD COLUMN `is_default` TINYINT(1) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:53'),(186,'Add column frequency','alter table `alert_notification` ADD COLUMN `frequency` BIGINT(20) NULL ',1,'','2023-09-28 20:45:53'),(187,'Add column send_reminder','alter table `alert_notification` ADD COLUMN `send_reminder` TINYINT(1) NULL DEFAULT 0 ',1,'','2023-09-28 20:45:53'),(188,'Add column disable_resolve_message','alter table `alert_notification` ADD COLUMN `disable_resolve_message` TINYINT(1) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:53'),(189,'add index alert_notification org_id & name','CREATE UNIQUE INDEX `UQE_alert_notification_org_id_name` ON `alert_notification` (`org_id`,`name`);',1,'','2023-09-28 20:45:53'),(190,'Update alert table charset','ALTER TABLE `alert` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, MODIFY `name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `message` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `state` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `settings` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `severity` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `execution_error` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `eval_data` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ;',1,'','2023-09-28 20:45:53'),(191,'Update alert_notification table charset','ALTER TABLE `alert_notification` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, MODIFY `name` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `type` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `settings` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL ;',1,'','2023-09-28 20:45:53'),(192,'create notification_journal table v1','CREATE TABLE IF NOT EXISTS `alert_notification_journal` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `alert_id` BIGINT(20) NOT NULL\n, `notifier_id` BIGINT(20) NOT NULL\n, `sent_at` BIGINT(20) NOT NULL\n, `success` TINYINT(1) NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:53'),(193,'add index notification_journal org_id & alert_id & notifier_id','CREATE INDEX `IDX_alert_notification_journal_org_id_alert_id_notifier_id` ON `alert_notification_journal` (`org_id`,`alert_id`,`notifier_id`);',1,'','2023-09-28 20:45:53'),(194,'drop alert_notification_journal','DROP TABLE IF EXISTS `alert_notification_journal`',1,'','2023-09-28 20:45:53'),(195,'create alert_notification_state table v1','CREATE TABLE IF NOT EXISTS `alert_notification_state` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `alert_id` BIGINT(20) NOT NULL\n, `notifier_id` BIGINT(20) NOT NULL\n, `state` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `version` BIGINT(20) NOT NULL\n, `updated_at` BIGINT(20) NOT NULL\n, `alert_rule_state_updated_version` BIGINT(20) NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:53'),(196,'add index alert_notification_state org_id & alert_id & notifier_id','CREATE UNIQUE INDEX `UQE_alert_notification_state_org_id_alert_id_notifier_id` ON `alert_notification_state` (`org_id`,`alert_id`,`notifier_id`);',1,'','2023-09-28 20:45:53'),(197,'Add for to alert table','alter table `alert` ADD COLUMN `for` BIGINT(20) NULL ',1,'','2023-09-28 20:45:53'),(198,'Add column uid in alert_notification','alter table `alert_notification` ADD COLUMN `uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:53'),(199,'Update uid column values in alert_notification','UPDATE alert_notification SET uid=lpad(id,9,\'0\') WHERE uid IS NULL;',1,'','2023-09-28 20:45:53'),(200,'Add unique index alert_notification_org_id_uid','CREATE UNIQUE INDEX `UQE_alert_notification_org_id_uid` ON `alert_notification` (`org_id`,`uid`);',1,'','2023-09-28 20:45:53'),(201,'Remove unique index org_id_name','DROP INDEX `UQE_alert_notification_org_id_name` ON `alert_notification`',1,'','2023-09-28 20:45:53'),(202,'Add column secure_settings in alert_notification','alter table `alert_notification` ADD COLUMN `secure_settings` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:53'),(203,'alter alert.settings to mediumtext','ALTER TABLE alert MODIFY settings MEDIUMTEXT;',1,'','2023-09-28 20:45:53'),(204,'Add non-unique index alert_notification_state_alert_id','CREATE INDEX `IDX_alert_notification_state_alert_id` ON `alert_notification_state` (`alert_id`);',1,'','2023-09-28 20:45:53'),(205,'Add non-unique index alert_rule_tag_alert_id','CREATE INDEX `IDX_alert_rule_tag_alert_id` ON `alert_rule_tag` (`alert_id`);',1,'','2023-09-28 20:45:53'),(206,'Drop old annotation table v4','DROP TABLE IF EXISTS `annotation`',1,'','2023-09-28 20:45:53'),(207,'create annotation table v5','CREATE TABLE IF NOT EXISTS `annotation` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `alert_id` BIGINT(20) NULL\n, `user_id` BIGINT(20) NULL\n, `dashboard_id` BIGINT(20) NULL\n, `panel_id` BIGINT(20) NULL\n, `category_id` BIGINT(20) NULL\n, `type` VARCHAR(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `title` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `text` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `metric` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `prev_state` VARCHAR(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `new_state` VARCHAR(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `data` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `epoch` BIGINT(20) NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:53'),(208,'add index annotation 0 v3','CREATE INDEX `IDX_annotation_org_id_alert_id` ON `annotation` (`org_id`,`alert_id`);',1,'','2023-09-28 20:45:53'),(209,'add index annotation 1 v3','CREATE INDEX `IDX_annotation_org_id_type` ON `annotation` (`org_id`,`type`);',1,'','2023-09-28 20:45:53'),(210,'add index annotation 2 v3','CREATE INDEX `IDX_annotation_org_id_category_id` ON `annotation` (`org_id`,`category_id`);',1,'','2023-09-28 20:45:53'),(211,'add index annotation 3 v3','CREATE INDEX `IDX_annotation_org_id_dashboard_id_panel_id_epoch` ON `annotation` (`org_id`,`dashboard_id`,`panel_id`,`epoch`);',1,'','2023-09-28 20:45:53'),(212,'add index annotation 4 v3','CREATE INDEX `IDX_annotation_org_id_epoch` ON `annotation` (`org_id`,`epoch`);',1,'','2023-09-28 20:45:53'),(213,'Update annotation table charset','ALTER TABLE `annotation` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, MODIFY `type` VARCHAR(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `title` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `text` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `metric` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL , MODIFY `prev_state` VARCHAR(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `new_state` VARCHAR(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , MODIFY `data` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL ;',1,'','2023-09-28 20:45:53'),(214,'Add column region_id to annotation table','alter table `annotation` ADD COLUMN `region_id` BIGINT(20) NULL DEFAULT 0 ',1,'','2023-09-28 20:45:53'),(215,'Drop category_id index','DROP INDEX `IDX_annotation_org_id_category_id` ON `annotation`',1,'','2023-09-28 20:45:53'),(216,'Add column tags to annotation table','alter table `annotation` ADD COLUMN `tags` VARCHAR(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:53'),(217,'Create annotation_tag table v2','CREATE TABLE IF NOT EXISTS `annotation_tag` (\n`annotation_id` BIGINT(20) NOT NULL\n, `tag_id` BIGINT(20) NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:53'),(218,'Add unique index annotation_tag.annotation_id_tag_id','CREATE UNIQUE INDEX `UQE_annotation_tag_annotation_id_tag_id` ON `annotation_tag` (`annotation_id`,`tag_id`);',1,'','2023-09-28 20:45:53'),(219,'drop index UQE_annotation_tag_annotation_id_tag_id - v2','DROP INDEX `UQE_annotation_tag_annotation_id_tag_id` ON `annotation_tag`',1,'','2023-09-28 20:45:53'),(220,'Rename table annotation_tag to annotation_tag_v2 - v2','ALTER TABLE `annotation_tag` RENAME TO `annotation_tag_v2`',1,'','2023-09-28 20:45:53'),(221,'Create annotation_tag table v3','CREATE TABLE IF NOT EXISTS `annotation_tag` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `annotation_id` BIGINT(20) NOT NULL\n, `tag_id` BIGINT(20) NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:53'),(222,'create index UQE_annotation_tag_annotation_id_tag_id - Add unique index annotation_tag.annotation_id_tag_id V3','CREATE UNIQUE INDEX `UQE_annotation_tag_annotation_id_tag_id` ON `annotation_tag` (`annotation_id`,`tag_id`);',1,'','2023-09-28 20:45:53'),(223,'copy annotation_tag v2 to v3','INSERT INTO `annotation_tag` (`annotation_id`\n, `tag_id`) SELECT `annotation_id`\n, `tag_id` FROM `annotation_tag_v2`',1,'','2023-09-28 20:45:53'),(224,'drop table annotation_tag_v2','DROP TABLE IF EXISTS `annotation_tag_v2`',1,'','2023-09-28 20:45:53'),(225,'Update alert annotations and set TEXT to empty','UPDATE annotation SET TEXT = \'\' WHERE alert_id > 0',1,'','2023-09-28 20:45:53'),(226,'Add created time to annotation table','alter table `annotation` ADD COLUMN `created` BIGINT(20) NULL DEFAULT 0 ',1,'','2023-09-28 20:45:53'),(227,'Add updated time to annotation table','alter table `annotation` ADD COLUMN `updated` BIGINT(20) NULL DEFAULT 0 ',1,'','2023-09-28 20:45:53'),(228,'Add index for created in annotation table','CREATE INDEX `IDX_annotation_org_id_created` ON `annotation` (`org_id`,`created`);',1,'','2023-09-28 20:45:53'),(229,'Add index for updated in annotation table','CREATE INDEX `IDX_annotation_org_id_updated` ON `annotation` (`org_id`,`updated`);',1,'','2023-09-28 20:45:53'),(230,'Convert existing annotations from seconds to milliseconds','UPDATE annotation SET epoch = (epoch*1000) where epoch < 9999999999',1,'','2023-09-28 20:45:53'),(231,'Add epoch_end column','alter table `annotation` ADD COLUMN `epoch_end` BIGINT(20) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:53'),(232,'Add index for epoch_end','CREATE INDEX `IDX_annotation_org_id_epoch_epoch_end` ON `annotation` (`org_id`,`epoch`,`epoch_end`);',1,'','2023-09-28 20:45:53'),(233,'Make epoch_end the same as epoch','UPDATE annotation SET epoch_end = epoch',1,'','2023-09-28 20:45:53'),(234,'Move region to single row','code migration',1,'','2023-09-28 20:45:53'),(235,'Remove index org_id_epoch from annotation table','DROP INDEX `IDX_annotation_org_id_epoch` ON `annotation`',1,'','2023-09-28 20:45:53'),(236,'Remove index org_id_dashboard_id_panel_id_epoch from annotation table','DROP INDEX `IDX_annotation_org_id_dashboard_id_panel_id_epoch` ON `annotation`',1,'','2023-09-28 20:45:53'),(237,'Add index for org_id_dashboard_id_epoch_end_epoch on annotation table','CREATE INDEX `IDX_annotation_org_id_dashboard_id_epoch_end_epoch` ON `annotation` (`org_id`,`dashboard_id`,`epoch_end`,`epoch`);',1,'','2023-09-28 20:45:53'),(238,'Add index for org_id_epoch_end_epoch on annotation table','CREATE INDEX `IDX_annotation_org_id_epoch_end_epoch` ON `annotation` (`org_id`,`epoch_end`,`epoch`);',1,'','2023-09-28 20:45:53'),(239,'Remove index org_id_epoch_epoch_end from annotation table','DROP INDEX `IDX_annotation_org_id_epoch_epoch_end` ON `annotation`',1,'','2023-09-28 20:45:53'),(240,'Add index for alert_id on annotation table','CREATE INDEX `IDX_annotation_alert_id` ON `annotation` (`alert_id`);',1,'','2023-09-28 20:45:53'),(241,'Increase tags column to length 4096','ALTER TABLE annotation MODIFY tags VARCHAR(4096);',1,'','2023-09-28 20:45:53'),(242,'create test_data table','CREATE TABLE IF NOT EXISTS `test_data` (\n`id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `metric1` VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `metric2` VARCHAR(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `value_big_int` BIGINT(20) NULL\n, `value_double` DOUBLE NULL\n, `value_float` FLOAT NULL\n, `value_int` INT NULL\n, `time_epoch` BIGINT(20) NOT NULL\n, `time_date_time` DATETIME NOT NULL\n, `time_time_stamp` TIMESTAMP NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:53'),(243,'create dashboard_version table v1','CREATE TABLE IF NOT EXISTS `dashboard_version` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `dashboard_id` BIGINT(20) NOT NULL\n, `parent_version` INT NOT NULL\n, `restored_from` INT NOT NULL\n, `version` INT NOT NULL\n, `created` DATETIME NOT NULL\n, `created_by` BIGINT(20) NOT NULL\n, `message` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `data` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:53'),(244,'add index dashboard_version.dashboard_id','CREATE INDEX `IDX_dashboard_version_dashboard_id` ON `dashboard_version` (`dashboard_id`);',1,'','2023-09-28 20:45:53'),(245,'add unique index dashboard_version.dashboard_id and dashboard_version.version','CREATE UNIQUE INDEX `UQE_dashboard_version_dashboard_id_version` ON `dashboard_version` (`dashboard_id`,`version`);',1,'','2023-09-28 20:45:53'),(246,'Set dashboard version to 1 where 0','UPDATE dashboard SET version = 1 WHERE version = 0',1,'','2023-09-28 20:45:53'),(247,'save existing dashboard data in dashboard_version table v1','INSERT INTO dashboard_version\n(\n  dashboard_id,\n  version,\n  parent_version,\n  restored_from,\n  created,\n  created_by,\n  message,\n  data\n)\nSELECT\n  dashboard.id,\n  dashboard.version,\n  dashboard.version,\n  dashboard.version,\n  dashboard.updated,\n  COALESCE(dashboard.updated_by, -1),\n  \'\',\n  dashboard.data\nFROM dashboard;',1,'','2023-09-28 20:45:53'),(248,'alter dashboard_version.data to mediumtext v1','ALTER TABLE dashboard_version MODIFY data MEDIUMTEXT;',1,'','2023-09-28 20:45:53'),(249,'create team table','CREATE TABLE IF NOT EXISTS `team` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `name` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:53'),(250,'add index team.org_id','CREATE INDEX `IDX_team_org_id` ON `team` (`org_id`);',1,'','2023-09-28 20:45:53'),(251,'add unique index team_org_id_name','CREATE UNIQUE INDEX `UQE_team_org_id_name` ON `team` (`org_id`,`name`);',1,'','2023-09-28 20:45:53'),(252,'Add column uid in team','alter table `team` ADD COLUMN `uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:53'),(253,'Update uid column values in team','UPDATE team SET uid=concat(\'t\',lpad(id,9,\'0\')) WHERE uid IS NULL;',1,'','2023-09-28 20:45:53'),(254,'Add unique index team_org_id_uid','CREATE UNIQUE INDEX `UQE_team_org_id_uid` ON `team` (`org_id`,`uid`);',1,'','2023-09-28 20:45:54'),(255,'create team member table','CREATE TABLE IF NOT EXISTS `team_member` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `team_id` BIGINT(20) NOT NULL\n, `user_id` BIGINT(20) NOT NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:54'),(256,'add index team_member.org_id','CREATE INDEX `IDX_team_member_org_id` ON `team_member` (`org_id`);',1,'','2023-09-28 20:45:54'),(257,'add unique index team_member_org_id_team_id_user_id','CREATE UNIQUE INDEX `UQE_team_member_org_id_team_id_user_id` ON `team_member` (`org_id`,`team_id`,`user_id`);',1,'','2023-09-28 20:45:54'),(258,'add index team_member.team_id','CREATE INDEX `IDX_team_member_team_id` ON `team_member` (`team_id`);',1,'','2023-09-28 20:45:54'),(259,'Add column email to team table','alter table `team` ADD COLUMN `email` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:54'),(260,'Add column external to team_member table','alter table `team_member` ADD COLUMN `external` TINYINT(1) NULL ',1,'','2023-09-28 20:45:54'),(261,'Add column permission to team_member table','alter table `team_member` ADD COLUMN `permission` SMALLINT NULL ',1,'','2023-09-28 20:45:54'),(262,'create dashboard acl table','CREATE TABLE IF NOT EXISTS `dashboard_acl` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `dashboard_id` BIGINT(20) NOT NULL\n, `user_id` BIGINT(20) NULL\n, `team_id` BIGINT(20) NULL\n, `permission` SMALLINT NOT NULL DEFAULT 4\n, `role` VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:54'),(263,'add index dashboard_acl_dashboard_id','CREATE INDEX `IDX_dashboard_acl_dashboard_id` ON `dashboard_acl` (`dashboard_id`);',1,'','2023-09-28 20:45:54'),(264,'add unique index dashboard_acl_dashboard_id_user_id','CREATE UNIQUE INDEX `UQE_dashboard_acl_dashboard_id_user_id` ON `dashboard_acl` (`dashboard_id`,`user_id`);',1,'','2023-09-28 20:45:54'),(265,'add unique index dashboard_acl_dashboard_id_team_id','CREATE UNIQUE INDEX `UQE_dashboard_acl_dashboard_id_team_id` ON `dashboard_acl` (`dashboard_id`,`team_id`);',1,'','2023-09-28 20:45:54'),(266,'add index dashboard_acl_user_id','CREATE INDEX `IDX_dashboard_acl_user_id` ON `dashboard_acl` (`user_id`);',1,'','2023-09-28 20:45:54'),(267,'add index dashboard_acl_team_id','CREATE INDEX `IDX_dashboard_acl_team_id` ON `dashboard_acl` (`team_id`);',1,'','2023-09-28 20:45:54'),(268,'add index dashboard_acl_org_id_role','CREATE INDEX `IDX_dashboard_acl_org_id_role` ON `dashboard_acl` (`org_id`,`role`);',1,'','2023-09-28 20:45:54'),(269,'add index dashboard_permission','CREATE INDEX `IDX_dashboard_acl_permission` ON `dashboard_acl` (`permission`);',1,'','2023-09-28 20:45:54'),(270,'save default acl rules in dashboard_acl table','\nINSERT INTO dashboard_acl\n  (\n    org_id,\n    dashboard_id,\n    permission,\n    role,\n    created,\n    updated\n  )\n  VALUES\n    (-1,-1, 1,\'Viewer\',\'2017-06-20\',\'2017-06-20\'),\n    (-1,-1, 2,\'Editor\',\'2017-06-20\',\'2017-06-20\')\n  ',1,'','2023-09-28 20:45:54'),(271,'delete acl rules for deleted dashboards and folders','DELETE FROM dashboard_acl WHERE dashboard_id NOT IN (SELECT id FROM dashboard) AND dashboard_id != -1',1,'','2023-09-28 20:45:54'),(272,'create tag table','CREATE TABLE IF NOT EXISTS `tag` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `key` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `value` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:54'),(273,'add index tag.key_value','CREATE UNIQUE INDEX `UQE_tag_key_value` ON `tag` (`key`,`value`);',1,'','2023-09-28 20:45:54'),(274,'create login attempt table','CREATE TABLE IF NOT EXISTS `login_attempt` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `username` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `ip_address` VARCHAR(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `created` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:54'),(275,'add index login_attempt.username','CREATE INDEX `IDX_login_attempt_username` ON `login_attempt` (`username`);',1,'','2023-09-28 20:45:54'),(276,'drop index IDX_login_attempt_username - v1','DROP INDEX `IDX_login_attempt_username` ON `login_attempt`',1,'','2023-09-28 20:45:54'),(277,'Rename table login_attempt to login_attempt_tmp_qwerty - v1','ALTER TABLE `login_attempt` RENAME TO `login_attempt_tmp_qwerty`',1,'','2023-09-28 20:45:54'),(278,'create login_attempt v2','CREATE TABLE IF NOT EXISTS `login_attempt` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `username` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `ip_address` VARCHAR(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `created` INT NOT NULL DEFAULT 0\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:54'),(279,'create index IDX_login_attempt_username - v2','CREATE INDEX `IDX_login_attempt_username` ON `login_attempt` (`username`);',1,'','2023-09-28 20:45:54'),(280,'copy login_attempt v1 to v2','INSERT INTO `login_attempt` (`id`\n, `username`\n, `ip_address`) SELECT `id`\n, `username`\n, `ip_address` FROM `login_attempt_tmp_qwerty`',1,'','2023-09-28 20:45:54'),(281,'drop login_attempt_tmp_qwerty','DROP TABLE IF EXISTS `login_attempt_tmp_qwerty`',1,'','2023-09-28 20:45:54'),(282,'create user auth table','CREATE TABLE IF NOT EXISTS `user_auth` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `user_id` BIGINT(20) NOT NULL\n, `auth_module` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `auth_id` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `created` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:54'),(283,'create index IDX_user_auth_auth_module_auth_id - v1','CREATE INDEX `IDX_user_auth_auth_module_auth_id` ON `user_auth` (`auth_module`,`auth_id`);',1,'','2023-09-28 20:45:54'),(284,'alter user_auth.auth_id to length 190','ALTER TABLE user_auth MODIFY auth_id VARCHAR(190);',1,'','2023-09-28 20:45:54'),(285,'Add OAuth access token to user_auth','alter table `user_auth` ADD COLUMN `o_auth_access_token` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:54'),(286,'Add OAuth refresh token to user_auth','alter table `user_auth` ADD COLUMN `o_auth_refresh_token` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:54'),(287,'Add OAuth token type to user_auth','alter table `user_auth` ADD COLUMN `o_auth_token_type` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:54'),(288,'Add OAuth expiry to user_auth','alter table `user_auth` ADD COLUMN `o_auth_expiry` DATETIME NULL ',1,'','2023-09-28 20:45:54'),(289,'Add index to user_id column in user_auth','CREATE INDEX `IDX_user_auth_user_id` ON `user_auth` (`user_id`);',1,'','2023-09-28 20:45:54'),(290,'Add OAuth ID token to user_auth','alter table `user_auth` ADD COLUMN `o_auth_id_token` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:54'),(291,'create server_lock table','CREATE TABLE IF NOT EXISTS `server_lock` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `operation_uid` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `version` BIGINT(20) NOT NULL\n, `last_execution` BIGINT(20) NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:54'),(292,'add index server_lock.operation_uid','CREATE UNIQUE INDEX `UQE_server_lock_operation_uid` ON `server_lock` (`operation_uid`);',1,'','2023-09-28 20:45:54'),(293,'create user auth token table','CREATE TABLE IF NOT EXISTS `user_auth_token` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `user_id` BIGINT(20) NOT NULL\n, `auth_token` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `prev_auth_token` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `user_agent` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `client_ip` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `auth_token_seen` TINYINT(1) NOT NULL\n, `seen_at` INT NULL\n, `rotated_at` INT NOT NULL\n, `created_at` INT NOT NULL\n, `updated_at` INT NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:54'),(294,'add unique index user_auth_token.auth_token','CREATE UNIQUE INDEX `UQE_user_auth_token_auth_token` ON `user_auth_token` (`auth_token`);',1,'','2023-09-28 20:45:54'),(295,'add unique index user_auth_token.prev_auth_token','CREATE UNIQUE INDEX `UQE_user_auth_token_prev_auth_token` ON `user_auth_token` (`prev_auth_token`);',1,'','2023-09-28 20:45:54'),(296,'add index user_auth_token.user_id','CREATE INDEX `IDX_user_auth_token_user_id` ON `user_auth_token` (`user_id`);',1,'','2023-09-28 20:45:54'),(297,'Add revoked_at to the user auth token','alter table `user_auth_token` ADD COLUMN `revoked_at` INT NULL ',1,'','2023-09-28 20:45:54'),(298,'create cache_data table','CREATE TABLE IF NOT EXISTS `cache_data` (\n`cache_key` VARCHAR(168) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci PRIMARY KEY NOT NULL\n, `data` BLOB NOT NULL\n, `expires` INTEGER(255) NOT NULL\n, `created_at` INTEGER(255) NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:54'),(299,'add unique index cache_data.cache_key','CREATE UNIQUE INDEX `UQE_cache_data_cache_key` ON `cache_data` (`cache_key`);',1,'','2023-09-28 20:45:54'),(300,'create short_url table v1','CREATE TABLE IF NOT EXISTS `short_url` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `path` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `created_by` INT NOT NULL\n, `created_at` INT NOT NULL\n, `last_seen_at` INT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:54'),(301,'add index short_url.org_id-uid','CREATE UNIQUE INDEX `UQE_short_url_org_id_uid` ON `short_url` (`org_id`,`uid`);',1,'','2023-09-28 20:45:54'),(302,'alter table short_url alter column created_by type to bigint','ALTER TABLE short_url MODIFY created_by BIGINT;',1,'','2023-09-28 20:45:54'),(303,'delete alert_definition table','DROP TABLE IF EXISTS `alert_definition`',1,'','2023-09-28 20:45:54'),(304,'recreate alert_definition table','CREATE TABLE IF NOT EXISTS `alert_definition` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `title` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `condition` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `data` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `updated` DATETIME NOT NULL\n, `interval_seconds` BIGINT(20) NOT NULL DEFAULT 60\n, `version` INT NOT NULL DEFAULT 0\n, `uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 0\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:54'),(305,'add index in alert_definition on org_id and title columns','CREATE INDEX `IDX_alert_definition_org_id_title` ON `alert_definition` (`org_id`,`title`);',1,'','2023-09-28 20:45:54'),(306,'add index in alert_definition on org_id and uid columns','CREATE INDEX `IDX_alert_definition_org_id_uid` ON `alert_definition` (`org_id`,`uid`);',1,'','2023-09-28 20:45:54'),(307,'alter alert_definition table data column to mediumtext in mysql','ALTER TABLE alert_definition MODIFY data MEDIUMTEXT;',1,'','2023-09-28 20:45:54'),(308,'drop index in alert_definition on org_id and title columns','DROP INDEX `IDX_alert_definition_org_id_title` ON `alert_definition`',1,'','2023-09-28 20:45:54'),(309,'drop index in alert_definition on org_id and uid columns','DROP INDEX `IDX_alert_definition_org_id_uid` ON `alert_definition`',1,'','2023-09-28 20:45:54'),(310,'add unique index in alert_definition on org_id and title columns','CREATE UNIQUE INDEX `UQE_alert_definition_org_id_title` ON `alert_definition` (`org_id`,`title`);',1,'','2023-09-28 20:45:54'),(311,'add unique index in alert_definition on org_id and uid columns','CREATE UNIQUE INDEX `UQE_alert_definition_org_id_uid` ON `alert_definition` (`org_id`,`uid`);',1,'','2023-09-28 20:45:54'),(312,'Add column paused in alert_definition','alter table `alert_definition` ADD COLUMN `paused` TINYINT(1) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:54'),(313,'drop alert_definition table','DROP TABLE IF EXISTS `alert_definition`',1,'','2023-09-28 20:45:54'),(314,'delete alert_definition_version table','DROP TABLE IF EXISTS `alert_definition_version`',1,'','2023-09-28 20:45:54'),(315,'recreate alert_definition_version table','CREATE TABLE IF NOT EXISTS `alert_definition_version` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `alert_definition_id` BIGINT(20) NOT NULL\n, `alert_definition_uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 0\n, `parent_version` INT NOT NULL\n, `restored_from` INT NOT NULL\n, `version` INT NOT NULL\n, `created` DATETIME NOT NULL\n, `title` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `condition` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `data` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `interval_seconds` BIGINT(20) NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:54'),(316,'add index in alert_definition_version table on alert_definition_id and version columns','CREATE UNIQUE INDEX `UQE_alert_definition_version_alert_definition_id_version` ON `alert_definition_version` (`alert_definition_id`,`version`);',1,'','2023-09-28 20:45:54'),(317,'add index in alert_definition_version table on alert_definition_uid and version columns','CREATE UNIQUE INDEX `UQE_alert_definition_version_alert_definition_uid_version` ON `alert_definition_version` (`alert_definition_uid`,`version`);',1,'','2023-09-28 20:45:54'),(318,'alter alert_definition_version table data column to mediumtext in mysql','ALTER TABLE alert_definition_version MODIFY data MEDIUMTEXT;',1,'','2023-09-28 20:45:54'),(319,'drop alert_definition_version table','DROP TABLE IF EXISTS `alert_definition_version`',1,'','2023-09-28 20:45:54'),(320,'create alert_instance table','CREATE TABLE IF NOT EXISTS `alert_instance` (\n`def_org_id` BIGINT(20) NOT NULL\n, `def_uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 0\n, `labels` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `labels_hash` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `current_state` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `current_state_since` BIGINT(20) NOT NULL\n, `last_eval_time` BIGINT(20) NOT NULL\n, PRIMARY KEY ( `def_org_id`,`def_uid`,`labels_hash` )) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:54'),(321,'add index in alert_instance table on def_org_id, def_uid and current_state columns','CREATE INDEX `IDX_alert_instance_def_org_id_def_uid_current_state` ON `alert_instance` (`def_org_id`,`def_uid`,`current_state`);',1,'','2023-09-28 20:45:54'),(322,'add index in alert_instance table on def_org_id, current_state columns','CREATE INDEX `IDX_alert_instance_def_org_id_current_state` ON `alert_instance` (`def_org_id`,`current_state`);',1,'','2023-09-28 20:45:54'),(323,'add column current_state_end to alert_instance','alter table `alert_instance` ADD COLUMN `current_state_end` BIGINT(20) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:54'),(324,'remove index def_org_id, def_uid, current_state on alert_instance','DROP INDEX `IDX_alert_instance_def_org_id_def_uid_current_state` ON `alert_instance`',1,'','2023-09-28 20:45:54'),(325,'remove index def_org_id, current_state on alert_instance','DROP INDEX `IDX_alert_instance_def_org_id_current_state` ON `alert_instance`',1,'','2023-09-28 20:45:54'),(326,'rename def_org_id to rule_org_id in alert_instance','ALTER TABLE alert_instance CHANGE def_org_id rule_org_id BIGINT;',1,'','2023-09-28 20:45:54'),(327,'rename def_uid to rule_uid in alert_instance','ALTER TABLE alert_instance CHANGE def_uid rule_uid VARCHAR(40);',1,'','2023-09-28 20:45:54'),(328,'add index rule_org_id, rule_uid, current_state on alert_instance','CREATE INDEX `IDX_alert_instance_rule_org_id_rule_uid_current_state` ON `alert_instance` (`rule_org_id`,`rule_uid`,`current_state`);',1,'','2023-09-28 20:45:54'),(329,'add index rule_org_id, current_state on alert_instance','CREATE INDEX `IDX_alert_instance_rule_org_id_current_state` ON `alert_instance` (`rule_org_id`,`current_state`);',1,'','2023-09-28 20:45:54'),(330,'add current_reason column related to current_state','alter table `alert_instance` ADD COLUMN `current_reason` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:54'),(331,'create alert_rule table','CREATE TABLE IF NOT EXISTS `alert_rule` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `title` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `condition` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `data` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `updated` DATETIME NOT NULL\n, `interval_seconds` BIGINT(20) NOT NULL DEFAULT 60\n, `version` INT NOT NULL DEFAULT 0\n, `uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 0\n, `namespace_uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `rule_group` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `no_data_state` VARCHAR(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT \'NoData\'\n, `exec_err_state` VARCHAR(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT \'Alerting\'\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:54'),(332,'add index in alert_rule on org_id and title columns','CREATE UNIQUE INDEX `UQE_alert_rule_org_id_title` ON `alert_rule` (`org_id`,`title`);',1,'','2023-09-28 20:45:54'),(333,'add index in alert_rule on org_id and uid columns','CREATE UNIQUE INDEX `UQE_alert_rule_org_id_uid` ON `alert_rule` (`org_id`,`uid`);',1,'','2023-09-28 20:45:54'),(334,'add index in alert_rule on org_id, namespace_uid, group_uid columns','CREATE INDEX `IDX_alert_rule_org_id_namespace_uid_rule_group` ON `alert_rule` (`org_id`,`namespace_uid`,`rule_group`);',1,'','2023-09-28 20:45:54'),(335,'alter alert_rule table data column to mediumtext in mysql','ALTER TABLE alert_rule MODIFY data MEDIUMTEXT;',1,'','2023-09-28 20:45:54'),(336,'add column for to alert_rule','alter table `alert_rule` ADD COLUMN `for` BIGINT(20) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:54'),(337,'add column annotations to alert_rule','alter table `alert_rule` ADD COLUMN `annotations` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:54'),(338,'add column labels to alert_rule','alter table `alert_rule` ADD COLUMN `labels` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:54'),(339,'remove unique index from alert_rule on org_id, title columns','DROP INDEX `UQE_alert_rule_org_id_title` ON `alert_rule`',1,'','2023-09-28 20:45:54'),(340,'add index in alert_rule on org_id, namespase_uid and title columns','CREATE UNIQUE INDEX `UQE_alert_rule_org_id_namespace_uid_title` ON `alert_rule` (`org_id`,`namespace_uid`,`title`);',1,'','2023-09-28 20:45:54'),(341,'add dashboard_uid column to alert_rule','alter table `alert_rule` ADD COLUMN `dashboard_uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:54'),(342,'add panel_id column to alert_rule','alter table `alert_rule` ADD COLUMN `panel_id` BIGINT(20) NULL ',1,'','2023-09-28 20:45:54'),(343,'add index in alert_rule on org_id, dashboard_uid and panel_id columns','CREATE INDEX `IDX_alert_rule_org_id_dashboard_uid_panel_id` ON `alert_rule` (`org_id`,`dashboard_uid`,`panel_id`);',1,'','2023-09-28 20:45:54'),(344,'add rule_group_idx column to alert_rule','alter table `alert_rule` ADD COLUMN `rule_group_idx` INT NOT NULL DEFAULT 1 ',1,'','2023-09-28 20:45:54'),(345,'add is_paused column to alert_rule table','alter table `alert_rule` ADD COLUMN `is_paused` TINYINT(1) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:54'),(346,'fix is_paused column for alert_rule table','SELECT 0;',1,'','2023-09-28 20:45:54'),(347,'create alert_rule_version table','CREATE TABLE IF NOT EXISTS `alert_rule_version` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `rule_org_id` BIGINT(20) NOT NULL\n, `rule_uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 0\n, `rule_namespace_uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `rule_group` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `parent_version` INT NOT NULL\n, `restored_from` INT NOT NULL\n, `version` INT NOT NULL\n, `created` DATETIME NOT NULL\n, `title` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `condition` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `data` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `interval_seconds` BIGINT(20) NOT NULL\n, `no_data_state` VARCHAR(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT \'NoData\'\n, `exec_err_state` VARCHAR(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT \'Alerting\'\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:54'),(348,'add index in alert_rule_version table on rule_org_id, rule_uid and version columns','CREATE UNIQUE INDEX `UQE_alert_rule_version_rule_org_id_rule_uid_version` ON `alert_rule_version` (`rule_org_id`,`rule_uid`,`version`);',1,'','2023-09-28 20:45:54'),(349,'add index in alert_rule_version table on rule_org_id, rule_namespace_uid and rule_group columns','CREATE INDEX `IDX_alert_rule_version_rule_org_id_rule_namespace_uid_rule_group` ON `alert_rule_version` (`rule_org_id`,`rule_namespace_uid`,`rule_group`);',1,'','2023-09-28 20:45:54'),(350,'alter alert_rule_version table data column to mediumtext in mysql','ALTER TABLE alert_rule_version MODIFY data MEDIUMTEXT;',1,'','2023-09-28 20:45:54'),(351,'add column for to alert_rule_version','alter table `alert_rule_version` ADD COLUMN `for` BIGINT(20) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:54'),(352,'add column annotations to alert_rule_version','alter table `alert_rule_version` ADD COLUMN `annotations` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:54'),(353,'add column labels to alert_rule_version','alter table `alert_rule_version` ADD COLUMN `labels` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:54'),(354,'add rule_group_idx column to alert_rule_version','alter table `alert_rule_version` ADD COLUMN `rule_group_idx` INT NOT NULL DEFAULT 1 ',1,'','2023-09-28 20:45:54'),(355,'add is_paused column to alert_rule_versions table','alter table `alert_rule_version` ADD COLUMN `is_paused` TINYINT(1) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:54'),(356,'fix is_paused column for alert_rule_version table','SELECT 0;',1,'','2023-09-28 20:45:54'),(357,'create_alert_configuration_table','CREATE TABLE IF NOT EXISTS `alert_configuration` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `alertmanager_configuration` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `configuration_version` VARCHAR(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `created_at` INT NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:54'),(358,'Add column default in alert_configuration','alter table `alert_configuration` ADD COLUMN `default` TINYINT(1) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:54'),(359,'alert alert_configuration alertmanager_configuration column from TEXT to MEDIUMTEXT if mysql','ALTER TABLE alert_configuration MODIFY alertmanager_configuration MEDIUMTEXT;',1,'','2023-09-28 20:45:54'),(360,'add column org_id in alert_configuration','alter table `alert_configuration` ADD COLUMN `org_id` BIGINT(20) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:54'),(361,'add index in alert_configuration table on org_id column','CREATE INDEX `IDX_alert_configuration_org_id` ON `alert_configuration` (`org_id`);',1,'','2023-09-28 20:45:54'),(362,'add configuration_hash column to alert_configuration','alter table `alert_configuration` ADD COLUMN `configuration_hash` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT \'not-yet-calculated\' ',1,'','2023-09-28 20:45:54'),(363,'create_ngalert_configuration_table','CREATE TABLE IF NOT EXISTS `ngalert_configuration` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `alertmanagers` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `created_at` INT NOT NULL\n, `updated_at` INT NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:54'),(364,'add index in ngalert_configuration on org_id column','CREATE UNIQUE INDEX `UQE_ngalert_configuration_org_id` ON `ngalert_configuration` (`org_id`);',1,'','2023-09-28 20:45:54'),(365,'add column send_alerts_to in ngalert_configuration','alter table `ngalert_configuration` ADD COLUMN `send_alerts_to` SMALLINT NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:54'),(366,'create provenance_type table','CREATE TABLE IF NOT EXISTS `provenance_type` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `record_key` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `record_type` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `provenance` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:54'),(367,'add index to uniquify (record_key, record_type, org_id) columns','CREATE UNIQUE INDEX `UQE_provenance_type_record_type_record_key_org_id` ON `provenance_type` (`record_type`,`record_key`,`org_id`);',1,'','2023-09-28 20:45:54'),(368,'create alert_image table','CREATE TABLE IF NOT EXISTS `alert_image` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `token` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `path` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `url` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `created_at` DATETIME NOT NULL\n, `expires_at` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:54'),(369,'add unique index on token to alert_image table','CREATE UNIQUE INDEX `UQE_alert_image_token` ON `alert_image` (`token`);',1,'','2023-09-28 20:45:54'),(370,'support longer URLs in alert_image table','ALTER TABLE alert_image MODIFY url VARCHAR(2048) NOT NULL;',1,'','2023-09-28 20:45:54'),(371,'create_alert_configuration_history_table','CREATE TABLE IF NOT EXISTS `alert_configuration_history` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL DEFAULT 0\n, `alertmanager_configuration` MEDIUMTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `configuration_hash` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT \'not-yet-calculated\'\n, `configuration_version` VARCHAR(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `created_at` INT NOT NULL\n, `default` TINYINT(1) NOT NULL DEFAULT 0\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:54'),(372,'drop non-unique orgID index on alert_configuration','DROP INDEX `IDX_alert_configuration_org_id` ON `alert_configuration`',1,'','2023-09-28 20:45:55'),(373,'drop unique orgID index on alert_configuration if exists','DROP INDEX `UQE_alert_configuration_org_id` ON `alert_configuration`',1,'','2023-09-28 20:45:55'),(374,'extract alertmanager configuration history to separate table','code migration',1,'','2023-09-28 20:45:55'),(375,'add unique index on orgID to alert_configuration','CREATE UNIQUE INDEX `UQE_alert_configuration_org_id` ON `alert_configuration` (`org_id`);',1,'','2023-09-28 20:45:55'),(376,'add last_applied column to alert_configuration_history','alter table `alert_configuration_history` ADD COLUMN `last_applied` INT NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:55'),(377,'move dashboard alerts to unified alerting','code migration',1,'','2023-09-28 20:45:55'),(378,'create library_element table v1','CREATE TABLE IF NOT EXISTS `library_element` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `folder_id` BIGINT(20) NOT NULL\n, `uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `name` VARCHAR(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `kind` BIGINT(20) NOT NULL\n, `type` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `description` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `model` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `created` DATETIME NOT NULL\n, `created_by` BIGINT(20) NOT NULL\n, `updated` DATETIME NOT NULL\n, `updated_by` BIGINT(20) NOT NULL\n, `version` BIGINT(20) NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(379,'add index library_element org_id-folder_id-name-kind','CREATE UNIQUE INDEX `UQE_library_element_org_id_folder_id_name_kind` ON `library_element` (`org_id`,`folder_id`,`name`,`kind`);',1,'','2023-09-28 20:45:55'),(380,'create library_element_connection table v1','CREATE TABLE IF NOT EXISTS `library_element_connection` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `element_id` BIGINT(20) NOT NULL\n, `kind` BIGINT(20) NOT NULL\n, `connection_id` BIGINT(20) NOT NULL\n, `created` DATETIME NOT NULL\n, `created_by` BIGINT(20) NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(381,'add index library_element_connection element_id-kind-connection_id','CREATE UNIQUE INDEX `UQE_library_element_connection_element_id_kind_connection_id` ON `library_element_connection` (`element_id`,`kind`,`connection_id`);',1,'','2023-09-28 20:45:55'),(382,'add unique index library_element org_id_uid','CREATE UNIQUE INDEX `UQE_library_element_org_id_uid` ON `library_element` (`org_id`,`uid`);',1,'','2023-09-28 20:45:55'),(383,'increase max description length to 2048','ALTER TABLE `library_element` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, MODIFY `description` VARCHAR(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL ;',1,'','2023-09-28 20:45:55'),(384,'alter library_element model to mediumtext','ALTER TABLE library_element MODIFY model MEDIUMTEXT NOT NULL;',1,'','2023-09-28 20:45:55'),(385,'clone move dashboard alerts to unified alerting','code migration',1,'','2023-09-28 20:45:55'),(386,'create data_keys table','CREATE TABLE IF NOT EXISTS `data_keys` (\n`name` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci PRIMARY KEY NOT NULL\n, `active` TINYINT(1) NOT NULL\n, `scope` VARCHAR(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `provider` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `encrypted_data` BLOB NOT NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(387,'create secrets table','CREATE TABLE IF NOT EXISTS `secrets` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `namespace` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `type` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `value` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(388,'rename data_keys name column to id','ALTER TABLE `data_keys` CHANGE `name` `id` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci',1,'','2023-09-28 20:45:55'),(389,'add name column into data_keys','alter table `data_keys` ADD COLUMN `name` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT \'\' ',1,'','2023-09-28 20:45:55'),(390,'copy data_keys id column values into name','UPDATE data_keys SET name = id',1,'','2023-09-28 20:45:55'),(391,'rename data_keys name column to label','ALTER TABLE `data_keys` CHANGE `name` `label` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci',1,'','2023-09-28 20:45:55'),(392,'rename data_keys id column back to name','ALTER TABLE `data_keys` CHANGE `id` `name` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci',1,'','2023-09-28 20:45:55'),(393,'create kv_store table v1','CREATE TABLE IF NOT EXISTS `kv_store` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `namespace` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `key` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `value` MEDIUMTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(394,'add index kv_store.org_id-namespace-key','CREATE UNIQUE INDEX `UQE_kv_store_org_id_namespace_key` ON `kv_store` (`org_id`,`namespace`,`key`);',1,'','2023-09-28 20:45:55'),(395,'update dashboard_uid and panel_id from existing annotations','set dashboard_uid and panel_id migration',1,'','2023-09-28 20:45:55'),(396,'create permission table','CREATE TABLE IF NOT EXISTS `permission` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `role_id` BIGINT(20) NOT NULL\n, `action` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `scope` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(397,'add unique index permission.role_id','CREATE INDEX `IDX_permission_role_id` ON `permission` (`role_id`);',1,'','2023-09-28 20:45:55'),(398,'add unique index role_id_action_scope','CREATE UNIQUE INDEX `UQE_permission_role_id_action_scope` ON `permission` (`role_id`,`action`,`scope`);',1,'','2023-09-28 20:45:55'),(399,'create role table','CREATE TABLE IF NOT EXISTS `role` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `name` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `description` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `version` BIGINT(20) NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(400,'add column display_name','alter table `role` ADD COLUMN `display_name` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:55'),(401,'add column group_name','alter table `role` ADD COLUMN `group_name` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:55'),(402,'add index role.org_id','CREATE INDEX `IDX_role_org_id` ON `role` (`org_id`);',1,'','2023-09-28 20:45:55'),(403,'add unique index role_org_id_name','CREATE UNIQUE INDEX `UQE_role_org_id_name` ON `role` (`org_id`,`name`);',1,'','2023-09-28 20:45:55'),(404,'add index role_org_id_uid','CREATE UNIQUE INDEX `UQE_role_org_id_uid` ON `role` (`org_id`,`uid`);',1,'','2023-09-28 20:45:55'),(405,'create team role table','CREATE TABLE IF NOT EXISTS `team_role` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `team_id` BIGINT(20) NOT NULL\n, `role_id` BIGINT(20) NOT NULL\n, `created` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(406,'add index team_role.org_id','CREATE INDEX `IDX_team_role_org_id` ON `team_role` (`org_id`);',1,'','2023-09-28 20:45:55'),(407,'add unique index team_role_org_id_team_id_role_id','CREATE UNIQUE INDEX `UQE_team_role_org_id_team_id_role_id` ON `team_role` (`org_id`,`team_id`,`role_id`);',1,'','2023-09-28 20:45:55'),(408,'add index team_role.team_id','CREATE INDEX `IDX_team_role_team_id` ON `team_role` (`team_id`);',1,'','2023-09-28 20:45:55'),(409,'create user role table','CREATE TABLE IF NOT EXISTS `user_role` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `user_id` BIGINT(20) NOT NULL\n, `role_id` BIGINT(20) NOT NULL\n, `created` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(410,'add index user_role.org_id','CREATE INDEX `IDX_user_role_org_id` ON `user_role` (`org_id`);',1,'','2023-09-28 20:45:55'),(411,'add unique index user_role_org_id_user_id_role_id','CREATE UNIQUE INDEX `UQE_user_role_org_id_user_id_role_id` ON `user_role` (`org_id`,`user_id`,`role_id`);',1,'','2023-09-28 20:45:55'),(412,'add index user_role.user_id','CREATE INDEX `IDX_user_role_user_id` ON `user_role` (`user_id`);',1,'','2023-09-28 20:45:55'),(413,'create builtin role table','CREATE TABLE IF NOT EXISTS `builtin_role` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `role` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `role_id` BIGINT(20) NOT NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(414,'add index builtin_role.role_id','CREATE INDEX `IDX_builtin_role_role_id` ON `builtin_role` (`role_id`);',1,'','2023-09-28 20:45:55'),(415,'add index builtin_role.name','CREATE INDEX `IDX_builtin_role_role` ON `builtin_role` (`role`);',1,'','2023-09-28 20:45:55'),(416,'Add column org_id to builtin_role table','alter table `builtin_role` ADD COLUMN `org_id` BIGINT(20) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:55'),(417,'add index builtin_role.org_id','CREATE INDEX `IDX_builtin_role_org_id` ON `builtin_role` (`org_id`);',1,'','2023-09-28 20:45:55'),(418,'add unique index builtin_role_org_id_role_id_role','CREATE UNIQUE INDEX `UQE_builtin_role_org_id_role_id_role` ON `builtin_role` (`org_id`,`role_id`,`role`);',1,'','2023-09-28 20:45:55'),(419,'Remove unique index role_org_id_uid','DROP INDEX `UQE_role_org_id_uid` ON `role`',1,'','2023-09-28 20:45:55'),(420,'add unique index role.uid','CREATE UNIQUE INDEX `UQE_role_uid` ON `role` (`uid`);',1,'','2023-09-28 20:45:55'),(421,'create seed assignment table','CREATE TABLE IF NOT EXISTS `seed_assignment` (\n`builtin_role` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `role_name` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(422,'add unique index builtin_role_role_name','CREATE UNIQUE INDEX `UQE_seed_assignment_builtin_role_role_name` ON `seed_assignment` (`builtin_role`,`role_name`);',1,'','2023-09-28 20:45:55'),(423,'add column hidden to role table','alter table `role` ADD COLUMN `hidden` TINYINT(1) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:55'),(424,'permission kind migration','alter table `permission` ADD COLUMN `kind` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT \'\' ',1,'','2023-09-28 20:45:55'),(425,'permission attribute migration','alter table `permission` ADD COLUMN `attribute` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT \'\' ',1,'','2023-09-28 20:45:55'),(426,'permission identifier migration','alter table `permission` ADD COLUMN `identifier` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT \'\' ',1,'','2023-09-28 20:45:55'),(427,'add permission identifier index','CREATE INDEX `IDX_permission_identifier` ON `permission` (`identifier`);',1,'','2023-09-28 20:45:55'),(428,'create query_history table v1','CREATE TABLE IF NOT EXISTS `query_history` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `datasource_uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `created_by` INT NOT NULL\n, `created_at` INT NOT NULL\n, `comment` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `queries` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(429,'add index query_history.org_id-created_by-datasource_uid','CREATE INDEX `IDX_query_history_org_id_created_by_datasource_uid` ON `query_history` (`org_id`,`created_by`,`datasource_uid`);',1,'','2023-09-28 20:45:55'),(430,'alter table query_history alter column created_by type to bigint','ALTER TABLE query_history MODIFY created_by BIGINT;',1,'','2023-09-28 20:45:55'),(431,'rbac disabled migrator','code migration',1,'','2023-09-28 20:45:55'),(432,'teams permissions migration','code migration',1,'','2023-09-28 20:45:55'),(433,'dashboard permissions','code migration',1,'','2023-09-28 20:45:55'),(434,'dashboard permissions uid scopes','code migration',1,'','2023-09-28 20:45:55'),(435,'drop managed folder create actions','code migration',1,'','2023-09-28 20:45:55'),(436,'alerting notification permissions','code migration',1,'','2023-09-28 20:45:55'),(437,'create query_history_star table v1','CREATE TABLE IF NOT EXISTS `query_history_star` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `query_uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `user_id` INT NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(438,'add index query_history.user_id-query_uid','CREATE UNIQUE INDEX `UQE_query_history_star_user_id_query_uid` ON `query_history_star` (`user_id`,`query_uid`);',1,'','2023-09-28 20:45:55'),(439,'add column org_id in query_history_star','alter table `query_history_star` ADD COLUMN `org_id` BIGINT(20) NOT NULL DEFAULT 1 ',1,'','2023-09-28 20:45:55'),(440,'alter table query_history_star_mig column user_id type to bigint','ALTER TABLE query_history_star MODIFY user_id BIGINT;',1,'','2023-09-28 20:45:55'),(441,'create correlation table v1','CREATE TABLE IF NOT EXISTS `correlation` (\n`uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `source_uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `target_uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `label` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `description` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, PRIMARY KEY ( `uid`,`source_uid` )) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(442,'add index correlations.uid','CREATE INDEX `IDX_correlation_uid` ON `correlation` (`uid`);',1,'','2023-09-28 20:45:55'),(443,'add index correlations.source_uid','CREATE INDEX `IDX_correlation_source_uid` ON `correlation` (`source_uid`);',1,'','2023-09-28 20:45:55'),(444,'add correlation config column','alter table `correlation` ADD COLUMN `config` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:55'),(445,'create entity_events table','CREATE TABLE IF NOT EXISTS `entity_event` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `entity_id` VARCHAR(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `event_type` VARCHAR(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `created` BIGINT(20) NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(446,'create dashboard public config v1','CREATE TABLE IF NOT EXISTS `dashboard_public_config` (\n`uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci PRIMARY KEY NOT NULL\n, `dashboard_uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `time_settings` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `refresh_rate` INT NOT NULL DEFAULT 30\n, `template_variables` MEDIUMTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(447,'drop index UQE_dashboard_public_config_uid - v1','DROP INDEX `UQE_dashboard_public_config_uid` ON `dashboard_public_config`',1,'','2023-09-28 20:45:55'),(448,'drop index IDX_dashboard_public_config_org_id_dashboard_uid - v1','DROP INDEX `IDX_dashboard_public_config_org_id_dashboard_uid` ON `dashboard_public_config`',1,'','2023-09-28 20:45:55'),(449,'Drop old dashboard public config table','DROP TABLE IF EXISTS `dashboard_public_config`',1,'','2023-09-28 20:45:55'),(450,'recreate dashboard public config v1','CREATE TABLE IF NOT EXISTS `dashboard_public_config` (\n`uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci PRIMARY KEY NOT NULL\n, `dashboard_uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `time_settings` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `refresh_rate` INT NOT NULL DEFAULT 30\n, `template_variables` MEDIUMTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(451,'create index UQE_dashboard_public_config_uid - v1','CREATE UNIQUE INDEX `UQE_dashboard_public_config_uid` ON `dashboard_public_config` (`uid`);',1,'','2023-09-28 20:45:55'),(452,'create index IDX_dashboard_public_config_org_id_dashboard_uid - v1','CREATE INDEX `IDX_dashboard_public_config_org_id_dashboard_uid` ON `dashboard_public_config` (`org_id`,`dashboard_uid`);',1,'','2023-09-28 20:45:55'),(453,'drop index UQE_dashboard_public_config_uid - v2','DROP INDEX `UQE_dashboard_public_config_uid` ON `dashboard_public_config`',1,'','2023-09-28 20:45:55'),(454,'drop index IDX_dashboard_public_config_org_id_dashboard_uid - v2','DROP INDEX `IDX_dashboard_public_config_org_id_dashboard_uid` ON `dashboard_public_config`',1,'','2023-09-28 20:45:55'),(455,'Drop public config table','DROP TABLE IF EXISTS `dashboard_public_config`',1,'','2023-09-28 20:45:55'),(456,'Recreate dashboard public config v2','CREATE TABLE IF NOT EXISTS `dashboard_public_config` (\n`uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci PRIMARY KEY NOT NULL\n, `dashboard_uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `time_settings` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `template_variables` MEDIUMTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `access_token` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `created_by` INT NOT NULL\n, `updated_by` INT NULL\n, `created_at` DATETIME NOT NULL\n, `updated_at` DATETIME NULL\n, `is_enabled` TINYINT(1) NOT NULL DEFAULT 0\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(457,'create index UQE_dashboard_public_config_uid - v2','CREATE UNIQUE INDEX `UQE_dashboard_public_config_uid` ON `dashboard_public_config` (`uid`);',1,'','2023-09-28 20:45:55'),(458,'create index IDX_dashboard_public_config_org_id_dashboard_uid - v2','CREATE INDEX `IDX_dashboard_public_config_org_id_dashboard_uid` ON `dashboard_public_config` (`org_id`,`dashboard_uid`);',1,'','2023-09-28 20:45:55'),(459,'create index UQE_dashboard_public_config_access_token - v2','CREATE UNIQUE INDEX `UQE_dashboard_public_config_access_token` ON `dashboard_public_config` (`access_token`);',1,'','2023-09-28 20:45:55'),(460,'Rename table dashboard_public_config to dashboard_public - v2','ALTER TABLE `dashboard_public_config` RENAME TO `dashboard_public`',1,'','2023-09-28 20:45:55'),(461,'add annotations_enabled column','alter table `dashboard_public` ADD COLUMN `annotations_enabled` TINYINT(1) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:55'),(462,'add time_selection_enabled column','alter table `dashboard_public` ADD COLUMN `time_selection_enabled` TINYINT(1) NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:55'),(463,'delete orphaned public dashboards','DELETE FROM dashboard_public WHERE dashboard_uid NOT IN (SELECT uid FROM dashboard)',1,'','2023-09-28 20:45:55'),(464,'add share column','alter table `dashboard_public` ADD COLUMN `share` VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT \'public\' ',1,'','2023-09-28 20:45:55'),(465,'backfill empty share column fields with default of public','UPDATE dashboard_public SET share=\'public\' WHERE share=\'\'',1,'','2023-09-28 20:45:55'),(466,'create default alerting folders','code migration',1,'','2023-09-28 20:45:55'),(467,'create file table','CREATE TABLE IF NOT EXISTS `file` (\n`path` VARCHAR(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `path_hash` VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `parent_folder_path_hash` VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `contents` BLOB NOT NULL\n, `etag` VARCHAR(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `cache_control` VARCHAR(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `content_disposition` VARCHAR(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `updated` DATETIME NOT NULL\n, `created` DATETIME NOT NULL\n, `size` BIGINT(20) NOT NULL\n, `mime_type` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(468,'file table idx: path natural pk','CREATE UNIQUE INDEX `UQE_file_path_hash` ON `file` (`path_hash`);',1,'','2023-09-28 20:45:55'),(469,'file table idx: parent_folder_path_hash fast folder retrieval','CREATE INDEX `IDX_file_parent_folder_path_hash` ON `file` (`parent_folder_path_hash`);',1,'','2023-09-28 20:45:55'),(470,'create file_meta table','CREATE TABLE IF NOT EXISTS `file_meta` (\n`path_hash` VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `key` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `value` VARCHAR(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(471,'file table idx: path key','CREATE UNIQUE INDEX `UQE_file_meta_path_hash_key` ON `file_meta` (`path_hash`,`key`);',1,'','2023-09-28 20:45:55'),(472,'set path collation in file table','SELECT 0;',1,'','2023-09-28 20:45:55'),(473,'migrate contents column to mediumblob for MySQL','ALTER TABLE file MODIFY contents MEDIUMBLOB;',1,'','2023-09-28 20:45:55'),(474,'managed permissions migration','code migration',1,'','2023-09-28 20:45:55'),(475,'managed folder permissions alert actions migration','code migration',1,'','2023-09-28 20:45:55'),(476,'RBAC action name migrator','code migration',1,'','2023-09-28 20:45:55'),(477,'Add UID column to playlist','alter table `playlist` ADD COLUMN `uid` VARCHAR(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 0 ',1,'','2023-09-28 20:45:55'),(478,'Update uid column values in playlist','UPDATE playlist SET uid=id;',1,'','2023-09-28 20:45:55'),(479,'Add index for uid in playlist','CREATE UNIQUE INDEX `UQE_playlist_org_id_uid` ON `playlist` (`org_id`,`uid`);',1,'','2023-09-28 20:45:55'),(480,'update group index for alert rules','code migration',1,'','2023-09-28 20:45:55'),(481,'managed folder permissions alert actions repeated migration','code migration',1,'','2023-09-28 20:45:55'),(482,'admin only folder/dashboard permission','code migration',1,'','2023-09-28 20:45:55'),(483,'add action column to seed_assignment','alter table `seed_assignment` ADD COLUMN `action` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:55'),(484,'add scope column to seed_assignment','alter table `seed_assignment` ADD COLUMN `scope` VARCHAR(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL ',1,'','2023-09-28 20:45:55'),(485,'remove unique index builtin_role_role_name before nullable update','DROP INDEX `UQE_seed_assignment_builtin_role_role_name` ON `seed_assignment`',1,'','2023-09-28 20:45:55'),(486,'update seed_assignment role_name column to nullable','ALTER TABLE seed_assignment MODIFY role_name VARCHAR(190) DEFAULT NULL;',1,'','2023-09-28 20:45:55'),(487,'add unique index builtin_role_name back','CREATE UNIQUE INDEX `UQE_seed_assignment_builtin_role_role_name` ON `seed_assignment` (`builtin_role`,`role_name`);',1,'','2023-09-28 20:45:55'),(488,'add unique index builtin_role_action_scope','CREATE UNIQUE INDEX `UQE_seed_assignment_builtin_role_action_scope` ON `seed_assignment` (`builtin_role`,`action`,`scope`);',1,'','2023-09-28 20:45:55'),(489,'add primary key to seed_assigment','code migration',1,'','2023-09-28 20:45:55'),(490,'managed folder permissions alert actions repeated fixed migration','code migration',1,'','2023-09-28 20:45:55'),(491,'migrate external alertmanagers to datsourcse','migrate external alertmanagers to datasource',1,'','2023-09-28 20:45:55'),(492,'create folder table','CREATE TABLE IF NOT EXISTS `folder` (\n`id` BIGINT(20) PRIMARY KEY AUTO_INCREMENT NOT NULL\n, `uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `org_id` BIGINT(20) NOT NULL\n, `title` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL\n, `description` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `parent_uid` VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL\n, `created` DATETIME NOT NULL\n, `updated` DATETIME NOT NULL\n) ENGINE=InnoDB DEFAULT CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;',1,'','2023-09-28 20:45:55'),(493,'Add index for parent_uid','CREATE INDEX `IDX_folder_parent_uid_org_id` ON `folder` (`parent_uid`,`org_id`);',1,'','2023-09-28 20:45:55'),(494,'Add unique index for folder.uid and folder.org_id','CREATE UNIQUE INDEX `UQE_folder_uid_org_id` ON `folder` (`uid`,`org_id`);',1,'','2023-09-28 20:45:55'),(495,'Update folder title length','ALTER TABLE `folder` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, MODIFY `title` VARCHAR(189) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL ;',1,'','2023-09-28 20:45:55'),(496,'Add unique index for folder.title and folder.parent_uid','CREATE UNIQUE INDEX `UQE_folder_title_parent_uid` ON `folder` (`title`,`parent_uid`);',1,'','2023-09-28 20:45:55');
/*!40000 ALTER TABLE `migration_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ngalert_configuration`
--

DROP TABLE IF EXISTS `ngalert_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ngalert_configuration` (
                                         `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                         `org_id` bigint(20) NOT NULL,
                                         `alertmanagers` text COLLATE utf8mb4_unicode_ci,
                                         `created_at` int(11) NOT NULL,
                                         `updated_at` int(11) NOT NULL,
                                         `send_alerts_to` smallint(6) NOT NULL DEFAULT '0',
                                         PRIMARY KEY (`id`),
                                         UNIQUE KEY `UQE_ngalert_configuration_org_id` (`org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ngalert_configuration`
--

LOCK TABLES `ngalert_configuration` WRITE;
/*!40000 ALTER TABLE `ngalert_configuration` DISABLE KEYS */;
/*!40000 ALTER TABLE `ngalert_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `org`
--

DROP TABLE IF EXISTS `org`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `org` (
                       `id` bigint(20) NOT NULL AUTO_INCREMENT,
                       `version` int(11) NOT NULL,
                       `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                       `address1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                       `address2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                       `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                       `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                       `zip_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                       `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                       `billing_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                       `created` datetime NOT NULL,
                       `updated` datetime NOT NULL,
                       PRIMARY KEY (`id`),
                       UNIQUE KEY `UQE_org_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `org`
--

LOCK TABLES `org` WRITE;
/*!40000 ALTER TABLE `org` DISABLE KEYS */;
INSERT INTO `org` VALUES (1,0,'Main Org.','','','','','','',NULL,'2023-09-28 20:45:55','2023-09-28 20:45:55');
/*!40000 ALTER TABLE `org` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `org_user`
--

DROP TABLE IF EXISTS `org_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `org_user` (
                            `id` bigint(20) NOT NULL AUTO_INCREMENT,
                            `org_id` bigint(20) NOT NULL,
                            `user_id` bigint(20) NOT NULL,
                            `role` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
                            `created` datetime NOT NULL,
                            `updated` datetime NOT NULL,
                            PRIMARY KEY (`id`),
                            UNIQUE KEY `UQE_org_user_org_id_user_id` (`org_id`,`user_id`),
                            KEY `IDX_org_user_org_id` (`org_id`),
                            KEY `IDX_org_user_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `org_user`
--

LOCK TABLES `org_user` WRITE;
/*!40000 ALTER TABLE `org_user` DISABLE KEYS */;
INSERT INTO `org_user` VALUES (1,1,1,'Admin','2023-09-28 20:45:55','2023-09-28 20:45:55');
/*!40000 ALTER TABLE `org_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission` (
                              `id` bigint(20) NOT NULL AUTO_INCREMENT,
                              `role_id` bigint(20) NOT NULL,
                              `action` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `scope` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `created` datetime NOT NULL,
                              `updated` datetime NOT NULL,
                              `kind` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
                              `attribute` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
                              `identifier` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
                              PRIMARY KEY (`id`),
                              UNIQUE KEY `UQE_permission_role_id_action_scope` (`role_id`,`action`,`scope`),
                              KEY `IDX_permission_role_id` (`role_id`),
                              KEY `IDX_permission_identifier` (`identifier`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission`
--

LOCK TABLES `permission` WRITE;
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
INSERT INTO `permission` VALUES (5,3,'dashboards:delete','dashboards:uid:f2945aef-5b5a-4e47-90dc-de604270e1fa','2023-09-28 21:05:42','2023-09-28 21:05:42','','',''),(6,3,'dashboards.permissions:read','dashboards:uid:f2945aef-5b5a-4e47-90dc-de604270e1fa','2023-09-28 21:05:42','2023-09-28 21:05:42','','',''),(7,3,'dashboards.permissions:write','dashboards:uid:f2945aef-5b5a-4e47-90dc-de604270e1fa','2023-09-28 21:05:42','2023-09-28 21:05:42','','',''),(8,3,'dashboards:read','dashboards:uid:f2945aef-5b5a-4e47-90dc-de604270e1fa','2023-09-28 21:05:42','2023-09-28 21:05:42','','',''),(9,3,'dashboards:write','dashboards:uid:f2945aef-5b5a-4e47-90dc-de604270e1fa','2023-09-28 21:05:42','2023-09-28 21:05:42','','',''),(10,1,'dashboards:read','dashboards:uid:f2945aef-5b5a-4e47-90dc-de604270e1fa','2023-09-28 21:05:42','2023-09-28 21:05:42','','',''),(11,1,'dashboards:write','dashboards:uid:f2945aef-5b5a-4e47-90dc-de604270e1fa','2023-09-28 21:05:42','2023-09-28 21:05:42','','',''),(12,1,'dashboards:delete','dashboards:uid:f2945aef-5b5a-4e47-90dc-de604270e1fa','2023-09-28 21:05:42','2023-09-28 21:05:42','','',''),(13,2,'dashboards:read','dashboards:uid:f2945aef-5b5a-4e47-90dc-de604270e1fa','2023-09-28 21:05:42','2023-09-28 21:05:42','','',''),(14,3,'dashboards:write','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(15,3,'dashboards.permissions:write','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(16,3,'folders.permissions:read','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(17,3,'alert.rules:read','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(18,3,'dashboards:create','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(19,3,'alert.rules:delete','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(20,3,'dashboards.permissions:read','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(21,3,'folders:read','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(22,3,'folders:delete','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(23,3,'alert.rules:create','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(24,3,'alert.rules:write','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(25,3,'dashboards:read','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(26,3,'dashboards:delete','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(27,3,'folders:write','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(28,3,'folders.permissions:write','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(29,1,'dashboards:read','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(30,1,'dashboards:write','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(31,1,'dashboards:delete','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(32,1,'folders:read','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(33,1,'folders:write','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(34,1,'folders:delete','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(35,1,'alert.rules:create','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(36,1,'alert.rules:write','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(37,1,'alert.rules:read','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(38,1,'dashboards:create','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(39,1,'alert.rules:delete','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(40,2,'alert.rules:read','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(41,2,'dashboards:read','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','',''),(42,2,'folders:read','folders:uid:b38e7302-eee9-49b1-abae-2d561374371c','2023-09-28 21:31:49','2023-09-28 21:31:49','','','');
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playlist`
--

DROP TABLE IF EXISTS `playlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playlist` (
                            `id` bigint(20) NOT NULL AUTO_INCREMENT,
                            `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                            `interval` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                            `org_id` bigint(20) NOT NULL,
                            `uid` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
                            PRIMARY KEY (`id`),
                            UNIQUE KEY `UQE_playlist_org_id_uid` (`org_id`,`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playlist`
--

LOCK TABLES `playlist` WRITE;
/*!40000 ALTER TABLE `playlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `playlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playlist_item`
--

DROP TABLE IF EXISTS `playlist_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playlist_item` (
                                 `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                 `playlist_id` bigint(20) NOT NULL,
                                 `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `order` int(11) NOT NULL,
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playlist_item`
--

LOCK TABLES `playlist_item` WRITE;
/*!40000 ALTER TABLE `playlist_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `playlist_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plugin_setting`
--

DROP TABLE IF EXISTS `plugin_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plugin_setting` (
                                  `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                  `org_id` bigint(20) DEFAULT NULL,
                                  `plugin_id` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                                  `enabled` tinyint(1) NOT NULL,
                                  `pinned` tinyint(1) NOT NULL,
                                  `json_data` text COLLATE utf8mb4_unicode_ci,
                                  `secure_json_data` text COLLATE utf8mb4_unicode_ci,
                                  `created` datetime NOT NULL,
                                  `updated` datetime NOT NULL,
                                  `plugin_version` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                  PRIMARY KEY (`id`),
                                  UNIQUE KEY `UQE_plugin_setting_org_id_plugin_id` (`org_id`,`plugin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plugin_setting`
--

LOCK TABLES `plugin_setting` WRITE;
/*!40000 ALTER TABLE `plugin_setting` DISABLE KEYS */;
/*!40000 ALTER TABLE `plugin_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `preferences`
--

DROP TABLE IF EXISTS `preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preferences` (
                               `id` bigint(20) NOT NULL AUTO_INCREMENT,
                               `org_id` bigint(20) NOT NULL,
                               `user_id` bigint(20) NOT NULL,
                               `version` int(11) NOT NULL,
                               `home_dashboard_id` bigint(20) NOT NULL,
                               `timezone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `theme` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `created` datetime NOT NULL,
                               `updated` datetime NOT NULL,
                               `team_id` bigint(20) DEFAULT NULL,
                               `week_start` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `json_data` mediumtext COLLATE utf8mb4_unicode_ci,
                               PRIMARY KEY (`id`),
                               KEY `IDX_preferences_org_id` (`org_id`),
                               KEY `IDX_preferences_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `preferences`
--

LOCK TABLES `preferences` WRITE;
/*!40000 ALTER TABLE `preferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provenance_type`
--

DROP TABLE IF EXISTS `provenance_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provenance_type` (
                                   `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                   `org_id` bigint(20) NOT NULL,
                                   `record_key` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `record_type` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `provenance` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   PRIMARY KEY (`id`),
                                   UNIQUE KEY `UQE_provenance_type_record_type_record_key_org_id` (`record_type`,`record_key`,`org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provenance_type`
--

LOCK TABLES `provenance_type` WRITE;
/*!40000 ALTER TABLE `provenance_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `provenance_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `query_history`
--

DROP TABLE IF EXISTS `query_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `query_history` (
                                 `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                 `uid` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `org_id` bigint(20) NOT NULL,
                                 `datasource_uid` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `created_by` bigint(20) DEFAULT NULL,
                                 `created_at` int(11) NOT NULL,
                                 `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `queries` text COLLATE utf8mb4_unicode_ci NOT NULL,
                                 PRIMARY KEY (`id`),
                                 KEY `IDX_query_history_org_id_created_by_datasource_uid` (`org_id`,`created_by`,`datasource_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `query_history`
--

LOCK TABLES `query_history` WRITE;
/*!40000 ALTER TABLE `query_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `query_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `query_history_star`
--

DROP TABLE IF EXISTS `query_history_star`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `query_history_star` (
                                      `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                      `query_uid` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
                                      `user_id` bigint(20) DEFAULT NULL,
                                      `org_id` bigint(20) NOT NULL DEFAULT '1',
                                      PRIMARY KEY (`id`),
                                      UNIQUE KEY `UQE_query_history_star_user_id_query_uid` (`user_id`,`query_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `query_history_star`
--

LOCK TABLES `query_history_star` WRITE;
/*!40000 ALTER TABLE `query_history_star` DISABLE KEYS */;
/*!40000 ALTER TABLE `query_history_star` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quota`
--

DROP TABLE IF EXISTS `quota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quota` (
                         `id` bigint(20) NOT NULL AUTO_INCREMENT,
                         `org_id` bigint(20) DEFAULT NULL,
                         `user_id` bigint(20) DEFAULT NULL,
                         `target` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `limit` bigint(20) NOT NULL,
                         `created` datetime NOT NULL,
                         `updated` datetime NOT NULL,
                         PRIMARY KEY (`id`),
                         UNIQUE KEY `UQE_quota_org_id_user_id_target` (`org_id`,`user_id`,`target`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quota`
--

LOCK TABLES `quota` WRITE;
/*!40000 ALTER TABLE `quota` DISABLE KEYS */;
/*!40000 ALTER TABLE `quota` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
                        `id` bigint(20) NOT NULL AUTO_INCREMENT,
                        `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `description` text COLLATE utf8mb4_unicode_ci,
                        `version` bigint(20) NOT NULL,
                        `org_id` bigint(20) NOT NULL,
                        `uid` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `created` datetime NOT NULL,
                        `updated` datetime NOT NULL,
                        `display_name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `group_name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `hidden` tinyint(1) NOT NULL DEFAULT '0',
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `UQE_role_org_id_name` (`org_id`,`name`),
                        UNIQUE KEY `UQE_role_uid` (`uid`),
                        KEY `IDX_role_org_id` (`org_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'managed:builtins:editor:permissions','',0,1,'af5caa1e-dbe3-477f-afa5-9fc6b268ca52','2023-09-28 20:45:57','2023-09-28 20:45:57','','',0),(2,'managed:builtins:viewer:permissions','',0,1,'f953873d-9c68-4e65-9d31-b745fcb5af2c','2023-09-28 20:45:57','2023-09-28 20:45:57','','',0),(3,'managed:users:1:permissions','',0,1,'b42c5834-51fa-43cd-ac4e-369fed57a15a','2023-09-28 21:05:42','2023-09-28 21:05:42','','',0);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `secrets`
--

DROP TABLE IF EXISTS `secrets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `secrets` (
                           `id` bigint(20) NOT NULL AUTO_INCREMENT,
                           `org_id` bigint(20) NOT NULL,
                           `namespace` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                           `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                           `value` text COLLATE utf8mb4_unicode_ci,
                           `created` datetime NOT NULL,
                           `updated` datetime NOT NULL,
                           PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `secrets`
--

LOCK TABLES `secrets` WRITE;
/*!40000 ALTER TABLE `secrets` DISABLE KEYS */;
INSERT INTO `secrets` VALUES (1,1,'TestData','datasource','I1lUYzJZVEV6TmpjdE16STNZUzAwTlRCbUxXSTJNbVl0TVdVeVpqQXhPVFppWVdOaCMqWVdWekxXTm1ZZyo4eUhETFhJWlCdy/o3hAiBbllCDrN2HzeWmw','2023-09-28 20:45:57','2023-09-28 20:47:45'),(2,1,'Auto Kuca','datasource','I1lXWTVaamhqTXpFdFpEZ3pOaTAwTldWbExUazRZekl0WTJOalpHRmtPR1F6Tm1FNSMqWVdWekxXTm1ZZypEemNzZHBoU5zX8XVe7EcOx1zFBLI0vwp6DRdzaJA3Wg04ypITN8WIclUvPB0smLmWTJQ','2023-09-29 13:45:12','2023-09-29 13:45:23');
/*!40000 ALTER TABLE `secrets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seed_assignment`
--

DROP TABLE IF EXISTS `seed_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seed_assignment` (
                                   `id` int(11) NOT NULL AUTO_INCREMENT,
                                   `builtin_role` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `role_name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                   `action` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                   `scope` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                   PRIMARY KEY (`id`),
                                   UNIQUE KEY `UQE_seed_assignment_builtin_role_role_name` (`builtin_role`,`role_name`),
                                   UNIQUE KEY `UQE_seed_assignment_builtin_role_action_scope` (`builtin_role`,`action`,`scope`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seed_assignment`
--

LOCK TABLES `seed_assignment` WRITE;
/*!40000 ALTER TABLE `seed_assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `seed_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `server_lock`
--

DROP TABLE IF EXISTS `server_lock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `server_lock` (
                               `id` bigint(20) NOT NULL AUTO_INCREMENT,
                               `operation_uid` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `version` bigint(20) NOT NULL,
                               `last_execution` bigint(20) NOT NULL,
                               PRIMARY KEY (`id`),
                               UNIQUE KEY `UQE_server_lock_operation_uid` (`operation_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `server_lock`
--

LOCK TABLES `server_lock` WRITE;
/*!40000 ALTER TABLE `server_lock` DISABLE KEYS */;
INSERT INTO `server_lock` VALUES (2,'cleanup expired auth tokens',2,1695994850),(4,'delete old login attempts',4,1695937658);
/*!40000 ALTER TABLE `server_lock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
                           `key` char(16) COLLATE utf8mb4_unicode_ci NOT NULL,
                           `data` blob NOT NULL,
                           `expiry` int(255) NOT NULL,
                           PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `short_url`
--

DROP TABLE IF EXISTS `short_url`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `short_url` (
                             `id` bigint(20) NOT NULL AUTO_INCREMENT,
                             `org_id` bigint(20) NOT NULL,
                             `uid` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `path` text COLLATE utf8mb4_unicode_ci NOT NULL,
                             `created_by` bigint(20) DEFAULT NULL,
                             `created_at` int(11) NOT NULL,
                             `last_seen_at` int(11) DEFAULT NULL,
                             PRIMARY KEY (`id`),
                             UNIQUE KEY `UQE_short_url_org_id_uid` (`org_id`,`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `short_url`
--

LOCK TABLES `short_url` WRITE;
/*!40000 ALTER TABLE `short_url` DISABLE KEYS */;
/*!40000 ALTER TABLE `short_url` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `star`
--

DROP TABLE IF EXISTS `star`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `star` (
                        `id` bigint(20) NOT NULL AUTO_INCREMENT,
                        `user_id` bigint(20) NOT NULL,
                        `dashboard_id` bigint(20) NOT NULL,
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `UQE_star_user_id_dashboard_id` (`user_id`,`dashboard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `star`
--

LOCK TABLES `star` WRITE;
/*!40000 ALTER TABLE `star` DISABLE KEYS */;
/*!40000 ALTER TABLE `star` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
                       `id` bigint(20) NOT NULL AUTO_INCREMENT,
                       `key` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                       `value` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                       PRIMARY KEY (`id`),
                       UNIQUE KEY `UQE_tag_key_value` (`key`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
                        `id` bigint(20) NOT NULL AUTO_INCREMENT,
                        `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `org_id` bigint(20) NOT NULL,
                        `created` datetime NOT NULL,
                        `updated` datetime NOT NULL,
                        `uid` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `email` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `UQE_team_org_id_name` (`org_id`,`name`),
                        UNIQUE KEY `UQE_team_org_id_uid` (`org_id`,`uid`),
                        KEY `IDX_team_org_id` (`org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_member`
--

DROP TABLE IF EXISTS `team_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_member` (
                               `id` bigint(20) NOT NULL AUTO_INCREMENT,
                               `org_id` bigint(20) NOT NULL,
                               `team_id` bigint(20) NOT NULL,
                               `user_id` bigint(20) NOT NULL,
                               `created` datetime NOT NULL,
                               `updated` datetime NOT NULL,
                               `external` tinyint(1) DEFAULT NULL,
                               `permission` smallint(6) DEFAULT NULL,
                               PRIMARY KEY (`id`),
                               UNIQUE KEY `UQE_team_member_org_id_team_id_user_id` (`org_id`,`team_id`,`user_id`),
                               KEY `IDX_team_member_org_id` (`org_id`),
                               KEY `IDX_team_member_team_id` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_member`
--

LOCK TABLES `team_member` WRITE;
/*!40000 ALTER TABLE `team_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `team_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_role`
--

DROP TABLE IF EXISTS `team_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_role` (
                             `id` bigint(20) NOT NULL AUTO_INCREMENT,
                             `org_id` bigint(20) NOT NULL,
                             `team_id` bigint(20) NOT NULL,
                             `role_id` bigint(20) NOT NULL,
                             `created` datetime NOT NULL,
                             PRIMARY KEY (`id`),
                             UNIQUE KEY `UQE_team_role_org_id_team_id_role_id` (`org_id`,`team_id`,`role_id`),
                             KEY `IDX_team_role_org_id` (`org_id`),
                             KEY `IDX_team_role_team_id` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_role`
--

LOCK TABLES `team_role` WRITE;
/*!40000 ALTER TABLE `team_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `team_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_user`
--

DROP TABLE IF EXISTS `temp_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_user` (
                             `id` bigint(20) NOT NULL AUTO_INCREMENT,
                             `org_id` bigint(20) NOT NULL,
                             `version` int(11) NOT NULL,
                             `email` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `code` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `invited_by_user_id` bigint(20) DEFAULT NULL,
                             `email_sent` tinyint(1) NOT NULL,
                             `email_sent_on` datetime DEFAULT NULL,
                             `remote_addr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `created` int(11) NOT NULL DEFAULT '0',
                             `updated` int(11) NOT NULL DEFAULT '0',
                             PRIMARY KEY (`id`),
                             KEY `IDX_temp_user_email` (`email`),
                             KEY `IDX_temp_user_org_id` (`org_id`),
                             KEY `IDX_temp_user_code` (`code`),
                             KEY `IDX_temp_user_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temp_user`
--

LOCK TABLES `temp_user` WRITE;
/*!40000 ALTER TABLE `temp_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `temp_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_data`
--

DROP TABLE IF EXISTS `test_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_data` (
                             `id` int(11) NOT NULL AUTO_INCREMENT,
                             `metric1` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `metric2` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `value_big_int` bigint(20) DEFAULT NULL,
                             `value_double` double DEFAULT NULL,
                             `value_float` float DEFAULT NULL,
                             `value_int` int(11) DEFAULT NULL,
                             `time_epoch` bigint(20) NOT NULL,
                             `time_date_time` datetime NOT NULL,
                             `time_time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_data`
--

LOCK TABLES `test_data` WRITE;
/*!40000 ALTER TABLE `test_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `test_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
                        `id` bigint(20) NOT NULL AUTO_INCREMENT,
                        `version` int(11) NOT NULL,
                        `login` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `email` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `salt` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `rands` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `org_id` bigint(20) NOT NULL,
                        `is_admin` tinyint(1) NOT NULL,
                        `email_verified` tinyint(1) DEFAULT NULL,
                        `theme` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `created` datetime NOT NULL,
                        `updated` datetime NOT NULL,
                        `help_flags1` bigint(20) NOT NULL DEFAULT '0',
                        `last_seen_at` datetime DEFAULT NULL,
                        `is_disabled` tinyint(1) NOT NULL DEFAULT '0',
                        `is_service_account` tinyint(1) DEFAULT '0',
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `UQE_user_login` (`login`),
                        UNIQUE KEY `UQE_user_email` (`email`),
                        KEY `IDX_user_login_email` (`login`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,0,'admin','admin@localhost','','c7e7c449e7d44df9d7415ca47ebdac628ad19fa2783f08e2cbacab9e24f824dca020879751d0a7a599acf1195f69b6fb3088','fBppBz3qyK','HCPuugvxId','',1,1,0,'','2023-09-28 20:45:55','2023-09-28 20:45:55',0,'2023-09-29 13:42:35',0,0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_auth`
--

DROP TABLE IF EXISTS `user_auth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_auth` (
                             `id` bigint(20) NOT NULL AUTO_INCREMENT,
                             `user_id` bigint(20) NOT NULL,
                             `auth_module` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `auth_id` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `created` datetime NOT NULL,
                             `o_auth_access_token` text COLLATE utf8mb4_unicode_ci,
                             `o_auth_refresh_token` text COLLATE utf8mb4_unicode_ci,
                             `o_auth_token_type` text COLLATE utf8mb4_unicode_ci,
                             `o_auth_expiry` datetime DEFAULT NULL,
                             `o_auth_id_token` text COLLATE utf8mb4_unicode_ci,
                             PRIMARY KEY (`id`),
                             KEY `IDX_user_auth_auth_module_auth_id` (`auth_module`,`auth_id`),
                             KEY `IDX_user_auth_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_auth`
--

LOCK TABLES `user_auth` WRITE;
/*!40000 ALTER TABLE `user_auth` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_auth` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_auth_token`
--

DROP TABLE IF EXISTS `user_auth_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_auth_token` (
                                   `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                   `user_id` bigint(20) NOT NULL,
                                   `auth_token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `prev_auth_token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `user_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `client_ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `auth_token_seen` tinyint(1) NOT NULL,
                                   `seen_at` int(11) DEFAULT NULL,
                                   `rotated_at` int(11) NOT NULL,
                                   `created_at` int(11) NOT NULL,
                                   `updated_at` int(11) NOT NULL,
                                   `revoked_at` int(11) DEFAULT NULL,
                                   PRIMARY KEY (`id`),
                                   UNIQUE KEY `UQE_user_auth_token_auth_token` (`auth_token`),
                                   UNIQUE KEY `UQE_user_auth_token_prev_auth_token` (`prev_auth_token`),
                                   KEY `IDX_user_auth_token_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_auth_token`
--

LOCK TABLES `user_auth_token` WRITE;
/*!40000 ALTER TABLE `user_auth_token` DISABLE KEYS */;
INSERT INTO `user_auth_token` VALUES (1,1,'0c2ff98ecc894698a5719a37f8d18a414bfc760ce56f4988a5a984722ac28225','aa89873f14a2b11b750c6dbd8c3b06c7634ba3967b521a7c471c1c93771b99f1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36','172.21.0.1',1,1695939059,1695939052,1695933999,1695933999,0),(2,1,'3c73bb7a2976e293e6dc4d3b3be3140d69c7169ad40edffe2a7c335af8599824','3c73bb7a2976e293e6dc4d3b3be3140d69c7169ad40edffe2a7c335af8599824','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36','192.168.240.1',1,1695994955,1695994954,1695994954,1695994954,0);
/*!40000 ALTER TABLE `user_auth_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
                             `id` bigint(20) NOT NULL AUTO_INCREMENT,
                             `org_id` bigint(20) NOT NULL,
                             `user_id` bigint(20) NOT NULL,
                             `role_id` bigint(20) NOT NULL,
                             `created` datetime NOT NULL,
                             PRIMARY KEY (`id`),
                             UNIQUE KEY `UQE_user_role_org_id_user_id_role_id` (`org_id`,`user_id`,`role_id`),
                             KEY `IDX_user_role_org_id` (`org_id`),
                             KEY `IDX_user_role_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,1,1,3,'2023-09-28 21:05:42');
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-29 13:46:24



USE autokuca;

CREATE TABLE IF NOT EXISTS automobili
(
    id                 BIGINT auto_increment PRIMARY KEY,
    marka              VARCHAR (255),
    model              VARCHAR (255),
    godina_proizvodnje INT,
    boja               VARCHAR (255),
    broj_vrata         INT,
    snaga_motora       INT,
    zapremina_motora   INT,
    gorivo             VARCHAR (255),
    cena               FLOAT (53),
    na_servisu         INT
    );
create table automobili_SEQ (next_val bigint) engine=InnoDB;
insert into automobili_SEQ values ( 16 );
INSERT INTO automobili
(marka,
 model,
 godina_proizvodnje,
 boja,
 broj_vrata,
 snaga_motora,
 zapremina_motora,
 gorivo,
 cena,
 na_servisu)
VALUES      ('Audi',
             'A4',
             2015,
             'Black',
             4,
             150,
             2000,
             'Diesel',
             20000.00,
             false),
            ('BMW',
             'X5',
             2018,
             'White',
             5,
             250,
             3000,
             'Petrol',
             50000.00,
             true),
            ('Mercedes',
             'C200',
             2017,
             'Blue',
             4,
             180,
             2200,
             'Diesel',
             30000.00,
             false),
            ('Toyota',
             'Corolla',
             2016,
             'Red',
             4,
             132,
             1800,
             'Petrol',
             15000.00,
             false),
            ('Honda',
             'Civic',
             2019,
             'Silver',
             4,
             158,
             2000,
             'Petrol',
             22000.00,
             true),
            ('Ford',
             'Mustang',
             2020,
             'Yellow',
             2,
             310,
             2300,
             'Petrol',
             35000.00,
             false),
            ('Chevrolet',
             'Camaro',
             2018,
             'Black',
             2,
             275,
             2000,
             'Petrol',
             32000.00,
             true),
            ('Hyundai',
             'Elantra',
             2017,
             'Blue',
             4,
             147,
             2000,
             'Petrol',
             17000.00,
             false),
            ('Nissan',
             'Altima',
             2019,
             'White',
             4,
             188,
             2500,
             'Petrol',
             24000.00,
             false),
            ('Volkswagen',
             'Golf',
             2016,
             'Red',
             4,
             170,
             1800,
             'Diesel',
             20000.00,
             true),
            ('Subaru',
             'Impreza',
             2020,
             'Silver',
             4,
             152,
             2000,
             'Petrol',
             22000.00,
             false),
            ('Mazda',
             '3',
             2018,
             'Blue',
             4,
             186,
             2500,
             'Petrol',
             21000.00,
             false),
            ('Kia',
             'Optima',
             2019,
             'Black',
             4,
             185,
             2400,
             'Petrol',
             23999.00,
             true),
            ('Jeep',
             'Cherokee',
             2020,
             'White',
             5,
             271,
             3200,
             'Diesel',
             34000.00,
             false),
            ('Dodge',
             'Charger',
             2018,
             'Red',
             4,
             292,
             3600,
             'Petrol',
             28000.00,
             true);

commit;